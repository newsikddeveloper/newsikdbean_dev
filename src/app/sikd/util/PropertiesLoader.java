package app.sikd.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author detra
 */
public class PropertiesLoader implements Serializable{
    String dbServer;
    String dbName;
    String dbPort;

    public PropertiesLoader() {
    }
    
    public static Properties loadProperties(String propertiesFile) throws Exception{
        URL url = Thread.currentThread().getContextClassLoader().getResource(propertiesFile);
        if (url == null) {
            throw new Exception("The configuration could not be found: ");
//            System.out.println("The configuration could not be found: ");
        } else {
            InputStream iss = null;
            try {
                Properties props = new Properties();
                iss = url.openStream();
                props.load(iss);
                
                return props;
            } catch (IOException ex) {
                Logger.getLogger(PropertiesLoader.class.getName()).log(Level.SEVERE, null, ex);
                throw new Exception(ex.getMessage());
            }
            finally{
                if(iss!= null)
                iss.close();
            }
        }
    }

//    public void loadProperties(String propertiesFile){
//        URL url = Thread.currentThread().getContextClassLoader().getResource(propertiesFile);
//        if (url == null) {
//            System.out.println("The configuration could not be found: ");
//        } else {
//            try {
//                Properties props = new Properties();
//                props.load(url.openStream());
//
//                dbServer = props.getProperty("dbServer");
//                dbName = props.getProperty("dbName");
//                dbPort = props.getProperty("dbPort");
//            } catch (IOException ex) {
//                Logger.getLogger(PropertiesLoader.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }

//    public String getDbName() {
//        return dbName;
//    }
//
//    public void setDbName(String dbName) {
//        this.dbName = dbName;
//    }
//
//    public String getDbPort() {
//        return dbPort;
//    }
//
//    public void setDbPort(String dbPort) {
//        this.dbPort = dbPort;
//    }
//
//    public String getDbServer() {
//        return dbServer;
//    }
//
//    public void setDbServer(String dbServer) {
//        this.dbServer = dbServer;
//    }
}
