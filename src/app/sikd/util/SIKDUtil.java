package app.sikd.util;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author detra
 */
public class SIKDUtil implements Serializable{
    public static final String APBD_MURNI = "Apbd Murni";
    public static final String APBD_PERUBAHAN = "Apbd Perubahan";
    
    public static final String[] APBD = {APBD_MURNI, APBD_PERUBAHAN};
    public static final short APBD_MURNI_SHORT = 0;
    public static final short APBD_PERUBAHAN_SHORT = 1;    
    
    
    public static final String STATUS_BELUM_SIAP_STRING = "Belum Siap Verifikasi ";
    public static final String STATUS_SIAP_STRING = "Siap Verifikasi";
    public static final String STATUS_TELAH_NOT_OK_STRING = "Telah Diverifikasi Pusat (HC <> SC)";
    public static final String STATUS_SEDANG_STRING = "Sedang Diverifikasi Pusat";
    public static final String STATUS_TELAH_OK_STRING = "Telah Diverifikasi Pusat (HC = SC)";
    
    public static final short STATUS_BELUM_SIAP = 0;
    public static final short STATUS_SIAP = 1;
    public static final short STATUS_TELAH_NOT_OK = 2;
    public static final short STATUS_SEDANG = 3;
    public static final short STATUS_TELAH_OK = 4;
    public static final String[] STATUS_DATA = {STATUS_BELUM_SIAP_STRING, STATUS_SIAP_STRING, STATUS_TELAH_NOT_OK_STRING, STATUS_SEDANG_STRING, STATUS_TELAH_OK_STRING};
    
    public static final String getStatusDataAsString(short statusData){
        if(statusData>=0 && statusData<5) return STATUS_DATA[statusData];
        return "";
    }
    public static final short getStatusDataAsShort(String statusData){
        if(statusData==null || statusData.trim().equals("") || statusData.trim().equals(STATUS_BELUM_SIAP_STRING.trim()))
            return STATUS_BELUM_SIAP;
        else if(statusData.trim().equals(STATUS_SEDANG_STRING.trim()) )
            return STATUS_SEDANG;
        else if(statusData.trim().equals(STATUS_SIAP_STRING.trim()) )
            return STATUS_SIAP;
        else if(statusData.trim().equals(STATUS_TELAH_NOT_OK_STRING.trim()) )
            return STATUS_TELAH_NOT_OK;
        else if(statusData.trim().equals(STATUS_TELAH_OK_STRING.trim()) )
            return STATUS_TELAH_OK;
        return 0;
    }
    
    /*public static final short STATUS_APBD_BELUM_FINAL = (short)0;
    public static final short STATUS_APBD_FINAL = (short)1;
    public static final short STATUS_APBD_PROSES_VERIFIKASI = (short)2;
    public static final short STATUS_APBD_SUDAH_VERIFIKASI = (short)3;
    
    public static final String STATUS_DATA_BELUM_FINAL_STRING = "Belum Final";
    public static final String STATUS_DATA_FINAL_PERWAL_STRING = "Final Perwal";
    public static final String STATUS_DATA_FINAL_AUDITED_STRING = "Final Audited";
    
    public static final short STATUS_DATA_BELUM_FINAL = (short)0;
    public static final short STATUS_DATA_FINAL_PERWAL = (short)1;
    public static final short APBD_DATA_FINAL_AUDITED = (short)2;*/
    
    public static final String BULAN_JANUARI = "Januari";
    public static final String BULAN_FEBRUARI = "Februari";
    public static final String BULAN_MARET = "Maret";
    public static final String BULAN_APRIL = "April";
    public static final String BULAN_MEI = "Mei";
    public static final String BULAN_JUNI = "Juni";
    public static final String BULAN_JULI = "Juli";
    public static final String BULAN_AGUSTUS = "Agustus";
    public static final String BULAN_SEPTEMBER = "September";
    public static final String BULAN_OKTOBER = "Oktober";
    public static final String BULAN_NOVEMBER = "November";
    public static final String BULAN_DESEMBER = "Desember";
    
    public static final String[] BULANS = {"", BULAN_JANUARI, BULAN_FEBRUARI, BULAN_MARET, BULAN_APRIL, BULAN_MEI, BULAN_JUNI, BULAN_JULI, BULAN_AGUSTUS, BULAN_SEPTEMBER, BULAN_OKTOBER, BULAN_NOVEMBER, BULAN_DESEMBER};
    
    public static final short BULAN_JANUARI_SHORT = 1;
    public static final short BULAN_FEBRUARI_SHORT = 2;
    public static final short BULAN_MARET_SHORT = 3;
    public static final short BULAN_APRIL_SHORT = 4;
    public static final short BULAN_MEI_SHORT = 5;
    public static final short BULAN_JUNI_SHORT = 6;
    public static final short BULAN_JULI_SHORT = 7;
    public static final short BULAN_AGUSTUS_SHORT = 8;
    public static final short BULAN_SEPTEMBER_SHORT = 9;
    public static final short BULAN_OKTOBER_SHORT = 10;
    public static final short BULAN_NOVEMBER_SHORT = 11;
    public static final short BULAN_DESEMBER_SHORT = 12;
    
    public static final String SESSION_USE = "l061N4cc";
    
    public static final String SESSION_MEN = "4ppM3N";
    
    public static String setBulan(String bulan, String msgName) throws SIKDServiceException{
        if( bulan==null || bulan.trim().length() == 0 )
            throw new SIKDServiceException("Silahkan isi data " + msgName);
        boolean find = false;
        int i = 1;
        String result = "";
        while(find==false && i < BULANS.length ){
            if( BULANS[i].trim().equalsIgnoreCase(bulan.trim()) ){
                result= BULANS[i].trim();
                find = true;
            }
            i++;
        }
        if( !result.trim().equals("") ) return result;
        else{
            String msg = "";
            for (String BULANS1 : BULANS) {
                if( !msg.trim().equals("") ) msg = msg + ", ";
                msg = msg + BULANS1;
            }
            throw new SIKDServiceException("Silahkan isi data " + msgName + " dengan nama-nama bulan " + msg);
        }        
    }
    
    public static short getShortBulan(String bulan){
        short result = 0;
        if( bulan!=null && !bulan.trim().equals("") ){
            short i = 1;
            boolean find = false;
            while( find == false && i < BULANS.length){
                if( BULANS[i].trim().equalsIgnoreCase(bulan.trim()) ){
                    result = i;
                    find = true;
                }
                i++;
            }
        }
        return result;
    }
    
    public static final String GOLONGAN_I = "I";
    public static final String GOLONGAN_II = "II";
    public static final String GOLONGAN_III = "III";
    public static final String GOLONGAN_IV = "IV";
    public static final String[] GOLONGANS = {"", GOLONGAN_I, GOLONGAN_II, GOLONGAN_III, GOLONGAN_IV};
    
    public static final short GOLONGAN_I_SHORT = 1;
    public static final short GOLONGAN_II_SHORT = 2;
    public static final short GOLONGAN_III_SHORT = 3;
    public static final short GOLONGAN_IV_SHORT = 4;
    
    public static short getGolonganAsShort(String gol){
        if( gol != null){
            if(GOLONGAN_I.trim().equals(gol.trim())) return GOLONGAN_I_SHORT;
            else if(GOLONGAN_II.trim().equals(gol.trim())) return GOLONGAN_II_SHORT;
            else if(GOLONGAN_III.trim().equals(gol.trim())) return GOLONGAN_III_SHORT;
            else if(GOLONGAN_IV.trim().equals(gol.trim())) return GOLONGAN_IV_SHORT;
            else return 0;
        }
        else return 0;
    }
    
    public static String getGolonganAsString(short gol){
        if( gol > 0){
            if(gol == GOLONGAN_I_SHORT) return GOLONGAN_I;
            else if(gol == GOLONGAN_II_SHORT) return GOLONGAN_II;
            else if(gol == GOLONGAN_III_SHORT) return GOLONGAN_III;
            else if(gol == GOLONGAN_IV_SHORT) return GOLONGAN_IV;
            else return "";
        }
        else return "";
    }
    
    
    
    public static final String GRADE_A = "a";
    public static final String GRADE_B = "b";
    public static final String GRADE_C = "c";
    public static final String GRADE_D = "d";
    public static final String GRADE_E = "e";
    public static final String[] GRADES = {"", GRADE_A, GRADE_B, GRADE_C, GRADE_D, GRADE_E};
    
    public static final short GRADE_A_SHORT = 1;
    public static final short GRADE_B_SHORT = 2;
    public static final short GRADE_C_SHORT = 3;
    public static final short GRADE_D_SHORT = 4;
    public static final short GRADE_E_SHORT = 5;
    
    public static short getGradeAsShort(String grade){
        if( grade != null){
            if(GRADE_A.trim().equals(grade.trim())) return GRADE_A_SHORT;
            else if(GRADE_B.trim().equals(grade.trim())) return GRADE_B_SHORT;
            else if(GRADE_C.trim().equals(grade.trim())) return GRADE_C_SHORT;
            else if(GRADE_D.trim().equals(grade.trim())) return GRADE_D_SHORT;
            else if(GRADE_E.trim().equals(grade.trim())) return GRADE_E_SHORT;
            else return 0;
        }
        else return 0;
    }
    
    public static String getGradeAsString(short grade){
        if( grade > 0){
            if(grade == GRADE_A_SHORT) return GRADE_A;
            else if(grade == GRADE_B_SHORT) return GRADE_B;
            else if(grade == GRADE_C_SHORT) return GRADE_C;
            else if(grade == GRADE_D_SHORT) return GRADE_D;
            else if(grade == GRADE_E_SHORT) return GRADE_E;
            else return "";
        }
        else return "";
    }
    
    public static final String ESELON_I = "I";
    public static final String ESELON_II = "II";
    public static final String ESELON_III = "III";
    public static final String ESELON_IV = "IV";
    public static final String ESELON_V = "V";
    public static final String ESELON_FUNGSIONAL = "Fungsional";
    public static final String ESELON_STAF = "Staf";
    public static final String[] ESELONS = {"", ESELON_I, ESELON_II, ESELON_III, ESELON_IV, ESELON_V, ESELON_FUNGSIONAL, ESELON_STAF};
    
    public static final short ESELON_I_SHORT = 1;
    public static final short ESELON_II_SHORT = 2;
    public static final short ESELON_III_SHORT = 3;
    public static final short ESELON_IV_SHORT = 4;
    public static final short ESELON_V_SHORT = 5;
    public static final short ESELON_FUNGSIONAL_SHORT = 6;
    public static final short ESELON_STAF_SHORT = 7;
    
    public static short getEselonAsShort(String eselon){
        if( eselon != null){
            if(ESELON_I.trim().equals(eselon.trim())) return ESELON_I_SHORT;
            else if(ESELON_II.trim().equals(eselon.trim())) return ESELON_II_SHORT;
            else if(ESELON_III.trim().equals(eselon.trim())) return ESELON_III_SHORT;
            else if(ESELON_IV.trim().equals(eselon.trim())) return ESELON_IV_SHORT;
            else if(ESELON_V.trim().equals(eselon.trim())) return ESELON_V_SHORT;
            else if(ESELON_FUNGSIONAL.trim().equals(eselon.trim())) return ESELON_FUNGSIONAL_SHORT;
            else if(ESELON_STAF.trim().equals(eselon.trim())) return ESELON_STAF_SHORT;
            else return 0;
        }
        else return 0;
    }
    
    public static final String SP2D_UP = "UP";
    public static final String SP2D_GU = "GU";
    public static final String SP2D_TU = "TU";
    public static final String SP2D_LS_GAJI = "LS-Gaji";
    public static final String SP2D_LS_BARANG_JASA = "LS-Barang/Jasa";
    public static final String SP2D_NIHIL = "NIHIL";
    
    public static final String[] SP2D = {"", SP2D_UP, SP2D_GU, SP2D_TU, SP2D_LS_GAJI, SP2D_LS_BARANG_JASA, SP2D_NIHIL};
    
    public static final short SP2D_UP_SHORT = 1;
    public static final short SP2D_GU_SHORT = 2;
    public static final short SP2D_TU_SHORT = 3;
    public static final short SP2D_LS_GAJI_SHORT = 4;
    public static final short SP2D_LS_BARANG_JASA_SHORT = 5;
    public static final short SP2D_NIHIL_SHORT = 6;
    
    public static short getJenisSP2DAsShort(String sp2d){
        if( sp2d != null){
            if(SP2D_UP.trim().equals(sp2d.trim())) return SP2D_UP_SHORT;
            else if(SP2D_GU.trim().equals(sp2d.trim())) return SP2D_GU_SHORT;
            else if(SP2D_TU.trim().equals(sp2d.trim())) return SP2D_TU_SHORT;
            else if(SP2D_LS_GAJI.trim().equals(sp2d.trim())) return SP2D_LS_GAJI_SHORT;
            else if(SP2D_LS_BARANG_JASA.trim().equals(sp2d.trim())) return SP2D_LS_BARANG_JASA_SHORT;
            else if(SP2D_NIHIL.trim().equals(sp2d.trim())) return SP2D_NIHIL_SHORT;
            else return 0;
        }
        else return 0;
    }
    
    public final String[] JENIS_PAJAKS= {"", "PPh21", "PPh22", "PPh23", "PPh25", "PPN"};
    
    public static final String PAJAK_PPh21 = "PPh21";
    public static final String PAJAK_PPh22 = "PPh22";
    public static final String PAJAK_PPh23 = "PPh23";
    public static final String PAJAK_PPh25 = "PPh25";
    public static final String PAJAK_PPN = "PPN";
    
    public static final short PAJAK_PPh21_SHORT = 1;
    public static final short PAJAK_PPh22_SHORT = 2;
    public static final short PAJAK_PPh23_SHORT = 3;
    public static final short PAJAK_PPh25_SHORT = 4;
    public static final short PAJAK_PPN_SHORT = 5;
    
    public static short getJenisPajakAsShort(String jenisPajak){
        if( jenisPajak != null){
            if(PAJAK_PPh21.trim().equals(jenisPajak.trim())) return PAJAK_PPh21_SHORT;
            else if(PAJAK_PPh22.trim().equals(jenisPajak.trim())) return PAJAK_PPh22_SHORT;
            else if(PAJAK_PPh23.trim().equals(jenisPajak.trim())) return PAJAK_PPh23_SHORT;
            else if(PAJAK_PPh25.trim().equals(jenisPajak.trim())) return PAJAK_PPh25_SHORT;
            else if(PAJAK_PPN.trim().equals(jenisPajak.trim())) return PAJAK_PPN_SHORT;
            else return 0;
        }
        else return 0;
    }
    
    public static String getJenisPajakAsString(short jenisPajak){
        
            if(jenisPajak == PAJAK_PPh21_SHORT) return PAJAK_PPh21;
            else if(jenisPajak == PAJAK_PPh22_SHORT) return PAJAK_PPh22;
            else if(jenisPajak == PAJAK_PPh23_SHORT) return PAJAK_PPh23;
            else if(jenisPajak == PAJAK_PPh25_SHORT) return PAJAK_PPh25;
            else if(jenisPajak == PAJAK_PPN_SHORT) return PAJAK_PPN;
            else return "";
        
    }
    
    
    public final String[] JENIS_POTONGANS= {"", "IWP", "TAPERUM", "TASPEN", "ASKES"};
    
    public static final String POTONGAN_IWP = "IWP";
    public static final String POTONGAN_TAPERUM = "TAPERUM";
    public static final String POTONGAN_TASPEN = "TASPEN";
    public static final String POTONGAN_ASKES = "ASKES";
    
    public static final short POTONGAN_IWP_SHORT = 1001;
    public static final short POTONGAN_TAPERUM_SHORT = 1002;
    public static final short POTONGAN_TASPEN_SHORT = 1003;
    public static final short POTONGAN_ASKES_SHORT = 1004;
    
    
    public static String getJenisPotonganAsString(short jenisPotongan){        
            if(jenisPotongan == POTONGAN_IWP_SHORT) return POTONGAN_IWP;
            else if(jenisPotongan == POTONGAN_TAPERUM_SHORT) return POTONGAN_TAPERUM;
            else if(jenisPotongan == POTONGAN_TASPEN_SHORT) return POTONGAN_TASPEN;
            else if(jenisPotongan == POTONGAN_ASKES_SHORT) return POTONGAN_ASKES;
            else return "";        
    }
    
    public static final String SUMBER_DANA_PAD = "PAD";
    public static final String SUMBER_DANA_DAU = "DAU";
    public static final String SUMBER_DANA_DAK = "DAK";
    public static final String SUMBER_DANA_OTSUS = "Dana Otsus";
    public static final String SUMBER_DANA_PENYESUAIAN = "Dana Penyesuaian";
    public static final String SUMBER_DANA_BAGI_HASIL = "Dana Bagi Hasil";
    public static final String SUMBER_DANA_LAIN = "Lain-lain Pendapatan Yang Sah";
    
    
    public static final String[] SUMBER_DANA = {"",SUMBER_DANA_PAD, SUMBER_DANA_DAU,SUMBER_DANA_DAK, SUMBER_DANA_OTSUS, SUMBER_DANA_PENYESUAIAN, SUMBER_DANA_BAGI_HASIL, SUMBER_DANA_LAIN};
    
    public static final short SUMBER_DANA_PAD_SHORT = 1;
    public static final short SUMBER_DANA_DAU_SHORT = 2;
    public static final short SUMBER_DANA_DAK_SHORT = 3;
    public static final short SUMBER_DANA_OTSUS_SHORT = 4;
    public static final short SUMBER_DANA_PENYESUAIAN_SHORT = 5;
    public static final short SUMBER_DANA_BAGI_HASIL_SHORT = 6;
    public static final short SUMBER_DANA_LAIN_SHORT = 7;
    
    public static String getSumberDanaSP2DAsString(short sumberDana){
        
            if(sumberDana == SUMBER_DANA_PAD_SHORT) return SUMBER_DANA_PAD;
            else if(sumberDana == SUMBER_DANA_DAU_SHORT) return SUMBER_DANA_DAU;
            else if(sumberDana == SUMBER_DANA_DAK_SHORT) return SUMBER_DANA_DAK;
            else if(sumberDana == SUMBER_DANA_OTSUS_SHORT) return SUMBER_DANA_OTSUS;
            else if(sumberDana == SUMBER_DANA_PENYESUAIAN_SHORT) return SUMBER_DANA_PENYESUAIAN;
            else if(sumberDana == SUMBER_DANA_BAGI_HASIL_SHORT) return SUMBER_DANA_BAGI_HASIL;
            else if(sumberDana == SUMBER_DANA_LAIN_SHORT) return SUMBER_DANA_LAIN;
            
            else return "";
        
    }
    
    public static final short COA_PMDN13 = 1;
    public static final short COA_PMDN64 = 2;
    
    public static String doubleToString(double nilai){
        Locale.setDefault(Locale.GERMAN);
        DecimalFormat df = new DecimalFormat("#,###,##0.00");
        df.setNegativePrefix("( ");
        df.setNegativeSuffix(" )");
        String result = df.format(nilai);
        return result;
    }
    
    public static double stringToDouble(String nilai){
        if (nilai != null && !nilai.trim().equals("")) {
            if( nilai.trim().startsWith("-") ){
                nilai = nilai.trim().substring(1).trim();
                nilai = "( " + nilai + " )";
            }
            else if( nilai.trim().startsWith("(") || nilai.trim().endsWith(")")){
                if( nilai.trim().startsWith("(") ) nilai = nilai.trim().substring(1).trim();
                if( nilai.trim().endsWith(")") ) nilai = nilai.trim().substring(0, nilai.trim().length()-1).trim();
                
                nilai = "( " + nilai + " )";
            }
            try {
                Locale.setDefault(Locale.GERMAN);
                DecimalFormat df = new DecimalFormat("#,###,##0.00");
                df.setNegativePrefix("( ");
                df.setNegativeSuffix(" )");
                double result = df.parse(nilai).doubleValue();
                return result;
            } catch (ParseException ex) {
                Logger.getLogger(SIKDUtil.class.getName()).log(Level.SEVERE, null, ex);
                return 0;
            }
        }
        else return 0;
    }
    
    @SuppressWarnings("SuspiciousIndentAfterControlStatement")
    public static Date stringToDate(String tgl){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            ParsePosition pos = new ParsePosition(0);
            if( tgl!=null && !tgl.trim().equals(""))
            return sdf.parse(tgl);
            return null;
        } catch (ParseException ex) {
            return null;
        }
    }
    public static String dateToString(Date tgl){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            return sdf.format(tgl);
        } catch (Exception ex) {
            return "";
        }
    }
    
    public static String initCap(String str){
        String result ="";
        if( str!=null && !str.trim().equals("")){
            str = str.trim().toLowerCase();
            StringTokenizer tok = new StringTokenizer(str, " ");
            List<String> arr = new ArrayList();
            while(tok.hasMoreTokens()){
                String s = tok.nextToken();
                String fs = s.substring(0,1).toUpperCase();
                String sisa = s.substring(1, s.length());
                arr.add(fs.trim()+sisa.trim().toLowerCase());
            }
            for (String arr1 : arr) { 
                result = result + arr1 + " ";
            }
            result=result.trim();
        }
        return result;
    }
    
    public static final String JENIS_PAJAKRETRIBUSI_PAJAK = "PAJAK";
    public static final String JENIS_PAJAKRETRIBUSI_RETRIBUSI = "RETRIBUSI";
    
    public static final String[] JENIS_PAJAKRETRIBUSI = {"",JENIS_PAJAKRETRIBUSI_PAJAK, JENIS_PAJAKRETRIBUSI_RETRIBUSI};
    
    public static final short JENIS_PAJAKRETRIBUSI_PAJAK_SHORT = 1;
    public static final short JENIS_PAJAKRETRIBUSI_RETRIBUSI_SHORT = 2;
    
    
    public static final String getJenisPajakRetribusiAsString(short jenis){
        if( jenis == JENIS_PAJAKRETRIBUSI_PAJAK_SHORT ) return JENIS_PAJAKRETRIBUSI_PAJAK;
        else if( jenis == JENIS_PAJAKRETRIBUSI_RETRIBUSI_SHORT ) return JENIS_PAJAKRETRIBUSI_RETRIBUSI;
        else return "";
    }
    
    public static final String getNamaMenu(String fullPath){
        StringTokenizer st = new StringTokenizer(fullPath, "/");
        String namamenu = "";
        int iii = 0;
        while (st.hasMoreTokens()) {
            String ss = st.nextToken();
            if (iii > 0) {
                if (!namamenu.trim().equals("")) {
                    namamenu = namamenu + "/";
                }
                namamenu = namamenu + ss;
            }
            iii++;
        }
        return namamenu;
    }
    
    public static List<String> getYears(short yearStart){
        List<String> result = new ArrayList();
        GregorianCalendar gc = (GregorianCalendar)Calendar.getInstance();
        gc.setTime(new Date());
        int y = gc.get(Calendar.YEAR);
        for (int i = yearStart; i < y+2; i++) {
            result.add(String.valueOf(i));
        }
        return result;
    }
    
    public static List<Periode> getPeriodes(){
        List<Periode> result = new ArrayList();
        result.add(new Periode((short)1, "Januari"));
        result.add(new Periode((short)2, "Februari"));
        result.add(new Periode((short)3, "Maret"));
        result.add(new Periode((short)4, "April"));
        result.add(new Periode((short)5, "Mei"));
        result.add(new Periode((short)6, "Juni"));
        result.add(new Periode((short)7, "Juli"));
        result.add(new Periode((short)8, "Agustus"));
        result.add(new Periode((short)9, "September"));
        result.add(new Periode((short)10, "Oktober"));
        result.add(new Periode((short)11, "November"));
        result.add(new Periode((short)12, "Desember"));
        return result;
    }
    public static List<Periode> getTriwulans(){
        List<Periode> result = new ArrayList();
        result.add(new Periode((short)1, "Triwulan I"));
        result.add(new Periode((short)2, "Triwulan II"));
        result.add(new Periode((short)3, "Triwulan III"));
        result.add(new Periode((short)4, "Triwulan IV"));
        return result;
    }
    
    public static List<Periode> getSemesters(){
        List<Periode> result = new ArrayList();
        result.add(new Periode((short)1, "Semester I"));
        result.add(new Periode((short)2, "Semester II"));
        return result;
    }

}

