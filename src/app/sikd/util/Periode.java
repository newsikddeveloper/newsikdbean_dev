package app.sikd.util;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sora
 */
public class Periode implements Serializable{
    private short bulanShort;
    private String bulanString;

    public Periode() {
    }

    public Periode(short bulanShort, String bulanString) {
        this.bulanShort = bulanShort;
        this.bulanString = bulanString;
    }

    public String getBulanString() {
        return bulanString;
    }

    public void setBulanString(String bulanString) {
        this.bulanString = bulanString;
    }

    public short getBulanShort() {
        return bulanShort;
    }

    public void setBulanShort(short bulanShort) {
        this.bulanShort = bulanShort;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.bulanShort;
        hash = 29 * hash + Objects.hashCode(this.bulanString);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Periode other = (Periode) obj;
        if (!Objects.equals(this.bulanString, other.bulanString)) {
            return false;
        }
        return true;
    }
    

    @Override
    public String toString() {
        return bulanString;
    }
    
    
    
}
