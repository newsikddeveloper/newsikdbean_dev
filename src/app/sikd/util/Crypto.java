package app.sikd.util;

import com.sun.crypto.provider.SunJCE;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/**
 *
 * @author USER1000
 */
public class Crypto {

    public Crypto() {
    }
    

    public static byte[] encrypt(byte[] text) throws NoSuchAlgorithmException,
            InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException,
            IllegalBlockSizeException, InvalidAlgorithmParameterException,
            BadPaddingException {
        SunJCE jce = new SunJCE();
        Security.addProvider(jce);

        PBEKeySpec pbeKeySpec;
        PBEParameterSpec pbeParamSpec;
        SecretKeyFactory keyFac;

        // Salt
        byte[] salt = {
            (byte) 0xc7, (byte) 0x75, (byte) 0x21, (byte) 0x8c,
            (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99
        };

        // Iteration count
        int count = 20;

        // Create PBE parameter set
        pbeParamSpec = new PBEParameterSpec(salt, count);

        char key[] = {'k', 'U', 'M', '@', 'l', 'A', 'y', '@', 'n', 'G', '$', 'N', '3', '@', 'n', 'g', '*',
            'N', 'u', '#', '@', 'm', '1', '5', '#', 'A', 'M', 'i', '$', '&'};

        pbeKeySpec = new PBEKeySpec(key);
        keyFac = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");

        // Initialize PBE Cipher with key and parameters
        pbeCipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);

        // Encrypt the cleartext
        return pbeCipher.doFinal(text);
    }

    public static byte[] decrypt(byte[] cipher) throws NoSuchAlgorithmException,
            InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException,
            IllegalBlockSizeException, InvalidAlgorithmParameterException,
            BadPaddingException {
        SunJCE jce = new SunJCE();
        Security.addProvider(jce);

        PBEKeySpec pbeKeySpec;
        PBEParameterSpec pbeParamSpec;
        SecretKeyFactory keyFac;

        // Salt
        byte[] salt = {
            (byte) 0xc7, (byte) 0x75, (byte) 0x21, (byte) 0x8c,
            (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99
        };

        // Iteration count
        int count = 20;

        // Create PBE parameter set
        pbeParamSpec = new PBEParameterSpec(salt, count);

        char key[] = {'k', 'U', 'M', '@', 'l', 'A', 'y', '@', 'n', 'G', '$', 'N', '3', '@', 'n', 'g', '*',
            'N', 'u', '#', '@', 'm', '1', '5', '#', 'A', 'M', 'i', '$', '&'};

        pbeKeySpec = new PBEKeySpec(key);
        keyFac = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");

        // Initialize PBE Cipher with key and parameters
        pbeCipher.init(Cipher.DECRYPT_MODE, pbeKey, pbeParamSpec);

        // Encrypt the cleartext
        return pbeCipher.doFinal(cipher);
    }
    
    
    public static byte[] encrypt(byte[] text, String name) throws NoSuchAlgorithmException,
            InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException,
            IllegalBlockSizeException, InvalidAlgorithmParameterException,
            BadPaddingException {
        SunJCE jce = new SunJCE();
        Security.addProvider(jce);

        PBEKeySpec pbeKeySpec;
        PBEParameterSpec pbeParamSpec;
        SecretKeyFactory keyFac;

        // Salt
        byte[] salt = {
            (byte) 0xc7, (byte) 0x75, (byte) 0x21, (byte) 0x8c,
            (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99
        };
                
        // Iteration count
        int count = 20;

        // Create PBE parameter set
        pbeParamSpec = new PBEParameterSpec(salt, count);
        
        String sss = "";
        for (int i = 0; i < name.trim().length(); i++) {
            sss=sss+(byte)name.trim().charAt(i);            
        }
        
        name = "kUM@lAy@nG$N3@ng*Nu#@m15#AMi$&"+sss.trim();
        char key []= new char[name.trim().length()];
        for (int i = 0; i < name.trim().length(); i++) {
            key[i] = name.trim().charAt(i);
            
        }
//        char key[] = {'k', 'U', 'M', '@', 'l', 'A', 'y', '@', 'n', 'G', '$', 'N', '3', '@', 'n', 'g', '*',
//            'N', 'u', '#', '@', 'm', '1', '5', '#', 'A', 'M', 'i', '$', '&'};

        pbeKeySpec = new PBEKeySpec(key);
        keyFac = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");

        // Initialize PBE Cipher with key and parameters
        pbeCipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);

        // Encrypt the cleartext
        return pbeCipher.doFinal(text);
    }

    public static byte[] decrypt(byte[] cipher, String name) throws NoSuchAlgorithmException,
            InvalidKeySpecException, InvalidKeyException, NoSuchPaddingException,
            IllegalBlockSizeException, InvalidAlgorithmParameterException,
            BadPaddingException {
        SunJCE jce = new SunJCE();
        Security.addProvider(jce);

        PBEKeySpec pbeKeySpec;
        PBEParameterSpec pbeParamSpec;
        SecretKeyFactory keyFac;

        // Salt
        byte[] salt = {
            (byte) 0xc7, (byte) 0x75, (byte) 0x21, (byte) 0x8c,
            (byte) 0x7e, (byte) 0xc8, (byte) 0xee, (byte) 0x99
        };

        // Iteration count
        int count = 20;

        // Create PBE parameter set
        pbeParamSpec = new PBEParameterSpec(salt, count);
        
        String sss = "";
        for (int i = 0; i < name.trim().length(); i++) {
            sss=sss+(byte)name.trim().charAt(i);            
        }
        
        name = "kUM@lAy@nG$N3@ng*Nu#@m15#AMi$&"+sss.trim();
        char key []= new char[name.trim().length()];
        for (int i = 0; i < name.trim().length(); i++) {
            key[i] = name.trim().charAt(i);
            
        }
//        char key[] = {'k', 'U', 'M', '@', 'l', 'A', 'y', '@', 'n', 'G', '$', 'N', '3', '@', 'n', 'g', '*',
//            'N', 'u', '#', '@', 'm', '1', '5', '#', 'A', 'M', 'i', '$', '&'};

        pbeKeySpec = new PBEKeySpec(key);
        keyFac = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
        SecretKey pbeKey = keyFac.generateSecret(pbeKeySpec);

        Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");

        // Initialize PBE Cipher with key and parameters
        pbeCipher.init(Cipher.DECRYPT_MODE, pbeKey, pbeParamSpec);

        // Encrypt the cleartext
        return pbeCipher.doFinal(cipher);
    }

}
