package app.sikd.util;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author sora
 */
public class ServiceUtil {
    
    public static final String getServiceUserName(String userName) throws Exception{
        try {
            String result = "";
            byte[] names = new byte[userName.trim().length()];
            for (int i = 0; i < userName.trim().length(); i++) {
                names[i] = (byte) userName.trim().charAt(i);
            }
                    
            byte[] dName = Crypto.decrypt(names);
            for (int i = 0; i < dName.length; i++) {
                result = result+(char)dName[i];
            }
            
            return result;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | InvalidAlgorithmParameterException | BadPaddingException ex) {
//            Logger.getLogger(ServiceUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("Gagal decrypt userName Service");
        }
                
    }
    
    public static final byte[] getPasswordService(String pass) throws Exception{        
        try {
            byte[] pp = new byte[pass.trim().length()];
            for (int i = 0; i < pass.trim().length(); i++) {
                pp[i] = (byte) pass.trim().charAt(i);
            }
            
//            byte[] p = Crypto.decrypt(pp, userName);
            return pp;
        } catch (Exception ex) {
            Logger.getLogger(ServiceUtil.class.getName()).log(Level.SEVERE, null, ex);
            throw new Exception("gagal decrypt password service : " + ex.getMessage());
        }
        
    }
    
    
    
}
