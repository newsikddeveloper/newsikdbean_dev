package app.sikd.entity;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class WilayahKerja implements Serializable{
    private long id;
    private String nama;
    private String deskripsi;
    private List<Pemda> pemdas;

    public WilayahKerja() {
    }

    public WilayahKerja(String nama, String deskripsi) {
        this.nama = nama;
        this.deskripsi = deskripsi;
    }

    public WilayahKerja(long id, String nama, String deskripsi) {
        this.id = id;
        this.nama = nama;
        this.deskripsi = deskripsi;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public List<Pemda> getPemdas() {
        return pemdas;
    }

    public void setPemdas(List<Pemda> pemdas) {
        this.pemdas = pemdas;
    }
    
    
    
}
