/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.utilitas;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sora
 */
public class MappingNonStandar implements Serializable{
    private long index;
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private String kodeAkun;
    private String nama;
    private SimpleAccount accrualAccount;

    public MappingNonStandar() {
    }

    public MappingNonStandar(String kodeSatker, String kodePemda, String namaPemda, String kodeAkun, String nama) {
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.kodeAkun = kodeAkun;
        this.nama = nama;
    }

    public MappingNonStandar(long index, String kodeSatker, String kodePemda, String namaPemda, String kodeAkun, String nama) {
        this.index = index;
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.kodeAkun = kodeAkun;
        this.nama = nama;
    }

    public MappingNonStandar(String kodeSatker, String kodePemda, String namaPemda, String kodeAkun, String nama, SimpleAccount accrualAccount) {
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.kodeAkun = kodeAkun;
        this.nama = nama;
        this.accrualAccount = accrualAccount;
    }

    public MappingNonStandar(long index, String kodeSatker, String kodePemda, String namaPemda, String kodeAkun, String nama, SimpleAccount accrualAccount) {
        this.index = index;
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.kodeAkun = kodeAkun;
        this.nama = nama;
        this.accrualAccount = accrualAccount;
    }
    
    public MappingNonStandar(MappingNonStandar mns){//long index, String kodeSatker, String kodePemda, String namaPemda, String kodeAkun, String nama, SimpleAccount accrualAccount) {
        this.index = mns.getIndex();
        this.kodeSatker = mns.getKodeSatker();
        this.kodePemda = mns.getKodePemda();
        this.namaPemda = mns.getNamaPemda();
        this.kodeAkun = mns.getKodeAkun();
        this.nama = mns.getNama();
        if( mns.getAccrualAccount()!=null)
            this.accrualAccount = new SimpleAccount(mns.getAccrualAccount().getIndex(), mns.getAccrualAccount().getCode(), mns.getAccrualAccount().getName());
    }

    public SimpleAccount getAccrualAccount() {
        return accrualAccount;
    }

    public void setAccrualAccount(SimpleAccount accrualAccount) {
        this.accrualAccount = accrualAccount;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) {
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) {
        this.kodeAkun = kodeAkun;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.kodeSatker);
        hash = 79 * hash + Objects.hashCode(this.kodeAkun);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MappingNonStandar other = (MappingNonStandar) obj;
        if (!Objects.equals(this.kodeSatker, other.kodeSatker)) {
            return false;
        }
        if (!Objects.equals(this.kodeAkun, other.kodeAkun)) {
            return false;
        }
        return true;
    }

    
    
    
    
    
    
    
            
    
}
