package app.sikd.entity.utilitas;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Dasep
 */
public class UploadFile  implements Serializable{    
    private long id;    
    private short tahun;
    private String ikd;
    private String kdsatker;
    private String filename;
    private Date tglupload;

    public UploadFile() {
    }

    public UploadFile(String ikd) {
        this.ikd = ikd;
    }

    public UploadFile(short tahun, String ikd, String kdsatker, String filename, Date tglupload) {
        this.tahun = tahun;
        this.ikd = ikd;
        this.kdsatker = kdsatker;
        this.filename = filename;
        this.tglupload = tglupload;
    }

    public UploadFile(long id, short tahun, String ikd, String kdsatker, String filename, Date tglupload) {
        this.id = id;
        this.tahun = tahun;
        this.ikd = ikd;
        this.kdsatker = kdsatker;
        this.filename = filename;
        this.tglupload = tglupload;
    }
    
    public long getId() {
        return id;
    }

    public String getKdsatker() {
        return kdsatker;
    }

    public String getFilename() {
        return filename;
    }

    public Date getTglupload() {
        return tglupload;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setKdsatker(String kdsatker) {
        this.kdsatker = kdsatker;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setTglupload(Date tglupload) {
        this.tglupload = tglupload;
    }

    public short getTahun() {
        return tahun;
    }

    public void setTahun(short tahun) {
        this.tahun = tahun;
    }

    public String getIkd() {
        return ikd;
    }

    public void setIkd(String ikd) {
        this.ikd = ikd;
    }
}
