/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.utilitas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author sora
 */
public class SimpleAccount implements Serializable{
    private long index;
    private String code;
    private String name;
    private short level;
    private List<SimpleAccount> subAccount;
    private String style;

    public SimpleAccount() {
    }

    public SimpleAccount(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public SimpleAccount(long index, String code, String name) {
        this.index = index;
        this.code = code;
        this.name = name;
    }

    public SimpleAccount(String code, String name, short level) {
        this.code = code;
        this.name = name;
        this.level = level;
    }

    public SimpleAccount(long index, String code, String name, short level) {
        this.index = index;
        this.code = code;
        this.name = name;
        this.level = level;
    }
    public SimpleAccount(long index, String code, String name, short level, String style) {
        this.index = index;
        this.code = code;
        this.name = name;
        this.level = level;
        this.style =style;
    }
    public SimpleAccount(SimpleAccount sa) {
        this.index = sa.getIndex();
        this.code = sa.getCode();
        this.name = sa.getName();
        this.level = sa.getLevel();
        this.style =sa.getStyle();
        this.subAccount=sa.getSubAccount();
    }

    @Override
    public String toString() {
        return code + " " + name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public List<SimpleAccount> getSubAccount() {
        if( subAccount == null ) subAccount = new ArrayList();
        return subAccount;
    }

    public void setSubAccount(List<SimpleAccount> subAccount) {
        this.subAccount = subAccount;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SimpleAccount other = (SimpleAccount) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return true;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
    
    
    
    
    
}
