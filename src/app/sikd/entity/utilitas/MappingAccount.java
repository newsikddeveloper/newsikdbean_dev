/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.utilitas;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class MappingAccount implements Serializable{
    private long index;
    private String code;
    private String name;
    private SimpleAccount mapAccount;
    private String style;

    public MappingAccount() {
    }

    public MappingAccount(long index, String code, String name) {
        this.index = index;
        this.code = code;
        this.name = name;
    }

    public MappingAccount(long index, String code, String name, SimpleAccount mapAccount) {
        this.index = index;
        this.code = code;
        this.name = name;
        this.mapAccount = mapAccount;
    }

    public MappingAccount(long index, String code, String name, SimpleAccount mapAccount, String style) {
        this.index = index;
        this.code = code;
        this.name = name;
        this.mapAccount = mapAccount;
        this.style = style;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public String toString() {
        return code + " " + name;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SimpleAccount getMapAccount() {
        return mapAccount;
    }

    public void setMapAccount(SimpleAccount mapAccount) {
        this.mapAccount = mapAccount;
    }
    
    
    
}
