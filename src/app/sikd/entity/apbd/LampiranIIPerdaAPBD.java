/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 *
 * @author detra
 */
public class LampiranIIPerdaAPBD implements Serializable{
    private String kode;    
    private String urusanPemerintah;
    private String kodeSKPD;
    private String namaSKPD;
    private double pendapatan;
    private double belanjaTidakLangsung;
    private double belanjaLangsung;
    
    private double penerimaan;
    private double pembiayaanNeto;
    private double silpa;

    public LampiranIIPerdaAPBD() {
    }

    public LampiranIIPerdaAPBD(String kode, String urusanPemerintah, double pendapatan, double belanjaTidakLangsung, double belanjaLangsung) {
        this.kode = kode;
        this.urusanPemerintah = urusanPemerintah;        
        this.pendapatan = pendapatan;
        this.belanjaTidakLangsung = belanjaTidakLangsung;
        this.belanjaLangsung = belanjaLangsung;
    }
    
    public LampiranIIPerdaAPBD(String kode, String urusanPemerintah, double pendapatan, double belanjaTidakLangsung, double belanjaLangsung, double penerimaan, double pembiayaanNeto,  double silpa) {
        this.kode = kode;
        this.urusanPemerintah = urusanPemerintah;
        this.pendapatan = pendapatan;
        this.belanjaTidakLangsung = belanjaTidakLangsung;
        this.belanjaLangsung = belanjaLangsung;
        this.penerimaan = penerimaan;
        this.pembiayaanNeto = pembiayaanNeto;
        this.silpa = silpa;
    }


    public double getSilpa() {
        return silpa;
    }

    public void setSilpa(double silpa) {
        this.silpa = silpa;
    }

    public String getKode() {
        if(kode==null) return "";
        else return kode.trim();
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getUrusanPemerintah() {
        if( urusanPemerintah == null ) return "";
        else return urusanPemerintah.trim();
    }

    public void setUrusanPemerintah(String urusanPemerintah) {
        this.urusanPemerintah = urusanPemerintah;
    }

    public double getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(double pendapatan) {
        this.pendapatan = pendapatan;
    }

    public double getBelanjaTidakLangsung() {
        return belanjaTidakLangsung;
    }

    public void setBelanjaTidakLangsung(double belanjaTidakLangsung) {
        this.belanjaTidakLangsung = belanjaTidakLangsung;
    }

    public double getBelanjaLangsung() {
        return belanjaLangsung;
    }

    public void setBelanjaLangsung(double belanjaLangsung) {
        this.belanjaLangsung = belanjaLangsung;
    }

    public double getPenerimaan() {
        return penerimaan;
    }

    public void setPenerimaan(double penerimaan) {
        this.penerimaan = penerimaan;
    }

    public double getPembiayaanNeto() {
        return pembiayaanNeto;
    }

    public void setPembiayaanNeto(double pembiayaanNeto) {
        this.pembiayaanNeto = pembiayaanNeto;
    }    
    
    public String getPendapatanString() {
        if( pendapatan==0 ){
            if(kode.trim().length()<=1) return "";
            else return String.valueOf(pendapatan);
        }
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(pendapatan);//bd.toPlainString();
        }
    }
    
    public void setPendapatanString(String pendapatanString){
        
    }
    
    public String getBTLString() {
        if( belanjaTidakLangsung==0 ){
            if(kode.trim().length()<=1) return "";
            else return String.valueOf(belanjaTidakLangsung);
        }
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(belanjaTidakLangsung);//bd.toPlainString();
        }
    }
    
    public void setBTLString(String bTLString){
        
    }
    public String getBLString() {
        if( belanjaLangsung==0 ){
            if(kode.trim().length()<=1) return "";
            else return String.valueOf(belanjaLangsung);
        }
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(belanjaLangsung);//bd.toPlainString();
        }
    }
    
    public void setBLString(String bLString){
        
    }
    public String getBLBTLString() {
        if( belanjaLangsung+belanjaTidakLangsung==0 ){
            if(kode.trim().length()<=1) return "";
            else return String.valueOf(0);
        }
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(belanjaLangsung+belanjaTidakLangsung);//bd.toPlainString();
        }
    }
    
    public void setBLBTLString(String bLBTLString){
        
    }
    public String getPenerimaanString() {
        if( penerimaan==0 ){
            if(kode.trim().length()<=1) return "";
            else return String.valueOf(0);
        }
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(penerimaan);//bd.toPlainString();
        }
    }
    
    public void setPenerimaanString(String penerimaanString){
        
    }
    public String getPembiayaanNetoString() {
        if( pembiayaanNeto==0 ){
            if(kode.trim().length()<=1) return "";
            else return String.valueOf(0);
        }
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(pembiayaanNeto);//bd.toPlainString();
        }
    }
    
    public void setPembiayaanNetoString(String pembiayaanNetoString){
        
    }
    public String getPenerimaanPembiayaanString() {
        
            if(kode.trim().length()<=1) return "";              
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(penerimaan-pembiayaanNeto);//bd.toPlainString();
        }
    }
    
    public void setPenerimaanPembiayaanNetoString(String penerimaanPembiayaanNetoString){
        
    }
    public String getSilpaString() {
        if( silpa==0 ){
            if(kode.trim().length()<=1) return "";
            else return String.valueOf(0);
        }
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(silpa);//bd.toPlainString();
        }
    }
    
    public void setSilpaString(String silpaString){
        
    }
}