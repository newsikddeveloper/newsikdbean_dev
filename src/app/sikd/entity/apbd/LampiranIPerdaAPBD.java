/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 *
 * @author detra
 */
public class LampiranIPerdaAPBD implements Serializable{
    private String  kode;
    private String uraian;
    private double jumlah;

    public LampiranIPerdaAPBD() { 
    }

    public LampiranIPerdaAPBD(String kode, String uraian, double jumlah) {
        this.kode = kode;
        this.uraian = uraian;
        this.jumlah = jumlah;
    }

    public double getJumlah() {
        return jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }
    
    public String getJumlahString() {
        if( jumlah==0 ){
            if(kode.trim().length()<=1) return "";
            else return String.valueOf(jumlah);
        }
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(jumlah);//bd.toPlainString();
        }
    }

    public void setJumlahString(String jumlah) {        
        this.jumlah = Double.parseDouble(jumlah);
    }

    public String getKode() {
        return kode;
    }

//    public void setKodeView(String kode) {
////        if( kode.trim().startsWith(uraian))
////        this.kode = kode;
//    }
//    
//    public String getKodeView() {
//        if( kode.trim().startsWith("5") ) {
//            return "2"+kode.substring(1).trim();
//        }
//        else if( kode.trim().startsWith("4") ) {
//            return "1"+kode.substring(1).trim();
//        }
//        else if( kode.trim().startsWith("6") ) {
//            return "3"+kode.substring(1).trim();
//        }
//        else return kode;
//    }

    public void setKode(String kode) {
        if( kode!=null && !kode.trim().equals("")){
            if( kode.trim().startsWith("5") ) this.kode = "2"+kode.substring(1).trim();
            else if( kode.trim().startsWith("4") ) this.kode= "1"+kode.substring(1).trim();        
            else if( kode.trim().startsWith("6") ) this.kode = "3"+kode.substring(1).trim();        
            else this.kode = kode;
        }
        else this.kode = kode;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }
    
}
