/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class LampiranXIPerdaAPBD implements Serializable{
    private String kode;
    private String judulKegiatan;
    private double apbdThnSebelumnya;
    private double perubahanApbdThnSebelumnya;
    private double realisasiApbdThnSebelumnya;
    private double sisaApbdThnSebelumnya;
    private double sisaPerubahanApbdThnSebelumnya;

    public LampiranXIPerdaAPBD() {
    }

    public LampiranXIPerdaAPBD(String kode, String judulKegiatan, double apbdThnSebelumnya, double perubahanApbdThnSebelumnya, double realisasiApbdThnSebelumnya, double sisaApbdThnSebelumnya, double sisaPerubahanApbdThnSebelumnya) {
        this.kode = kode;
        this.judulKegiatan = judulKegiatan;
        this.apbdThnSebelumnya = apbdThnSebelumnya;
        this.perubahanApbdThnSebelumnya = perubahanApbdThnSebelumnya;
        this.realisasiApbdThnSebelumnya = realisasiApbdThnSebelumnya;
        this.sisaApbdThnSebelumnya = sisaApbdThnSebelumnya;
        this.sisaPerubahanApbdThnSebelumnya = sisaPerubahanApbdThnSebelumnya;
    }

    public double getSisaPerubahanApbdThnSebelumnya() {
        return sisaPerubahanApbdThnSebelumnya;
    }

    public void setSisaPerubahanApbdThnSebelumnya(double sisaPerubahanApbdThnSebelumnya) {
        this.sisaPerubahanApbdThnSebelumnya = sisaPerubahanApbdThnSebelumnya;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getJudulKegiatan() {
        return judulKegiatan;
    }

    public void setJudulKegiatan(String judulKegiatan) {
        this.judulKegiatan = judulKegiatan;
    }

    public double getApbdThnSebelumnya() {
        return apbdThnSebelumnya;
    }

    public void setApbdThnSebelumnya(double apbdThnSebelumnya) {
        this.apbdThnSebelumnya = apbdThnSebelumnya;
    }

    public double getPerubahanApbdThnSebelumnya() {
        return perubahanApbdThnSebelumnya;
    }

    public void setPerubahanApbdThnSebelumnya(double perubahanApbdThnSebelumnya) {
        this.perubahanApbdThnSebelumnya = perubahanApbdThnSebelumnya;
    }

    public double getRealisasiApbdThnSebelumnya() {
        return realisasiApbdThnSebelumnya;
    }

    public void setRealisasiApbdThnSebelumnya(double realisasiApbdThnSebelumnya) {
        this.realisasiApbdThnSebelumnya = realisasiApbdThnSebelumnya;
    }

    public double getSisaApbdThnSebelumnya() {
        return sisaApbdThnSebelumnya;
    }

    public void setSisaApbdThnSebelumnya(double sisaApbdThnSebelumnya) {
        this.sisaApbdThnSebelumnya = sisaApbdThnSebelumnya;
    }
    
    
    
}
