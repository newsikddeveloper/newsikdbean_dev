/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class LampiranVIIPerdaAPBD implements Serializable{
    private String uraian;
    private short thnPengakuan;
    private double jumlah;
    private double penambahan;
    private double pengurangan;

    public LampiranVIIPerdaAPBD() {
    }

    public LampiranVIIPerdaAPBD(String uraian, short thnPengakuan, double jumlah, double penambahan, double pengurangan) {
        this.uraian = uraian;
        this.thnPengakuan = thnPengakuan;
        this.jumlah = jumlah;
        this.penambahan = penambahan;
        this.pengurangan = pengurangan;
    }

    public double getPengurangan() {
        return pengurangan;
    }

    public void setPengurangan(double pengurangan) {
        this.pengurangan = pengurangan;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public short getThnPengakuan() {
        return thnPengakuan;
    }

    public void setThnPengakuan(short thnPengakuan) {
        this.thnPengakuan = thnPengakuan;
    }

    public double getJumlah() {
        return jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    public double getPenambahan() {
        return penambahan;
    }

    public void setPenambahan(double penambahan) {
        this.penambahan = penambahan;
    }

    
    
}
