/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class LampiranXPerdaAPBD implements Serializable{
    private String jenisAset;
    private double saldoThnSebelumnya;
    private double penambahanSaldoThnSkarang;
    private double penguranganSaldoThnSkarang;

    public LampiranXPerdaAPBD() {
    }

    public LampiranXPerdaAPBD(String jenisAset, double saldoThnSebelumnya, double penambahanSaldoThnSkarang, double penguranganSaldoThnSkarang) {
        this.jenisAset = jenisAset;
        this.saldoThnSebelumnya = saldoThnSebelumnya;
        this.penambahanSaldoThnSkarang = penambahanSaldoThnSkarang;
        this.penguranganSaldoThnSkarang = penguranganSaldoThnSkarang;
    }

    public double getPenguranganSaldoThnSkarang() {
        return penguranganSaldoThnSkarang;
    }

    public void setPenguranganSaldoThnSkarang(double penguranganSaldoThnSkarang) {
        this.penguranganSaldoThnSkarang = penguranganSaldoThnSkarang;
    }

    public String getJenisAset() {
        return jenisAset;
    }

    public void setJenisAset(String jenisAset) {
        this.jenisAset = jenisAset;
    }

    public double getSaldoThnSebelumnya() {
        return saldoThnSebelumnya;
    }

    public void setSaldoThnSebelumnya(double saldoThnSebelumnya) {
        this.saldoThnSebelumnya = saldoThnSebelumnya;
    }

    public double getPenambahanSaldoThnSkarang() {
        return penambahanSaldoThnSkarang;
    }

    public void setPenambahanSaldoThnSkarang(double penambahanSaldoThnSkarang) {
        this.penambahanSaldoThnSkarang = penambahanSaldoThnSkarang;
    }
    
    
    
    
}
