/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class LampiranIXPerdaAPBD implements Serializable{
    private String jenisAsetTetap;
    private double saldoThnSebelumnya;
    private double penambahanSaldoThnSekarang;
    private double penguranganSaldoThnSekarang;

    public LampiranIXPerdaAPBD() {
    }

    public LampiranIXPerdaAPBD(String jenisAsetTetap, double saldoThnSebelumnya, double penambahanSaldoThnSekarang, double penguranganSaldoThnSekarang) {
        this.jenisAsetTetap = jenisAsetTetap;
        this.saldoThnSebelumnya = saldoThnSebelumnya;
        this.penambahanSaldoThnSekarang = penambahanSaldoThnSekarang;
        this.penguranganSaldoThnSekarang = penguranganSaldoThnSekarang;
    }

    public double getPenguranganSaldoThnSekarang() {
        return penguranganSaldoThnSekarang;
    }

    public void setPenguranganSaldoThnSekarang(double penguranganSaldoThnSekarang) {
        this.penguranganSaldoThnSekarang = penguranganSaldoThnSekarang;
    }

    public String getJenisAsetTetap() {
        return jenisAsetTetap;
    }

    public void setJenisAsetTetap(String jenisAsetTetap) {
        this.jenisAsetTetap = jenisAsetTetap;
    }

    public double getSaldoThnSebelumnya() {
        return saldoThnSebelumnya;
    }

    public void setSaldoThnSebelumnya(double saldoThnSebelumnya) {
        this.saldoThnSebelumnya = saldoThnSebelumnya;
    }

    public double getPenambahanSaldoThnSekarang() {
        return penambahanSaldoThnSekarang;
    }

    public void setPenambahanSaldoThnSekarang(double penambahanSaldoThnSekarang) {
        this.penambahanSaldoThnSekarang = penambahanSaldoThnSekarang;
    }
    
    

}
