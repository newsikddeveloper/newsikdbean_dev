/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class LampiranIIIPerdaAPBD extends LampiranIPerdaAPBD implements Serializable{
    private String dasarHukum;

    public LampiranIIIPerdaAPBD() {
    }

    public LampiranIIIPerdaAPBD(String kode, String uraian, double jumlah) {
        super(kode, uraian, jumlah);
    }

    public LampiranIIIPerdaAPBD(String dasarHukum, String kode, String uraian, double jumlah) {
        super(kode, uraian, jumlah);
        this.dasarHukum = dasarHukum;
    }

    public String getDasarHukum() {
        return dasarHukum;
    }

    public void setDasarHukum(String dasarHukum) {
        this.dasarHukum = dasarHukum;
    }
    
}
