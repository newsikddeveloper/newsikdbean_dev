/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class LampiranVPerdaAPBD extends LampiranIVPerdaAPBD implements Serializable{
    private double pegawaiTidakLangsung;
    private double belanjaLainnya;
    public LampiranVPerdaAPBD() {
    }

    public LampiranVPerdaAPBD(double pegawaiTidakLangsung, double belanjaLainnya, String kode, String urusanPemerintahan, double pegawai, double barangJasa, double modal) {
        super(kode, urusanPemerintahan, pegawai, barangJasa, modal);
        this.pegawaiTidakLangsung = pegawaiTidakLangsung;
        this.belanjaLainnya = belanjaLainnya;
    }

    public double getPegawaiTidakLangsung() {
        return pegawaiTidakLangsung;
    }

    public void setPegawaiTidakLangsung(double pegawaiTidakLangsung) {
        this.pegawaiTidakLangsung = pegawaiTidakLangsung;
    }

    public double getBelanjaLainnya() {
        return belanjaLainnya;
    }

    public void setBelanjaLainnya(double belanjaLainnya) {
        this.belanjaLainnya = belanjaLainnya;
    }
    
}
