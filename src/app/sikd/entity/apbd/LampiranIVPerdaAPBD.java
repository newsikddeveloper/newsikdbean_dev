/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class LampiranIVPerdaAPBD implements Serializable{
    private String kode;
    private String urusanPemerintahan;
    private double pegawai;
    private double barangJasa;
    private double modal;

    public LampiranIVPerdaAPBD() {
    }

    public LampiranIVPerdaAPBD(String kode, String urusanPemerintahan, double pegawai, double barangJasa, double modal) {
        this.kode = kode;
        this.urusanPemerintahan = urusanPemerintahan;
        this.pegawai = pegawai;
        this.barangJasa = barangJasa;
        this.modal = modal;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getUrusanPemerintahan() {
        return urusanPemerintahan;
    }

    public void setUrusanPemerintahan(String urusanPemerintahan) {
        this.urusanPemerintahan = urusanPemerintahan;
    }

    public double getPegawai() {
        return pegawai;
    }

    public void setPegawai(double pegawai) {
        this.pegawai = pegawai;
    }

    public double getBarangJasa() {
        return barangJasa;
    }

    public void setBarangJasa(double barangJasa) {
        this.barangJasa = barangJasa;
    }

    public double getModal() {
        return modal;
    }

    public void setModal(double modal) {
        this.modal = modal;
    }
}
