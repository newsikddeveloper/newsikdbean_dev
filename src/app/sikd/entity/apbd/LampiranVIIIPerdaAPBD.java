/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class LampiranVIIIPerdaAPBD implements Serializable{
    private short thnPenyertaan;
    private String namaBadan;
    private String dasarHukum;
    private String bentuk;
    private double jumlah;
    private double jumlahThnSebelumnya;
    private double penyertaanModalThnIni;
    private double hasilPenyertaanModal;
    private double jumlahModalAkanDiterimaThnIni;

    public LampiranVIIIPerdaAPBD() {
    }

    public LampiranVIIIPerdaAPBD(short thnPenyertaan, String namaBadan, String dasarHukum, String bentuk, double jumlah, double jumlahThnSebelumnya, double penyertaanModalThnIni, double hasilPenyertaanModal, double jumlahModalAkanDiterimaThnIni) {
        this.thnPenyertaan = thnPenyertaan;
        this.namaBadan = namaBadan;
        this.dasarHukum = dasarHukum;
        this.bentuk = bentuk;
        this.jumlah = jumlah;
        this.jumlahThnSebelumnya = jumlahThnSebelumnya;
        this.penyertaanModalThnIni = penyertaanModalThnIni;
        this.hasilPenyertaanModal = hasilPenyertaanModal;
        this.jumlahModalAkanDiterimaThnIni = jumlahModalAkanDiterimaThnIni;
    }

    public double getJumlahModalAkanDiterimaThnIni() {
        return jumlahModalAkanDiterimaThnIni;
    }

    public void setJumlahModalAkanDiterimaThnIni(double jumlahModalAkanDiterimaThnIni) {
        this.jumlahModalAkanDiterimaThnIni = jumlahModalAkanDiterimaThnIni;
    }

    public short getThnPenyertaan() {
        return thnPenyertaan;
    }

    public void setThnPenyertaan(short thnPenyertaan) {
        this.thnPenyertaan = thnPenyertaan;
    }

    public String getNamaBadan() {
        return namaBadan;
    }

    public void setNamaBadan(String namaBadan) {
        this.namaBadan = namaBadan;
    }

    public String getDasarHukum() {
        return dasarHukum;
    }

    public void setDasarHukum(String dasarHukum) {
        this.dasarHukum = dasarHukum;
    }

    public String getBentuk() {
        return bentuk;
    }

    public void setBentuk(String bentuk) {
        this.bentuk = bentuk;
    }

    public double getJumlah() {
        return jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    public double getJumlahThnSebelumnya() {
        return jumlahThnSebelumnya;
    }

    public void setJumlahThnSebelumnya(double jumlahThnSebelumnya) {
        this.jumlahThnSebelumnya = jumlahThnSebelumnya;
    }

    public double getPenyertaanModalThnIni() {
        return penyertaanModalThnIni;
    }

    public void setPenyertaanModalThnIni(double penyertaanModalThnIni) {
        this.penyertaanModalThnIni = penyertaanModalThnIni;
    }

    public double getHasilPenyertaanModal() {
        return hasilPenyertaanModal;
    }

    public void setHasilPenyertaanModal(double hasilPenyertaanModal) {
        this.hasilPenyertaanModal = hasilPenyertaanModal;
    }
    
    
    
}
