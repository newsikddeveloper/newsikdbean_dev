/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 *
 * @author sora
 */
public class LampiranILra implements Serializable{
    private String kodeAkun;
    private String namaAkun;
    private double nilaiAkunApbd;
    private double nilaiAkunLra;
    private String fontStyle;

    public LampiranILra() {
    }

    public LampiranILra(String kodeAkun, String namaAkun, double nilaiAkunApbd, double nilaiAkunLra) {
        this.kodeAkun = kodeAkun;
        this.namaAkun = namaAkun;
        this.nilaiAkunApbd = nilaiAkunApbd;
        this.nilaiAkunLra = nilaiAkunLra;
    }

    public LampiranILra(String kodeAkun, String namaAkun, double nilaiAkunApbd, double nilaiAkunLra, String fontStyle) {
        this.kodeAkun = kodeAkun;
        this.namaAkun = namaAkun;
        this.nilaiAkunApbd = nilaiAkunApbd;
        this.nilaiAkunLra = nilaiAkunLra;
        this.fontStyle = fontStyle;
    }
    
    public String getNilaiAkunApbdAsString(){
        if( nilaiAkunApbd==0 ){
            if(kodeAkun.trim().length()<=1) return "";
            else return String.valueOf(nilaiAkunApbd);
        }
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(nilaiAkunApbd);
        }
    }
    
    public void setNilaiAkunApbdAsString(String nilaiAkunApbdAsString){
        this.nilaiAkunApbd = Double.parseDouble(nilaiAkunApbdAsString);
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) {
        this.kodeAkun = kodeAkun;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) {
        this.namaAkun = namaAkun;
    }

    public double getNilaiAkunApbd() {
        return nilaiAkunApbd;
    }

    public void setNilaiAkunApbd(double nilaiAkunApbd) {
        this.nilaiAkunApbd = nilaiAkunApbd;
    }

    public double getNilaiAkunLra() {
        return nilaiAkunLra;
    }

    public void setNilaiAkunLra(double nilaiAkunLra) {
        this.nilaiAkunLra = nilaiAkunLra;
    }
    
    public String getNilaiAkunLraAsString(){
        if( nilaiAkunLra==0 ){
            if(kodeAkun.trim().length()<=1) return "";
            else return String.valueOf(nilaiAkunLra);
        }
        else{
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            
            return df.format(nilaiAkunLra);
        }
    }
    
    public void setNilaiAkunLraAsString(String nilaiAkunLraAsString){
        this.nilaiAkunLra = Double.parseDouble(nilaiAkunLraAsString);
    }

    public String getSelisihAsString(){
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            df.setNegativePrefix("( ");
            df.setNegativeSuffix(" )");
            if(kodeAkun==null || kodeAkun.trim().equals("")) return "";
            else return df.format(nilaiAkunLra-nilaiAkunApbd);
    }
    
    public void setSelisihAsString(String selisihAsString){
    }
    
    public String getPersentaseAsString(){        
        String result = "";
            Locale.setDefault(Locale.GERMAN);
            DecimalFormat df = new DecimalFormat("#,###,##0.00");
            df.setNegativePrefix("( ");
            df.setNegativeSuffix(" )");
            if(kodeAkun!=null && !kodeAkun.trim().equals("")){
                if(nilaiAkunApbd<=0) 
                    result= ""+'\u221e';
                else 
                    result= df.format((nilaiAkunLra/nilaiAkunApbd)*100) +" %";
            }
        return result;
    }
    
    public void setPersentaseAsString(String persentaseAsString){
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }
    
    
    
    
        
}
