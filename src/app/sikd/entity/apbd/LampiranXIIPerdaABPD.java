/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class LampiranXIIPerdaABPD implements Serializable{
    private String tujuan;
    private String dasarHukum;
    private double jumlahDana;
    private double saldoAwal;
    private double transferKasDaeah;
    private double saldoAkhir;
    private double sisaDana;

    public LampiranXIIPerdaABPD() {
    }

    public LampiranXIIPerdaABPD(String tujuan, String dasarHukum, double jumlahDana, double saldoAwal, double transferKasDaeah, double saldoAkhir, double sisaDana) {
        this.tujuan = tujuan;
        this.dasarHukum = dasarHukum;
        this.jumlahDana = jumlahDana;
        this.saldoAwal = saldoAwal;
        this.transferKasDaeah = transferKasDaeah;
        this.saldoAkhir = saldoAkhir;
        this.sisaDana = sisaDana;
    }

    public double getSisaDana() {
        return sisaDana;
    }

    public void setSisaDana(double sisaDana) {
        this.sisaDana = sisaDana;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public String getDasarHukum() {
        return dasarHukum;
    }

    public void setDasarHukum(String dasarHukum) {
        this.dasarHukum = dasarHukum;
    }

    public double getJumlahDana() {
        return jumlahDana;
    }

    public void setJumlahDana(double jumlahDana) {
        this.jumlahDana = jumlahDana;
    }

    public double getSaldoAwal() {
        return saldoAwal;
    }

    public void setSaldoAwal(double saldoAwal) {
        this.saldoAwal = saldoAwal;
    }

    public double getTransferKasDaeah() {
        return transferKasDaeah;
    }

    public void setTransferKasDaeah(double transferKasDaeah) {
        this.transferKasDaeah = transferKasDaeah;
    }

    public double getSaldoAkhir() {
        return saldoAkhir;
    }

    public void setSaldoAkhir(double saldoAkhir) {
        this.saldoAkhir = saldoAkhir;
    }
    
    
    
}
