/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.apbd;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class LampiranVIPerdaAPBD implements Serializable{
    private String golongan;
    private int eselon1;
    private int eselon2;
    private int eselon3;
    private int eselon4;
    private int eselon5;
    private int fungsional;
    private int staff;

    public LampiranVIPerdaAPBD() {
    }

    public LampiranVIPerdaAPBD(String golongan, int eselon1, int eselon2, int eselon3, int eselon4, int eselon5, int fungsional, int staff) {
        this.golongan = golongan;
        this.eselon1 = eselon1;
        this.eselon2 = eselon2;
        this.eselon3 = eselon3;
        this.eselon4 = eselon4;
        this.eselon5 = eselon5;
        this.fungsional = fungsional;
        this.staff = staff;
    }

    public int getStaff() {
        return staff;
    }

    public void setStaff(int staff) {
        this.staff = staff;
    }

    public String getGolongan() {
        return golongan;
    }

    public void setGolongan(String golongan) {
        this.golongan = golongan;
    }

    public int getEselon1() {
        return eselon1;
    }

    public void setEselon1(int eselon1) {
        this.eselon1 = eselon1;
    }

    public int getEselon2() {
        return eselon2;
    }

    public void setEselon2(int eselon2) {
        this.eselon2 = eselon2;
    }

    public int getEselon3() {
        return eselon3;
    }

    public void setEselon3(int eselon3) {
        this.eselon3 = eselon3;
    }

    public int getEselon4() {
        return eselon4;
    }

    public void setEselon4(int eselon4) {
        this.eselon4 = eselon4;
    }

    public int getEselon5() {
        return eselon5;
    }

    public void setEselon5(int eselon5) {
        this.eselon5 = eselon5;
    }

    public int getFungsional() {
        return fungsional;
    }

    public void setFungsional(int fungsional) {
        this.fungsional = fungsional;
    }
    
    
    
}
