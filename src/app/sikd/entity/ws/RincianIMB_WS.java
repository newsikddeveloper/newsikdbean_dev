package app.sikd.entity.ws;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rincianImbWS", propOrder = {
    "nomorIzin",
    "tanggalIzin",
    "namaPemohon",
    "alamatPemohon",
    "npwpPemohon",
    "kppPemohon",
    "cabangNpwpPemohon",
    "lokasi",
    "luasBangunan",
    "luasTanah",
    "jumlahLantai",
    "fungsi",
    "statusTanah"    
})
public class RincianIMB_WS implements Serializable{
    private String nomorIzin;
    private Date tanggalIzin;
    private String namaPemohon;
    private String alamatPemohon;
    private String npwpPemohon;
    private String kppPemohon;
    private String cabangNpwpPemohon;
    private String lokasi;
    private double luasBangunan;
    private double luasTanah;
    private int jumlahLantai;
    private String fungsi;
    private String statusTanah;

    public RincianIMB_WS() {
    }
    
    public String getNomorIzin() {
        return nomorIzin;
    }

    public void setNomorIzin(String nomorIzin) {
        this.nomorIzin = nomorIzin;
    }

    public Date getTanggalIzin() {
        return tanggalIzin;
    }

    public void setTanggalIzin(Date tanggalIzin) {
        this.tanggalIzin = tanggalIzin;
    }

    public String getNamaPemohon() {
        return namaPemohon;
    }

    public void setNamaPemohon(String namaPemohon) {
        this.namaPemohon = namaPemohon;
    }

    public String getAlamatPemohon() {
        return alamatPemohon;
    }

    public void setAlamatPemohon(String alamatPemohon) {
        this.alamatPemohon = alamatPemohon;
    }

    public String getNpwpPemohon() {
        return npwpPemohon;
    }

    public void setNpwpPemohon(String npwpPemohon) {
        this.npwpPemohon = npwpPemohon;
    }

    public String getKppPemohon() {
        return kppPemohon;
    }

    public void setKppPemohon(String kppPemohon) {
        this.kppPemohon = kppPemohon;
    }

    public String getCabangNpwpPemohon() {
        return cabangNpwpPemohon;
    }

    public void setCabangNpwpPemohon(String cabangNpwpPemohon) {
        this.cabangNpwpPemohon = cabangNpwpPemohon;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public double getLuasBangunan() {
        return luasBangunan;
    }

    public void setLuasBangunan(double luasBangunan) {
        this.luasBangunan = luasBangunan;
    }

    public double getLuasTanah() {
        return luasTanah;
    }

    public void setLuasTanah(double luasTanah) {
        this.luasTanah = luasTanah;
    }

    public int getJumlahLantai() {
        return jumlahLantai;
    }

    public void setJumlahLantai(int jumlahLantai) {
        this.jumlahLantai = jumlahLantai;
    }

    public String getFungsi() {
        return fungsi;
    }

    public void setFungsi(String fungsi) {
        this.fungsi = fungsi;
    }

    public String getStatusTanah() {
        return statusTanah;
    }

    public void setStatusTanah(String statusTanah) {
        this.statusTanah = statusTanah;
    }
    
}
