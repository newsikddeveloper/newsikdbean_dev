package app.sikd.entity.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "arusKasSaldoWS", propOrder = {    
    "kasBUDAwal",
    "kasBUDAkhir",
    "kasBendaharaPengeluaranAwal",
    "kasBendaharaPenerimaanAwal",    
    "kasLainnya"
})
public class ArusKasSaldo_WS implements Serializable{
    private double kasBUDAwal;
    private double kasBUDAkhir;
    private double kasBendaharaPengeluaranAwal;
    private double kasBendaharaPenerimaanAwal;
    private double kasLainnya;

    public ArusKasSaldo_WS() {
    }

    public ArusKasSaldo_WS(double kasBUDAwal, double kasBUDAkhir, double kasBendaharaPengeluaranAwal, double kasBendaharaPenerimaanAwal, double kasLainnya) {
        this.kasBUDAwal = kasBUDAwal;
        this.kasBUDAkhir = kasBUDAkhir;
        this.kasBendaharaPengeluaranAwal = kasBendaharaPengeluaranAwal;
        this.kasBendaharaPenerimaanAwal = kasBendaharaPenerimaanAwal;
        this.kasLainnya = kasLainnya;
    }

    public double getKasLainnya() {
        return kasLainnya;
    }

    public void setKasLainnya(double kasLainnya) {
        this.kasLainnya = kasLainnya;
    }

    public double getKasBUDAwal() {
        return kasBUDAwal;
    }

    public void setKasBUDAwal(double kasBUDAwal) {
        this.kasBUDAwal = kasBUDAwal;
    }

    public double getKasBUDAkhir() {
        return kasBUDAkhir;
    }

    public void setKasBUDAkhir(double kasBUDAkhir) {
        this.kasBUDAkhir = kasBUDAkhir;
    }

    public double getKasBendaharaPengeluaranAwal() {
        return kasBendaharaPengeluaranAwal;
    }

    public void setKasBendaharaPengeluaranAwal(double kasBendaharaPengeluaranAwal) {
        this.kasBendaharaPengeluaranAwal = kasBendaharaPengeluaranAwal;
    }

    public double getKasBendaharaPenerimaanAwal() {
        return kasBendaharaPenerimaanAwal;
    }

    public void setKasBendaharaPenerimaanAwal(double kasBendaharaPenerimaanAwal) {
        this.kasBendaharaPenerimaanAwal = kasBendaharaPenerimaanAwal;
    }
    
    
}
