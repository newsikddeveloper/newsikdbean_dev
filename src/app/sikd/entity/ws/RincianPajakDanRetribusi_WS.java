package app.sikd.entity.ws;


import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rincianPajakDanRetribusiWS", propOrder = {
    "namaPungutan",
    "jenisPungutan",
    "dasarHukum",
    "jumlahPungutan"
})

public class RincianPajakDanRetribusi_WS implements Serializable{    
    private String namaPungutan;
    private short jenisPungutan;
    private String dasarHukum;
    private double jumlahPungutan;

    public RincianPajakDanRetribusi_WS() {
    }        

    public String getNamaPungutan() {
        return namaPungutan;
    }

    public void setNamaPungutan(String namaPungutan) {
        this.namaPungutan = namaPungutan;
    }
    
    public short getJenisPungutan() {
        return jenisPungutan;
    }

    public void setJenisPungutan(short jenisPungutan) throws SIKDServiceException{
        if( jenisPungutan < 1 || jenisPungutan > 2 ) throw new SIKDServiceException("Silahkan isi data Jenis Pungutan dengan menggunakan bilangan 1 atau 2\n Untuk keterangan lebih lanjut silahkan lihat Developer Guide");
        this.jenisPungutan = jenisPungutan;
    }

    public String getDasarHukum() {
        return dasarHukum;
    }

    public void setDasarHukum(String dasarHukum) {
        this.dasarHukum = dasarHukum;
    }

    public double getJumlahPungutan() {
        return jumlahPungutan;
    }

    public void setJumlahPungutan(double jumlahPungutan) {
        this.jumlahPungutan = jumlahPungutan;
    }
}