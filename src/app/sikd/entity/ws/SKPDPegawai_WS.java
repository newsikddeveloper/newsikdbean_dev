package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "skpdPegawaiWS", propOrder = {
    "kodeUrusan",
    "namaUrusan",
    "kodeSKPD",
    "namaSKPD",
    "rincians"
})
public class SKPDPegawai_WS implements Serializable {

    private String kodeUrusan;
    private String namaUrusan;
    private String kodeSKPD;
    private String namaSKPD;
    private List<RincianPegawai_WS> rincians;

    public SKPDPegawai_WS() {
    }

    public SKPDPegawai_WS(String kodeUrusan, String namaUrusan, String kodeSKPD, String namaSKPD) {
        this.kodeUrusan = kodeUrusan;
        this.namaUrusan = namaUrusan;
        this.kodeSKPD = kodeSKPD;
        this.namaSKPD = namaSKPD;
    }

    public String getNamaSKPD() {
        return namaSKPD;
    }

    public void setNamaSKPD(String namaSKPD) {
        this.namaSKPD = namaSKPD;
    }

    public String getKodeUrusan() {
        return kodeUrusan;
    }

    public void setKodeUrusan(String kodeUrusan) throws SIKDServiceException{
        if( kodeUrusan==null || kodeUrusan.trim().length()!=4) throw new SIKDServiceException("Silahkan isi data Kode Urusan dengan panjang 4 digit");
        this.kodeUrusan = kodeUrusan;
    }

    public String getNamaUrusan() {
        return namaUrusan;
    }

    public void setNamaUrusan(String namaUrusan) {
        this.namaUrusan = namaUrusan;
    }

    public String getKodeSKPD() {
        return kodeSKPD;
    }

    public void setKodeSKPD(String kodeSKPD) throws SIKDServiceException{
        if( kodeSKPD==null || kodeSKPD.trim().length()!=4) throw new SIKDServiceException("Silahkan isi data Kode SKPD dengan panjang 4 digit");
        this.kodeSKPD = kodeSKPD;
    }

    public List<RincianPegawai_WS> getRincians() {
        return rincians;
    }

    public void setRincians(List<RincianPegawai_WS> rincians) {
        this.rincians = rincians;
    }

}
