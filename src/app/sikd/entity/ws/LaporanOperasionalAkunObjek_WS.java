package app.sikd.entity.ws;


import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */

@XmlRootElement
@XmlType(name = "laporanOperasionalAkunObjekWS", propOrder = {
    "kodeAkun",
    "namaAkun",
    "nilai"
})
public class LaporanOperasionalAkunObjek_WS  implements Serializable{
    private String kodeAkun;
    private String namaAkun;
    private double nilai;
    
    public LaporanOperasionalAkunObjek_WS() {
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) throws SIKDServiceException{
        if( kodeAkun == null || kodeAkun.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi kode Rekening");
        this.kodeAkun = kodeAkun;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) throws SIKDServiceException{
        if( namaAkun == null || namaAkun.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi Nama Rekening");
        this.namaAkun = namaAkun;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }
}
