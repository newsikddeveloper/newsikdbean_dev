package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlRootElement
@XmlType(name = "SP2DKegiatanWS", propOrder = {    
    "kodeUrusanProgram",
    "namaUrusanProgram",
    "kodeUrusanPelaksana",
    "namaUrusanPelaksana",
    "kodeSKPD",
    "namaSKPD",
    "kodeProgram",
    "namaProgram",
    "kodeKegiatan",
    "namaKegiatan",    
    "akuns"    
})
public class SP2DKegiatan_WS implements Serializable{
    private String kodeUrusanProgram;
    private String namaUrusanProgram;
    private String kodeUrusanPelaksana;
    private String namaUrusanPelaksana;
    private String kodeSKPD;
    private String namaSKPD;
    private String kodeProgram;
    private String namaProgram;
    private String kodeKegiatan;
    private String namaKegiatan;
    private List<AkunDTHSKPD_WS> akuns;
    

    public SP2DKegiatan_WS() {
    }

    public String getKodeUrusanProgram()  throws SIKDServiceException{
        if( kodeUrusanProgram == null || kodeUrusanProgram.trim().equals("") || 
                kodeUrusanProgram.trim().length() > 5)
            throw new SIKDServiceException("Silahkan isi data Kode Urusan Program dengan panjang maksimal 5 digit");
        return kodeUrusanProgram;
    }

    public void setKodeUrusanProgram(String kodeUrusanProgram) throws SIKDServiceException{
        if( kodeUrusanProgram == null || kodeUrusanProgram.trim().length() > 5)
            throw new SIKDServiceException("Silahkan isi data Kode Urusan Program dengan panjang maksimal 5 digit");
        this.kodeUrusanProgram = kodeUrusanProgram;
    }

    public String getNamaUrusanProgram() throws SIKDServiceException{
        if( namaUrusanProgram == null || namaUrusanProgram.trim().equals("") )
            throw new SIKDServiceException("Silahkan isi data Nama Urusan Program");
        return namaUrusanProgram;
    }

    public void setNamaUrusanProgram(String namaUrusanProgram) throws SIKDServiceException{
        if( namaUrusanProgram == null || namaUrusanProgram.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Urusan Program");
        this.namaUrusanProgram = namaUrusanProgram;
    }

    public String getKodeUrusanPelaksana() throws SIKDServiceException{
        if( kodeUrusanPelaksana == null || kodeUrusanPelaksana.trim().equals("") || kodeUrusanPelaksana.trim().length() > 5)
            throw new SIKDServiceException("Silahkan isi data Kode Urusan Pelaksana dengan panjang maksimal 5 digit");
        return kodeUrusanPelaksana;
    }

    public void setKodeUrusanPelaksana(String kodeUrusanPelaksana) throws SIKDServiceException{
        if( kodeUrusanPelaksana == null || kodeUrusanPelaksana.trim().length() > 5)
            throw new SIKDServiceException("Silahkan isi data Kode Urusan Pelaksana dengan maksimal panjang 5 digit");
        this.kodeUrusanPelaksana = kodeUrusanPelaksana;
    }

    public String getNamaUrusanPelaksana() throws SIKDServiceException{
        if( namaUrusanPelaksana == null || namaUrusanPelaksana.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Urusan Pelaksana");
        return namaUrusanPelaksana;
    }

    public void setNamaUrusanPelaksana(String namaUrusanPelaksana) throws SIKDServiceException{
        if( namaUrusanPelaksana == null || namaUrusanPelaksana.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Urusan Pelaksana");
        this.namaUrusanPelaksana = namaUrusanPelaksana;
    }

    public String getKodeSKPD() throws SIKDServiceException {
        if (kodeSKPD == null || kodeSKPD.trim().length() > 4) {
            throw new SIKDServiceException("Silahkan isi data Kode SKPD dengan panjang maksimal 4 digit");
        } else {
            if (kodeSKPD == null) kodeSKPD = "";
            while (kodeSKPD.trim().length() < 4) {
                kodeSKPD = "0" + kodeSKPD;
            }
        }
        return kodeSKPD;
    }

    public void setKodeSKPD(String kodeSKPD) throws SIKDServiceException {
        if (kodeSKPD == null || kodeSKPD.trim().length() > 4) {
            throw new SIKDServiceException("Silahkan isi data Kode SKPD dengan panjang maksimal 4 digit");
        } else {
            this.kodeSKPD = kodeSKPD;
            if (this.kodeSKPD == null) this.kodeSKPD = "";
            while (this.kodeSKPD.trim().length() < 4) {
                this.kodeSKPD = "0" + this.kodeSKPD;
            }
        }
    }

    public String getNamaSKPD() throws SIKDServiceException{
        if( namaSKPD == null || namaSKPD.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama SKPD");
        return namaSKPD;
    }

    public void setNamaSKPD(String namaSKPD) throws SIKDServiceException{
        if( namaSKPD==null || namaSKPD.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama SKPD");
        this.namaSKPD = namaSKPD;
    }
    
    public String getKodeProgram() throws SIKDServiceException{
        if( kodeProgram != null && !kodeProgram.trim().equals("")){
            if( kodeProgram.trim().length() > 3 ) throw new SIKDServiceException("Silahkan isi data Kode Program dengan panjang maksimal 3 digit");
        }   
        if( kodeProgram == null ) this.kodeProgram = "";
        while (kodeProgram.trim().length() < 3) {
            kodeProgram = "0" + kodeProgram.trim();
        }
        return kodeProgram;
    }
    
    public void setKodeProgram(String kodeProgram) throws SIKDServiceException{
        if( kodeProgram != null && !kodeProgram.trim().equals("")){
            if( kodeProgram.trim().length() > 3 ) throw new SIKDServiceException("Silahkan isi data Kode Program dengan panjang maksimal 3 digit");
        }                   
            this.kodeProgram = kodeProgram;
            if( this.kodeProgram == null ) this.kodeProgram = "";
            while (this.kodeProgram.trim().length() < 3) {
                this.kodeProgram = "0" + this.kodeProgram.trim();
            }        
    }

    public String getNamaProgram() {
        if( namaProgram==null ) namaProgram = "";
        return namaProgram;
    }

    public void setNamaProgram(String namaProgram) {
        if( namaProgram==null ) this.namaProgram = "";
        else this.namaProgram = namaProgram;
    }

    public String getKodeKegiatan() throws SIKDServiceException{
        if( kodeKegiatan != null && !kodeKegiatan.trim().equals("")){
            if( kodeKegiatan.trim().length() > 6 ) throw new SIKDServiceException("Silahkan isi data Kode Kegiatan dengan panjang maksimal 6 digit");
        }
        if( kodeKegiatan==null ) kodeKegiatan = "";
        while (kodeKegiatan.trim().length() < 6) {
            kodeKegiatan = "0" + kodeKegiatan.trim();
        }
        return kodeKegiatan;
    }

    public void setKodeKegiatan(String kodeKegiatan) throws SIKDServiceException{
        if( kodeKegiatan != null && !kodeKegiatan.trim().equals("")){
            if( kodeKegiatan.trim().length() > 6 ) throw new SIKDServiceException("Silahkan isi data Kode Kegiatan dengan panjang maksimal 6 digit");
        }        
            this.kodeKegiatan = kodeKegiatan;
            if( this.kodeKegiatan==null ) this.kodeKegiatan = "";
            while (this.kodeKegiatan.trim().length() < 6) {
                this.kodeKegiatan = "0" + this.kodeKegiatan.trim();
            }
    }

    public String getNamaKegiatan() {
        if( namaKegiatan == null ) namaKegiatan = "";
        return namaKegiatan;
    }

    public void setNamaKegiatan(String namaKegiatan) {
        if( namaKegiatan == null ) this.namaKegiatan = "";
        else this.namaKegiatan = namaKegiatan;
    }

    public List<AkunDTHSKPD_WS> getAkuns() {
        return akuns;
    }

    public void setAkuns(List<AkunDTHSKPD_WS> akuns) {
        this.akuns = akuns;
    }    
    
}
