package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "neracaAkunUtamaWS", propOrder = {
    "kodeAkun",
    "namaAkun",
    "nilai",
    "akunKelompoks"
})
public class NeracaAkunUtama_WS implements Serializable {

    private String kodeAkun;
    private String namaAkun;
    private double nilai;
    List<NeracaAkunKelompok_WS> akunKelompoks;

    public NeracaAkunUtama_WS() {
    }

    public NeracaAkunUtama_WS(String kodeAkun, String namaAkun, double nilai) {
        this.kodeAkun = kodeAkun;
        this.namaAkun = namaAkun;
        this.nilai = nilai;
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) throws SIKDServiceException {
        if (kodeAkun == null || kodeAkun.trim().equals("") || kodeAkun.trim().length() > 1) {
            throw new SIKDServiceException("Silahkan isi Data Kode Rekening dengan panjang maximal 1 karakter");
        }
        this.kodeAkun = kodeAkun;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) throws SIKDServiceException {
        if (namaAkun == null || namaAkun.trim().equals("")) {
            throw new SIKDServiceException("Silahkan isi Data Nama Kode Rekening");
        }
        this.namaAkun = namaAkun;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public List<NeracaAkunKelompok_WS> getAkunKelompoks() {
        return akunKelompoks;
    }

    public void setAkunKelompoks(List<NeracaAkunKelompok_WS> akunKelompoks) {
        this.akunKelompoks = akunKelompoks;
    }
}
