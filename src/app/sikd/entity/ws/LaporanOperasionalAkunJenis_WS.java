package app.sikd.entity.ws;


import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */

@XmlRootElement
@XmlType(name = "laporanOperasionalAkunJenisWS", propOrder = {
    "kodeAkun",
    "namaAkun",
    "nilai",    
    "akunObjeks"
})
public class LaporanOperasionalAkunJenis_WS  implements Serializable{
    private String kodeAkun;
    private String namaAkun;
    private double nilai;
    private List<LaporanOperasionalAkunObjek_WS> akunObjeks;
    
    public LaporanOperasionalAkunJenis_WS() {
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) throws SIKDServiceException{
        if(kodeAkun==null || kodeAkun.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi kode Rekening Jenis");
        this.kodeAkun = kodeAkun;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) throws SIKDServiceException{
        if( namaAkun == null || namaAkun.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi nama Akun Jenis");
        this.namaAkun = namaAkun;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public List<LaporanOperasionalAkunObjek_WS> getAkunObjeks() {
        return akunObjeks;
    }

    public void setAkunObjeks(List<LaporanOperasionalAkunObjek_WS> akunObjeks) {
        this.akunObjeks = akunObjeks;
    }
    
    

}
