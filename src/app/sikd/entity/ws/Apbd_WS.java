package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */

@XmlRootElement
@XmlType(name = "apbdWS", propOrder = {
    "kodeSatker",
    "kodePemda",
    "namaPemda",
    "tahunAnggaran",
    "kodeData",
    "jenisCOA",
    "statusData",
    "nomorPerda",
    "tanggalPerda",
    "userName",
    "password",
    "namaAplikasi",
    "pengembangAplikasi",
    "kegiatans"
})
public class Apbd_WS implements Serializable{
    
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private short kodeData;
    private short jenisCOA;
    private short statusData;
    private String nomorPerda;
    private Date tanggalPerda;    
    private String userName;
    private String password;
    private String namaAplikasi;
    private String pengembangAplikasi;
    private List<KegiatanAPBD_WS> kegiatans;    

    public Apbd_WS() {
    }
    
    public Apbd_WS(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short kodeData, short jenisCOA, short statusData, String nomorPerda, Date tanggalPerda, String userName, String password, String namaAplikasi, String pengembangAplikasi) throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().equals("") ) throw new SIKDServiceException("Silahkan isi data Kode Satker");
        if( kodePemda == null || kodePemda.trim().equals("") )throw new SIKDServiceException("Silahkan isi data Kode Pemda");
        if( namaPemda == null || namaPemda.trim().equals("") )throw new SIKDServiceException("Silahkan isi data Nama Pemda");
        if(kodeData < 0 || kodeData > 1 ) throw new SIKDServiceException("silahkan isi Kode Data dengan angka 0 atau 1 \n Untuk keterangan lebih lanjut silahkan lihat developer guide");
        if(jenisCOA < 1 || jenisCOA > 2 ) throw new SIKDServiceException("silahkan isi jenis COA dengan angka 1 atau 2 \n Untuk keterangan lebih lanjut silahkan lihat developer guide");
        if(statusData < 0 || statusData > 4 ) throw new SIKDServiceException("silahkan isi status data dengan angka 0, 1, 2, 3 atau 4  \n Untuk keterangan lebih lanjut silahkan lihat developer guide");
        if( userName == null || userName.trim().equals("") )throw new SIKDServiceException("Silahkan isi data User Name Service");
        if(password == null || password.trim().equals("") ) throw new SIKDServiceException("silahkan isi data Password Service, untuk mendapatkan password Service silahkan lihat developer guide");
        if( namaAplikasi == null || namaAplikasi.trim().equals("") )throw new SIKDServiceException("Silahkan isi data Nama Aplikasi");
        if( pengembangAplikasi == null || pengembangAplikasi.trim().equals("") )throw new SIKDServiceException("Silahkan isi data Pengembang Aplikasi");
        
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda =  namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.kodeData = kodeData;
        this.jenisCOA = jenisCOA;
        this.statusData = statusData;
        this.nomorPerda = nomorPerda;
        this.tanggalPerda = tanggalPerda;
        this.userName = userName;
        this.password = password;
        this.namaAplikasi = namaAplikasi;
        this.pengembangAplikasi = pengembangAplikasi;
    }

    public String getKodeSatker() throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public short getKodeData() {
        return kodeData;
    }
    public void setKodeData(short kodeData) throws SIKDServiceException{
        if(kodeData < 0 || kodeData > 1 ) this.kodeData = 0;
        else this.kodeData = kodeData;
    }

    public List<KegiatanAPBD_WS> getKegiatans() {
        return kegiatans;
    }

    public void setKegiatans(List<KegiatanAPBD_WS> kegiatans) {
        this.kegiatans = kegiatans;
    }
    
    public short getStatusData() throws SIKDServiceException{
        if(statusData < 0 || statusData > 4 ) throw new SIKDServiceException("Silahkan isi Status Data dengan bilangan 0, 1, 2, 3, atau 4 \n untuk keterangan lebih lanjut silahkan baca developer guide");
        return statusData;
    }

    public void setStatusData(short statusData)  throws SIKDServiceException{
        if(statusData < 0 || statusData > 4 ) throw new SIKDServiceException("Silahkan isi Status Data dengan bilangan 0, 1, 2, 3, atau 4 \n untuk keterangan lebih lanjut silahkan baca developer guide");
        else this.statusData = statusData;
    }

    public String getUserName() throws SIKDServiceException{
        if(userName == null || userName.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data UserName");
        return userName;
    }

    public void setUserName(String userName)  throws SIKDServiceException{
        if(userName == null || userName.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data UserName");
        this.userName = userName;
    }

    public String getPassword() throws SIKDServiceException{
        if(password == null || password.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data Password");
        return password;
    }

    public void setPassword(String password) throws SIKDServiceException{
        if(password == null || password.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data Password");
        this.password = password;
    }

    public short getJenisCOA() throws SIKDServiceException{
        if(jenisCOA < 1 || jenisCOA > 2 ) throw new SIKDServiceException("Silahkan isi data jenis COA dengan bilangan 1 atau 2 \n untuk keterangan lebih lanjut silahkan baca developer guide");
        return jenisCOA;
    }

    public void setJenisCOA(short jenisCOA) throws SIKDServiceException{
        if(jenisCOA < 1 || jenisCOA > 2 ) throw new SIKDServiceException("Silahkan isi data jenis COA dengan bilangan 1 atau 2 \n untuk keterangan lebih lanjut silahkan baca developer guide");
        else this.jenisCOA = jenisCOA;
    }

    public String getNomorPerda() {
        if(nomorPerda == null ) nomorPerda="";
        return nomorPerda;
    }

    public void setNomorPerda(String nomorPerda) {
        if(nomorPerda == null ) nomorPerda="";
        this.nomorPerda = nomorPerda;
    }

    public Date getTanggalPerda() {
        return tanggalPerda;
    }

    public void setTanggalPerda(Date tanggalPerda) {
        this.tanggalPerda = tanggalPerda;
    }

    public String getNamaAplikasi() throws SIKDServiceException{
        if( namaAplikasi==null || namaAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Nama Aplikasi");
        return namaAplikasi;
    }

    public void setNamaAplikasi(String namaAplikasi) throws SIKDServiceException{
        if( namaAplikasi==null || namaAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Nama Aplikasi");
        this.namaAplikasi = namaAplikasi;
    }

    public String getPengembangAplikasi() throws SIKDServiceException {
        if( pengembangAplikasi==null || pengembangAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Pengembang Aplikasi");
        return pengembangAplikasi;
    }

    public void setPengembangAplikasi(String pengembangAplikasi) throws SIKDServiceException {
        if( pengembangAplikasi==null || pengembangAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Pengembang Aplikasi");
        this.pengembangAplikasi = pengembangAplikasi;
    }
    
}
