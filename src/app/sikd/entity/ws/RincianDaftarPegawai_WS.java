package app.sikd.entity.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rincianDaftarPegawai", propOrder = {
    "kodeGolongan",
    "kodeGrade",
    "kodeEselon",
    "jumlah"
})
public class RincianDaftarPegawai_WS implements Serializable{
    
    private short kodeGolongan;
    private short kodeGrade;
    private short kodeEselon;
    private int jumlah;

    public RincianDaftarPegawai_WS() {
    }

//    public String getKodeGolongan() {
//        return kodeGolongan;
//    }
//
//    public void setKodeGolongan(String kodeGolongan) throws SIKDServiceException{
//        if( kodeGolongan != null && !kodeGolongan.trim().equals("")){
//        if( kodeGolongan.trim().equalsIgnoreCase(SIKDUtil.GOLONGAN_I) ) this.kodeGolongan = SIKDUtil.GOLONGAN_I;
//        else if( kodeGolongan.trim().equalsIgnoreCase(SIKDUtil.GOLONGAN_II) ) this.kodeGolongan = SIKDUtil.GOLONGAN_II;
//        else if( kodeGolongan.trim().equalsIgnoreCase(SIKDUtil.GOLONGAN_III) ) this.kodeGolongan = SIKDUtil.GOLONGAN_III;
//        else if( kodeGolongan.trim().equalsIgnoreCase(SIKDUtil.GOLONGAN_IV) ) this.kodeGolongan = SIKDUtil.GOLONGAN_IV;
//        else 
//            throw new SIKDServiceException("Silahkan isi Kode Golongan dengan data {" + SIKDUtil.GOLONGAN_I + " atau " + SIKDUtil.GOLONGAN_II + " atau "+ SIKDUtil.GOLONGAN_III + " atau " + SIKDUtil.GOLONGAN_IV + "}");
//        }
//        else this.kodeGolongan = "";
//    }
//    
//    public void setKodeGolonganShort(short kodeGolongan) throws SIKDServiceException{
//        if( kodeGolongan==SIKDUtil.GOLONGAN_I_SHORT ) this.kodeGolongan = SIKDUtil.GOLONGAN_I;
//        else if( kodeGolongan==SIKDUtil.GOLONGAN_II_SHORT ) this.kodeGolongan = SIKDUtil.GOLONGAN_II;
//        else if( kodeGolongan==SIKDUtil.GOLONGAN_III_SHORT ) this.kodeGolongan = SIKDUtil.GOLONGAN_III;
//        else if( kodeGolongan==SIKDUtil.GOLONGAN_IV_SHORT ) this.kodeGolongan = SIKDUtil.GOLONGAN_IV;
//        else this.kodeGolongan = "";
//    }
//
//    public String getKodeGrade() {
//        return kodeGrade;
//    }
//
//    public void setKodeGrade(String kodeGrade) throws SIKDServiceException{
//        if( kodeGrade != null && !kodeGrade.trim().equals("")){
//        if( kodeGrade.trim().equalsIgnoreCase(SIKDUtil.GRADE_A) ) this.kodeGrade = SIKDUtil.GRADE_A;
//        else if( kodeGrade.trim().equalsIgnoreCase(SIKDUtil.GRADE_B) ) this.kodeGrade = SIKDUtil.GRADE_B;
//        else if( kodeGrade.trim().equalsIgnoreCase(SIKDUtil.GRADE_C) ) this.kodeGrade = SIKDUtil.GRADE_C;
//        else if( kodeGrade.trim().equalsIgnoreCase(SIKDUtil.GRADE_D) ) this.kodeGrade = SIKDUtil.GRADE_D;
//        else if( kodeGrade.trim().equalsIgnoreCase(SIKDUtil.GRADE_E) ) this.kodeGrade = SIKDUtil.GRADE_E;
//        else 
//            throw new SIKDServiceException("Silahkan isi Kode Grade dengan data {" + SIKDUtil.GRADE_A + " atau " + SIKDUtil.GRADE_B + " atau "+ SIKDUtil.GRADE_C + " atau " + SIKDUtil.GRADE_D + " atau " + SIKDUtil.GRADE_E + "}");
//        }
//        else this.kodeGrade = "";
//    }
//    
//    public void setKodeGradeShort(short kodeGrade) throws SIKDServiceException{
//        if( kodeGrade==SIKDUtil.GRADE_A_SHORT ) this.kodeGrade = SIKDUtil.GRADE_A;
//        else if( kodeGrade==SIKDUtil.GRADE_B_SHORT ) this.kodeGrade = SIKDUtil.GRADE_B;
//        else if( kodeGrade==SIKDUtil.GRADE_C_SHORT ) this.kodeGrade = SIKDUtil.GRADE_C;
//        else if( kodeGrade==SIKDUtil.GRADE_D_SHORT ) this.kodeGrade = SIKDUtil.GRADE_D;
//        else if( kodeGrade==SIKDUtil.GRADE_E_SHORT ) this.kodeGrade = SIKDUtil.GRADE_E;
//        else 
//            throw new SIKDServiceException("Silahkan isi Kode Grade dengan data {" + SIKDUtil.GRADE_A + " atau " + SIKDUtil.GRADE_B + " atau "+ SIKDUtil.GRADE_C + " atau " + SIKDUtil.GRADE_D + " atau " + SIKDUtil.GRADE_E + "}");    
//    }
//
//    public String getKodeEselon() {
//        return kodeEselon;
//    }
//
//    public void setKodeEselon(String kodeEselon) throws SIKDServiceException{
//        if( kodeEselon != null && !kodeEselon.trim().equals("")){
//        if( kodeEselon.trim().equalsIgnoreCase(SIKDUtil.ESELON_I) || kodeEselon.trim().equalsIgnoreCase(String.valueOf(SIKDUtil.ESELON_I_SHORT)) ) this.kodeEselon = SIKDUtil.ESELON_I;
//        else if( kodeEselon.trim().equalsIgnoreCase(SIKDUtil.ESELON_II) || kodeEselon.trim().equalsIgnoreCase(String.valueOf(SIKDUtil.ESELON_II_SHORT)) ) this.kodeEselon = SIKDUtil.ESELON_II;
//        else if( kodeEselon.trim().equalsIgnoreCase(SIKDUtil.ESELON_III) || kodeEselon.trim().equalsIgnoreCase(String.valueOf(SIKDUtil.ESELON_III_SHORT))) this.kodeEselon = SIKDUtil.ESELON_III;
//        else if( kodeEselon.trim().equalsIgnoreCase(SIKDUtil.ESELON_IV) || kodeEselon.trim().equalsIgnoreCase(String.valueOf(SIKDUtil.ESELON_IV_SHORT)) ) this.kodeEselon = SIKDUtil.ESELON_IV;
//        else if( kodeEselon.trim().equalsIgnoreCase(SIKDUtil.ESELON_V) || kodeEselon.trim().equalsIgnoreCase(String.valueOf(SIKDUtil.ESELON_V_SHORT)) ) this.kodeEselon = SIKDUtil.ESELON_V;
//        else if( kodeEselon.trim().equalsIgnoreCase(SIKDUtil.ESELON_STAF) || kodeEselon.trim().equalsIgnoreCase(String.valueOf(SIKDUtil.ESELON_STAF_SHORT)) ) this.kodeEselon = SIKDUtil.ESELON_STAF;
//        else if( kodeEselon.trim().equalsIgnoreCase(SIKDUtil.ESELON_FUNGSIONAL) || kodeEselon.trim().equalsIgnoreCase(String.valueOf(SIKDUtil.ESELON_FUNGSIONAL_SHORT)) ) this.kodeEselon = SIKDUtil.ESELON_FUNGSIONAL;
//        else 
//            throw new SIKDServiceException("Silahkan isi Kode eselon dengan data {" + SIKDUtil.ESELON_I_SHORT + " atau " + SIKDUtil.ESELON_II_SHORT + " atau "+ SIKDUtil.ESELON_III_SHORT + " atau " + SIKDUtil.ESELON_IV_SHORT + " atau " + SIKDUtil.ESELON_V_SHORT + " atau " + SIKDUtil.ESELON_STAF_SHORT + " atau " + SIKDUtil.ESELON_FUNGSIONAL_SHORT + "}");
//        }
//        else this.kodeEselon = "";
//    }
//    
//    public void setKodeEselonShort(short kodeEselon) throws SIKDServiceException{
//        if( kodeEselon==SIKDUtil.ESELON_I_SHORT ) this.kodeEselon = SIKDUtil.ESELON_I;
//        else if( kodeEselon==SIKDUtil.ESELON_II_SHORT ) this.kodeEselon = SIKDUtil.ESELON_II;
//        else if( kodeEselon==SIKDUtil.ESELON_III_SHORT ) this.kodeEselon = SIKDUtil.ESELON_III;
//        else if( kodeEselon==SIKDUtil.ESELON_IV_SHORT ) this.kodeEselon = SIKDUtil.ESELON_IV;
//        else if( kodeEselon==SIKDUtil.ESELON_V_SHORT ) this.kodeEselon = SIKDUtil.ESELON_V;
//        else if( kodeEselon==SIKDUtil.ESELON_STAF_SHORT ) this.kodeEselon = SIKDUtil.ESELON_STAF;
//        else if( kodeEselon==SIKDUtil.ESELON_FUNGSIONAL_SHORT ) this.kodeEselon = SIKDUtil.ESELON_FUNGSIONAL;
//        else 
//            throw new SIKDServiceException("Silahkan isi Kode Eselon dengan data {" + SIKDUtil.ESELON_I_SHORT + " atau " + SIKDUtil.ESELON_II_SHORT + " atau "+ SIKDUtil.ESELON_III_SHORT + " atau " + SIKDUtil.ESELON_IV_SHORT + " atau " + SIKDUtil.ESELON_V_SHORT + " atau " + SIKDUtil.ESELON_STAF_SHORT + " atau " + SIKDUtil.ESELON_FUNGSIONAL_SHORT + "}");
//    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public short getKodeGolongan() {
        return kodeGolongan;
    }

    public void setKodeGolongan(short kodeGolongan) {
        this.kodeGolongan = kodeGolongan;
    }

    public short getKodeGrade() {
        return kodeGrade;
    }

    public void setKodeGrade(short kodeGrade) {
        this.kodeGrade = kodeGrade;
    }

    public short getKodeEselon() {
        return kodeEselon;
    }

    public void setKodeEselon(short kodeEselon) {
        this.kodeEselon = kodeEselon;
    }
    
    
}
