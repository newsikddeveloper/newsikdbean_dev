package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */

@XmlRootElement
@XmlType(name = "laporanOperasionalAkunUtamaWS", propOrder = {
    "kodeAkun",
    "namaAkun",
    "nilai",    
    "akunKelompoks"
})
public class LaporanOperasionalAkunUtama_WS  implements Serializable{
    private String kodeAkun;
    private String namaAkun;
    private double nilai;
    private List<LaporanOperasionalAkunKelompok_WS> akunKelompoks;
    
    public LaporanOperasionalAkunUtama_WS() {
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) throws SIKDServiceException{
        if(kodeAkun==null || kodeAkun.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi kode Rekening Utama");
        this.kodeAkun = kodeAkun;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) throws SIKDServiceException{
        if( namaAkun == null || namaAkun.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi nama Akun Utama");
        this.namaAkun = namaAkun;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public List<LaporanOperasionalAkunKelompok_WS> getAkunKelompoks() {
        return akunKelompoks;
    }

    public void setAkunKelompoks(List<LaporanOperasionalAkunKelompok_WS> akunKelompoks) {
        this.akunKelompoks = akunKelompoks;
    }
    
    

}
