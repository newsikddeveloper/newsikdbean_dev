package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rincianPinjamanDaerahWS", propOrder = {
    "sumber",
    "dasarHukum",
    "tanggalPerjanjian",
    "jumlahPinjaman",
    "jangkaWaktu",
    "bunga",
    "tujuan",
    "bayarPokok",
    "bayarBunga",
    "sisaPokok",
    "sisaBunga"
})
public class RincianPinjamanDaerah_WS implements Serializable{
    
    String sumber;
    String dasarHukum;
    Date tanggalPerjanjian;
    double jumlahPinjaman;
    double jangkaWaktu;
    double bunga;
    String tujuan;
    double bayarPokok;
    double bayarBunga;
    double sisaPokok;
    double sisaBunga;

    public RincianPinjamanDaerah_WS() {
    }

    public String getSumber() {
        return sumber;
    }

    public void setSumber(String sumber) throws SIKDServiceException{
        if( sumber == null || sumber.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Sumber Pinjaman");
        this.sumber = sumber;
    }

    public String getDasarHukum() {
        return dasarHukum;
    }

    public void setDasarHukum(String dasarHukum) {
        this.dasarHukum = dasarHukum;
    }

    public Date getTanggalPerjanjian() {
        return tanggalPerjanjian;
    }

    public void setTanggalPerjanjian(Date tanggalPerjanjian) {
        this.tanggalPerjanjian = tanggalPerjanjian;
    }

    public double getJumlahPinjaman() {
        return jumlahPinjaman;
    }

    public void setJumlahPinjaman(double jumlahPinjaman) {
        this.jumlahPinjaman = jumlahPinjaman;
    }

    public double getJangkaWaktu() {
        return jangkaWaktu;
    }

    public void setJangkaWaktu(double jangkaWaktu) {
        this.jangkaWaktu = jangkaWaktu;
    }

    public double getBunga() {
        return bunga;
    }

    public void setBunga(double bunga) {
        this.bunga = bunga;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public double getBayarPokok() {
        return bayarPokok;
    }

    public void setBayarPokok(double bayarPokok) {
        this.bayarPokok = bayarPokok;
    }

    public double getBayarBunga() {
        return bayarBunga;
    }

    public void setBayarBunga(double bayarBunga) {
        this.bayarBunga = bayarBunga;
    }

    public double getSisaPokok() {
        return sisaPokok;
    }

    public void setSisaPokok(double sisaPokok) {
        this.sisaPokok = sisaPokok;
    }

    public double getSisaBunga() {
        return sisaBunga;
    }

    public void setSisaBunga(double sisaBunga) {
        this.sisaBunga = sisaBunga;
    }
}
