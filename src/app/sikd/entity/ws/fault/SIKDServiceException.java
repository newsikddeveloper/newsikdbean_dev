package app.sikd.entity.ws.fault;

import java.io.Serializable;
import javax.xml.ws.WebFault;

/**
 *
 * @author detra
 */
@WebFault(name="SIKDServiceFault")
public class SIKDServiceException extends Exception implements Serializable{ 
    SIKDServiceFault fault;

    public SIKDServiceException() {
    }

    public SIKDServiceException(SIKDServiceFault fault) {
        super(fault.getFaultDescription());
        this.fault = fault;
    }

    public SIKDServiceException(SIKDServiceFault fault, String message) {
        super(message);
        this.fault = fault;
    }

    public SIKDServiceException(SIKDServiceFault fault, String message, Throwable cause) {
        super(message, cause);
        this.fault = fault;
    }

    public SIKDServiceFault getFault() {
        return fault;
    }

    public SIKDServiceException(String message) {
        super(message);
    }

    public SIKDServiceException(String code, String message) {
        super(message);
        this.fault = new SIKDServiceFault();
        this.fault.setFaultDescription(message);
        this.fault.setFaultCode(code);
    }

    public SIKDServiceException(Throwable cause) {
        super(cause);
    }

    public SIKDServiceException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
