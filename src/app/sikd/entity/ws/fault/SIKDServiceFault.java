package app.sikd.entity.ws.fault;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class SIKDServiceFault implements Serializable{ 
    private String faultCode;
    private String faultDescription;

    public String getFaultCode() {
        return faultCode;
    }

    public void setFaultCode(String faultCode) {
        this.faultCode = faultCode;
    }

    public String getFaultDescription() {
        return faultDescription;
    }

    public void setFaultDescription(String faultDescription) {
        this.faultDescription = faultDescription;
    }
    
    
}
