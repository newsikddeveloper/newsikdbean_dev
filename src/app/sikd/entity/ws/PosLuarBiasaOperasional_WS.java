package app.sikd.entity.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */

@XmlRootElement
@XmlType(name = "posLuarBiasaOperasionalWS", propOrder = {
    "pendapatan",
    "beban"
})
public class PosLuarBiasaOperasional_WS  implements Serializable{
    private double pendapatan;
    private double beban;
    
    public PosLuarBiasaOperasional_WS() {
    }

    public double getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(double pendapatan) {
        this.pendapatan = pendapatan;
    }

    public double getBeban() {
        return beban;
    }

    public void setBeban(double beban) {
        this.beban = beban;
    }
    
}
