package app.sikd.entity.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rincianPerhitunganFihakKetigaWS", propOrder = {    
    "uraian",
    "pungutan",
    "setoran"
})
public class RincianPerhitunganFihakKetiga_WS implements Serializable{
    
    String uraian;
    private double pungutan;
    private double setoran;

    public RincianPerhitunganFihakKetiga_WS() {
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public double getPungutan() {
        return pungutan;
    }

    public void setPungutan(double pungutan) {
        this.pungutan = pungutan;
    }

    public double getSetoran() {
        return setoran;
    }

    public void setSetoran(double setoran) {
        this.setoran = setoran;
    }
    
    

}
