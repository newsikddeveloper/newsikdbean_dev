package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlRootElement
@XmlType(name = "akunDthSkpdWS", propOrder = {
    "kodeAkunUtama",
    "namaAkunUtama",
    "kodeAkunKelompok",
    "namaAkunKelompok",
    "kodeAkunJenis",
    "namaAkunJenis",
    "kodeAkunObjek",
    "namaAkunObjek",
    "kodeAkunRincian",
    "namaAkunRincian",
    "kodeAkunSubRincian",
    "namaAkunSubRincian",
    "nilaiRekening"
})
public class AkunDTHSKPD_WS implements Serializable{ 
    String kodeAkunUtama;
    String namaAkunUtama;
    String kodeAkunKelompok;
    String namaAkunKelompok;
    String kodeAkunJenis;
    String namaAkunJenis;
    String kodeAkunObjek;
    String namaAkunObjek;
    String kodeAkunRincian;    
    String namaAkunRincian;
    private String kodeAkunSubRincian;
    private String namaAkunSubRincian;
    private double nilaiRekening;

    public AkunDTHSKPD_WS() {
    }
    
    public String getKodeAkunUtama() {
        return kodeAkunUtama;
    }

    public void setKodeAkunUtama(String kodeAkunUtama) throws SIKDServiceException{
        if( kodeAkunUtama == null || kodeAkunUtama.trim().length() > 2)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Utama dengan panjang 2 digit");
        this.kodeAkunUtama = kodeAkunUtama;
    }

    public String getNamaAkunUtama() {
        return namaAkunUtama;
    }

    public void setNamaAkunUtama(String namaAkunUtama) throws SIKDServiceException{
        if( namaAkunUtama == null || namaAkunUtama.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Akun Utama");
        this.namaAkunUtama = namaAkunUtama;
    }

    public String getKodeAkunKelompok() {
        return kodeAkunKelompok;
    }

    public void setKodeAkunKelompok(String kodeAkunKelompok)  throws SIKDServiceException{
        if( kodeAkunKelompok == null || kodeAkunKelompok.trim().length() > 2)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Kelompok dengan panjang 2 digit");
        this.kodeAkunKelompok = kodeAkunKelompok;
    }

    public String getNamaAkunKelompok() {
        return namaAkunKelompok;
    }

    public void setNamaAkunKelompok(String namaAkunKelompok)  throws SIKDServiceException{
        if( namaAkunKelompok == null || namaAkunKelompok.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Akun Kelompok");
        this.namaAkunKelompok = namaAkunKelompok;
    }

    public String getKodeAkunJenis() {
        return kodeAkunJenis;
    }

    public void setKodeAkunJenis(String kodeAkunJenis)  throws SIKDServiceException{
        if( kodeAkunJenis == null || kodeAkunJenis.trim().length() > 3)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Jenis dengan panjang 3 digit");
        this.kodeAkunJenis = kodeAkunJenis;
    }

    public String getNamaAkunJenis() {
        return namaAkunJenis;
    }

    public void setNamaAkunJenis(String namaAkunJenis)  throws SIKDServiceException{
        if( namaAkunJenis == null || namaAkunJenis.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Akun Jenis");
        this.namaAkunJenis = namaAkunJenis;
    }

    public String getKodeAkunObjek() {
        return kodeAkunObjek;
    }

    public void setKodeAkunObjek(String kodeAkunObjek)  throws SIKDServiceException{
        if( kodeAkunObjek == null || kodeAkunObjek.trim().length() > 3)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Objek dengan panjang 3 digit");
        this.kodeAkunObjek = kodeAkunObjek;
    }

    public String getNamaAkunObjek() {
        return namaAkunObjek;
    }

    public void setNamaAkunObjek(String namaAkunObjek)  throws SIKDServiceException{
        if( namaAkunObjek == null || namaAkunObjek.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Akun Objek");
        this.namaAkunObjek = namaAkunObjek;
    }

    public String getKodeAkunRincian() {
        return kodeAkunRincian;
    }

    public void setKodeAkunRincian(String kodeAkunRincian)  throws SIKDServiceException{
        if( kodeAkunRincian == null || kodeAkunRincian.trim().length() > 3)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Rincian dengan panjang 3 digit");
        this.kodeAkunRincian = kodeAkunRincian;
    }

    public String getNamaAkunRincian() {
        return namaAkunRincian;
    }

    public void setNamaAkunRincian(String namaAkunRincian)  throws SIKDServiceException{
        if( namaAkunRincian == null || namaAkunRincian.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Akun Rincian");
        this.namaAkunRincian = namaAkunRincian;
    }
    
    public String getKodeAkunSubRincian() {
        if(kodeAkunSubRincian==null) kodeAkunSubRincian="";
        return kodeAkunSubRincian;
    }

    public void setKodeAkunSubRincian(String kodeAkunSubRincian) throws SIKDServiceException{
        if( kodeAkunSubRincian != null && kodeAkunSubRincian.trim().length() > 3)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Sub Rincian dengan panjang 3 digit");
        this.kodeAkunSubRincian = kodeAkunSubRincian;
    }

    public String getNamaAkunSubRincian() {
        if(namaAkunSubRincian==null) namaAkunSubRincian = "";
        return namaAkunSubRincian;
    }

    public void setNamaAkunSubRincian(String namaAkunSubRincian) {
        if(namaAkunSubRincian==null) namaAkunSubRincian="";
        this.namaAkunSubRincian = namaAkunSubRincian;
    }

    public double getNilaiRekening() {
        return nilaiRekening;
    }

    public void setNilaiRekening(double nilaiRekening) {
        this.nilaiRekening = nilaiRekening;
    }
    
}