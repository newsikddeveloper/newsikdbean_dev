package app.sikd.entity.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */

@XmlRootElement
@XmlType(name = "defisitNonOperasionalWS", propOrder = {
    "penjualanAsetNonLancar",
    "kewajibanJangkaPanjang",
    "defisitLainnya"
})
public class DefisitNonOperasional_WS  implements Serializable{
    
    private double penjualanAsetNonLancar;
    private double kewajibanJangkaPanjang;
    private double defisitLainnya;
    
    public DefisitNonOperasional_WS() {
    }

    public double getPenjualanAsetNonLancar() {
        return penjualanAsetNonLancar;
    }

    public void setPenjualanAsetNonLancar(double penjualanAsetNonLancar) {
        this.penjualanAsetNonLancar = penjualanAsetNonLancar;
    }

    public double getKewajibanJangkaPanjang() {
        return kewajibanJangkaPanjang;
    }

    public void setKewajibanJangkaPanjang(double kewajibanJangkaPanjang) {
        this.kewajibanJangkaPanjang = kewajibanJangkaPanjang;
    }

    public double getDefisitLainnya() {
        return defisitLainnya;
    }

    public void setDefisitLainnya(double defisitLainnya) {
        this.defisitLainnya = defisitLainnya;
    }
    
    

}
