package app.sikd.entity.ws;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rincianBphtbWS", propOrder = {
    "namaPenerima",
    "alamatPenerima",
    "npwpPenerima",
    "kppPenerima",
    "cabangNpwpPenerima",
    "alamatObjek",
    "nilaiPerolehan",
    "luasTanah",
    "luasBangunan",
    "tanggalTransaksi",
    "nilaiBphtb"
})
public class RincianBPHTB_WS implements Serializable{
    private String namaPenerima;
    private String alamatPenerima;
    private String npwpPenerima;
    private String kppPenerima;
    private String cabangNpwpPenerima;
    private String alamatObjek;
    private double nilaiPerolehan;
    private double luasTanah;
    private double luasBangunan;
    private Date tanggalTransaksi;
    private double nilaiBphtb;

    public RincianBPHTB_WS() {
    }    

    public String getNamaPenerima() {
        return namaPenerima;
    }

    public void setNamaPenerima(String namaPenerima) {
        this.namaPenerima = namaPenerima;
    }

    public String getAlamatPenerima() {
        return alamatPenerima;
    }

    public void setAlamatPenerima(String alamatPenerima) {
        this.alamatPenerima = alamatPenerima;
    }

    public String getNpwpPenerima() {
        return npwpPenerima;
    }

    public void setNpwpPenerima(String npwpPenerima) {
        this.npwpPenerima = npwpPenerima;
    }

    public String getKppPenerima() {
        return kppPenerima;
    }

    public void setKppPenerima(String kppPenerima) {
        this.kppPenerima = kppPenerima;
    }

    public String getCabangNpwpPenerima() {
        return cabangNpwpPenerima;
    }

    public void setCabangNpwpPenerima(String cabangNpwpPenerima) {
        this.cabangNpwpPenerima = cabangNpwpPenerima;
    }

    public String getAlamatObjek() {
        return alamatObjek;
    }

    public void setAlamatObjek(String alamatObjek) {
        this.alamatObjek = alamatObjek;
    }

    public double getNilaiPerolehan() {
        return nilaiPerolehan;
    }

    public void setNilaiPerolehan(double nilaiPerolehan) {
        this.nilaiPerolehan = nilaiPerolehan;
    }

    public double getLuasTanah() {
        return luasTanah;
    }

    public void setLuasTanah(double luasTanah) {
        this.luasTanah = luasTanah;
    }

    public double getLuasBangunan() {
        return luasBangunan;
    }

    public void setLuasBangunan(double luasBangunan) {
        this.luasBangunan = luasBangunan;
    }

    public Date getTanggalTransaksi() {
        return tanggalTransaksi;
    }

    public void setTanggalTransaksi(Date tanggalTransaksi) {
        this.tanggalTransaksi = tanggalTransaksi;
    }

    public double getNilaiBphtb() {
        return nilaiBphtb;
    }

    public void setNilaiBphtb(double nilaiBphtb) {
        this.nilaiBphtb = nilaiBphtb;
    }
    
}
