package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rincianPegawaiWS", propOrder = {
    "nip",
    "namaPegawai",
    "npwpPegawai",
    "kodeStatus",
    "uraianStatus",
    "gender",
    "tanggalLahir",
    "tempatLahir",
    "kodeGolongan",
    "namaGolongan",
    "kodeStruktural",
    "namaStruktural",
    "kodeKelompokFungsional",
    "namaKelompokFungsional",
    "kodeJabatanFungsional",
    "namaJabatanFungsional",
    "kodeJabatanKhusus",
    "namaJabatanKhusus",
    "kodeGuru",
    "namaGuru",
    "kodeSertifikasi",
    "namaSertifikasi",
    "jumlahPasangan",
    "jumlahAnak",
    "gajiPokok",
    "persentaseGajiPokok",
    "tunjanganPasangan",
    "tunjanganAnak",
    "tunjanganPerbaikan",
    "tunjanganStruktural",
    "tunjanganFungsional",
    "tunjanganKhusus",
    "tunjanganUmum",
    "tunjanganKemahalan",
    "tunjanganPendidikan",
    "tunjanganTerpencil",
    "tunjanganAskes",
    "tunjanganPajak",
    "tunjanganPembulatan",
    "tunjanganBeras",
    "jumlahKotor",
    "iwp",
    "potonganAskes",
    "potonganBulog",
    "potonganPerum",
    "potonganPajak",
    "potonganSewarumah",
    "potonganUtang",
    "jumlahPotongan",
    "jumlahBersih"
})

public class RincianPegawai_WS implements Serializable {

    private String nip;
    private String namaPegawai;
    private String npwpPegawai;
    private String kodeStatus;
    private String uraianStatus;
    private short gender;
    private Date tanggalLahir;
    private String tempatLahir;
    private String kodeGolongan;
    private String namaGolongan;
    private String kodeStruktural;
    private String namaStruktural;
    private String kodeKelompokFungsional;
    private String namaKelompokFungsional;
    private String kodeJabatanFungsional;
    private String namaJabatanFungsional;
    private String kodeJabatanKhusus;
    private String namaJabatanKhusus;
    private String kodeGuru;
    private String namaGuru;
    private String kodeSertifikasi;
    private String namaSertifikasi;
    private short jumlahPasangan;
    private short jumlahAnak;
    private double gajiPokok;
    private double persentaseGajiPokok;
    private double tunjanganPasangan;
    private double tunjanganAnak;
    private double tunjanganPerbaikan;
    private double tunjanganStruktural;
    private double tunjanganFungsional;
    private double tunjanganKhusus;
    private double tunjanganUmum;
    private double tunjanganKemahalan;
    private double tunjanganPendidikan;
    private double tunjanganTerpencil;
    private double tunjanganAskes;
    private double tunjanganPajak;
    private double tunjanganPembulatan;
    private double tunjanganBeras;
    private double jumlahKotor;
    private double iwp;
    private double potonganAskes;
    private double potonganBulog;
    private double potonganPerum;
    private double potonganPajak;
    private double potonganSewarumah;
    private double potonganUtang;
    private double jumlahPotongan;
    private double jumlahBersih;

    public RincianPegawai_WS() {
    }

    public RincianPegawai_WS(String nip, String namaPegawai, String npwpPegawai, String kodeStatus, String uraianStatus, short gender, Date tanggalLahir, String tempatLahir, String kodeGolongan, String namaGolongan, String kodeStruktural, String namaStruktural, String kodeKelompokFungsional, String namaKelompokFungsional, String kodeJabatanFungsional, String namaJabatanFungsional, String kodeJabatanKhusus, String namaJabatanKhusus, String kodeGuru, String namaGuru, String kodeSertifikasi, String namaSertifikasi, short jumlahPasangan, short jumlahAnak, double gajiPokok, double persentaseGajiPokok, double tunjanganPasangan, double tunjanganAnak, double tunjanganPerbaikan, double tunjanganStruktural, double tunjanganFungsional, double tunjanganKhusus, double tunjanganUmum, double tunjanganKemahalan, double tunjanganPendidikan, double tunjanganTerpencil, double tunjanganAskes, double tunjanganPajak, double tunjanganPembulatan, double tunjanganBeras, double jumlahKotor, double iwp, double potonganAskes, double potonganBulog, double potonganPerum, double potonganPajak, double potonganSewarumah, double potonganUtang, double jumlahPotongan, double jumlahBersih) {
        this.nip = nip;
        this.namaPegawai = namaPegawai;
        this.npwpPegawai = npwpPegawai;
        this.kodeStatus = kodeStatus;
        this.uraianStatus = uraianStatus;
        this.gender = gender;
        this.tanggalLahir = tanggalLahir;
        this.tempatLahir = tempatLahir;
        this.kodeGolongan = kodeGolongan;
        this.namaGolongan = namaGolongan;
        this.kodeStruktural = kodeStruktural;
        this.namaStruktural = namaStruktural;
        this.kodeKelompokFungsional = kodeKelompokFungsional;
        this.namaKelompokFungsional = namaKelompokFungsional;
        this.kodeJabatanFungsional = kodeJabatanFungsional;
        this.namaJabatanFungsional = namaJabatanFungsional;
        this.kodeJabatanKhusus = kodeJabatanKhusus;
        this.namaJabatanKhusus = namaJabatanKhusus;
        this.kodeGuru = kodeGuru;
        this.namaGuru = namaGuru;
        this.kodeSertifikasi = kodeSertifikasi;
        this.namaSertifikasi = namaSertifikasi;
        this.jumlahPasangan = jumlahPasangan;
        this.jumlahAnak = jumlahAnak;
        this.gajiPokok = gajiPokok;
        this.persentaseGajiPokok = persentaseGajiPokok;
        this.tunjanganPasangan = tunjanganPasangan;
        this.tunjanganAnak = tunjanganAnak;
        this.tunjanganPerbaikan = tunjanganPerbaikan;
        this.tunjanganStruktural = tunjanganStruktural;
        this.tunjanganFungsional = tunjanganFungsional;
        this.tunjanganKhusus = tunjanganKhusus;
        this.tunjanganUmum = tunjanganUmum;
        this.tunjanganKemahalan = tunjanganKemahalan;
        this.tunjanganPendidikan = tunjanganPendidikan;
        this.tunjanganTerpencil = tunjanganTerpencil;
        this.tunjanganAskes = tunjanganAskes;
        this.tunjanganPajak = tunjanganPajak;
        this.tunjanganPembulatan = tunjanganPembulatan;
        this.tunjanganBeras = tunjanganBeras;
        this.jumlahKotor = jumlahKotor;
        this.iwp = iwp;
        this.potonganAskes = potonganAskes;
        this.potonganBulog = potonganBulog;
        this.potonganPerum = potonganPerum;
        this.potonganPajak = potonganPajak;
        this.potonganSewarumah = potonganSewarumah;
        this.potonganUtang = potonganUtang;
        this.jumlahPotongan = jumlahPotongan;
        this.jumlahBersih = jumlahBersih;
    }

    public double getJumlahBersih() {
        return jumlahBersih;
    }

    public void setJumlahBersih(double jumlahBersih) {
        this.jumlahBersih = jumlahBersih;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNamaPegawai() {
        return namaPegawai;
    }

    public void setNamaPegawai(String namaPegawai) {
        this.namaPegawai = namaPegawai;
    }

    public String getNpwpPegawai() {
        return npwpPegawai;
    }

    public void setNpwpPegawai(String npwpPegawai) {
        this.npwpPegawai = npwpPegawai;
    }

    public String getKodeStatus() {
        return kodeStatus;
    }

    public void setKodeStatus(String kodeStatus) {
        this.kodeStatus = kodeStatus;
    }

    public String getUraianStatus() {
        return uraianStatus;
    }

    public void setUraianStatus(String uraianStatus) {
        this.uraianStatus = uraianStatus;
    }

    public short getGender() {
        return gender;
    }

    public void setGender(short gender) throws SIKDServiceException {
        if (gender < 1 || gender > 2) {
            throw new SIKDServiceException("Silahkan isi data  Jenis Kelamin dengan menggunakan bilangan 1 atau 2");
        }
        this.gender = gender;
    }

    public Date getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getKodeGolongan() {
        return kodeGolongan;
    }

    public void setKodeGolongan(String kodeGolongan) {
        this.kodeGolongan = kodeGolongan;
    }

    public String getNamaGolongan() {
        return namaGolongan;
    }

    public void setNamaGolongan(String namaGolongan) {
        this.namaGolongan = namaGolongan;
    }

    public String getKodeStruktural() {
        return kodeStruktural;
    }

    public void setKodeStruktural(String kodeStruktural) {
        this.kodeStruktural = kodeStruktural;
    }

    public String getNamaStruktural() {
        return namaStruktural;
    }

    public void setNamaStruktural(String namaStruktural) {
        this.namaStruktural = namaStruktural;
    }

    public String getKodeKelompokFungsional() {
        return kodeKelompokFungsional;
    }

    public void setKodeKelompokFungsional(String kodeKelompokFungsional) {
        this.kodeKelompokFungsional = kodeKelompokFungsional;
    }

    public String getNamaKelompokFungsional() {
        return namaKelompokFungsional;
    }

    public void setNamaKelompokFungsional(String namaKelompokFungsional) {
        this.namaKelompokFungsional = namaKelompokFungsional;
    }

    public String getKodeJabatanFungsional() {
        return kodeJabatanFungsional;
    }

    public void setKodeJabatanFungsional(String kodeJabatanFungsional) {
        this.kodeJabatanFungsional = kodeJabatanFungsional;
    }

    public String getNamaJabatanFungsional() {
        return namaJabatanFungsional;
    }

    public void setNamaJabatanFungsional(String namaJabatanFungsional) {
        this.namaJabatanFungsional = namaJabatanFungsional;
    }

    public String getKodeJabatanKhusus() {
        return kodeJabatanKhusus;
    }

    public void setKodeJabatanKhusus(String kodeJabatanKhusus) {
        this.kodeJabatanKhusus = kodeJabatanKhusus;
    }

    public String getNamaJabatanKhusus() {
        return namaJabatanKhusus;
    }

    public void setNamaJabatanKhusus(String namaJabatanKhusus) {
        this.namaJabatanKhusus = namaJabatanKhusus;
    }

    public String getKodeGuru() {
        return kodeGuru;
    }

    public void setKodeGuru(String kodeGuru) {
        this.kodeGuru = kodeGuru;
    }

    public String getNamaGuru() {
        return namaGuru;
    }

    public void setNamaGuru(String namaGuru) {
        this.namaGuru = namaGuru;
    }

    public String getKodeSertifikasi() {
        return kodeSertifikasi;
    }

    public void setKodeSertifikasi(String kodeSertifikasi) {
        this.kodeSertifikasi = kodeSertifikasi;
    }

    public String getNamaSertifikasi() {
        return namaSertifikasi;
    }

    public void setNamaSertifikasi(String namaSertifikasi) {
        this.namaSertifikasi = namaSertifikasi;
    }

    public short getJumlahPasangan() {
        return jumlahPasangan;
    }

    public void setJumlahPasangan(short jumlahPasangan) {
        this.jumlahPasangan = jumlahPasangan;
    }

    public short getJumlahAnak() {
        return jumlahAnak;
    }

    public void setJumlahAnak(short jumlahAnak) {
        this.jumlahAnak = jumlahAnak;
    }

    public double getGajiPokok() {
        return gajiPokok;
    }

    public void setGajiPokok(double gajiPokok) {
        this.gajiPokok = gajiPokok;
    }

    public double getPersentaseGajiPokok() {
        return persentaseGajiPokok;
    }

    public void setPersentaseGajiPokok(double persentaseGajiPokok) {
        this.persentaseGajiPokok = persentaseGajiPokok;
    }

    public double getTunjanganPasangan() {
        return tunjanganPasangan;
    }

    public void setTunjanganPasangan(double tunjanganPasangan) {
        this.tunjanganPasangan = tunjanganPasangan;
    }

    public double getTunjanganAnak() {
        return tunjanganAnak;
    }

    public void setTunjanganAnak(double tunjanganAnak) {
        this.tunjanganAnak = tunjanganAnak;
    }

    public double getTunjanganPerbaikan() {
        return tunjanganPerbaikan;
    }

    public void setTunjanganPerbaikan(double tunjanganPerbaikan) {
        this.tunjanganPerbaikan = tunjanganPerbaikan;
    }

    public double getTunjanganStruktural() {
        return tunjanganStruktural;
    }

    public void setTunjanganStruktural(double tunjanganStruktural) {
        this.tunjanganStruktural = tunjanganStruktural;
    }

    public double getTunjanganFungsional() {
        return tunjanganFungsional;
    }

    public void setTunjanganFungsional(double tunjanganFungsional) {
        this.tunjanganFungsional = tunjanganFungsional;
    }

    public double getTunjanganKhusus() {
        return tunjanganKhusus;
    }

    public void setTunjanganKhusus(double tunjanganKhusus) {
        this.tunjanganKhusus = tunjanganKhusus;
    }

    public double getTunjanganUmum() {
        return tunjanganUmum;
    }

    public void setTunjanganUmum(double tunjanganUmum) {
        this.tunjanganUmum = tunjanganUmum;
    }

    public double getTunjanganKemahalan() {
        return tunjanganKemahalan;
    }

    public void setTunjanganKemahalan(double tunjanganKemahalan) {
        this.tunjanganKemahalan = tunjanganKemahalan;
    }

    public double getTunjanganTerpencil() {
        return tunjanganTerpencil;
    }

    public void setTunjanganTerpencil(double tunjanganTerpencil) {
        this.tunjanganTerpencil = tunjanganTerpencil;
    }

    public double getTunjanganAskes() {
        return tunjanganAskes;
    }

    public void setTunjanganAskes(double tunjanganAskes) {
        this.tunjanganAskes = tunjanganAskes;
    }

    public double getTunjanganPajak() {
        return tunjanganPajak;
    }

    public void setTunjanganPajak(double tunjanganPajak) {
        this.tunjanganPajak = tunjanganPajak;
    }

    public double getTunjanganPembulatan() {
        return tunjanganPembulatan;
    }

    public void setTunjanganPembulatan(double tunjanganPembulatan) {
        this.tunjanganPembulatan = tunjanganPembulatan;
    }

    public double getTunjanganBeras() {
        return tunjanganBeras;
    }

    public void setTunjanganBeras(double tunjanganBeras) {
        this.tunjanganBeras = tunjanganBeras;
    }

    public double getJumlahKotor() {
        return jumlahKotor;
    }

    public void setJumlahKotor(double jumlahKotor) {
        this.jumlahKotor = jumlahKotor;
    }

    public double getIwp() {
        return iwp;
    }

    public void setIwp(double iwp) {
        this.iwp = iwp;
    }

    public double getPotonganAskes() {
        return potonganAskes;
    }

    public void setPotonganAskes(double potonganAskes) {
        this.potonganAskes = potonganAskes;
    }

    public double getPotonganBulog() {
        return potonganBulog;
    }

    public void setPotonganBulog(double potonganBulog) {
        this.potonganBulog = potonganBulog;
    }

    public double getPotonganPerum() {
        return potonganPerum;
    }

    public void setPotonganPerum(double potonganPerum) {
        this.potonganPerum = potonganPerum;
    }

    public double getPotonganPajak() {
        return potonganPajak;
    }

    public void setPotonganPajak(double potonganPajak) {
        this.potonganPajak = potonganPajak;
    }

    public double getPotonganSewarumah() {
        return potonganSewarumah;
    }

    public void setPotonganSewarumah(double potonganSewarumah) {
        this.potonganSewarumah = potonganSewarumah;
    }

    public double getPotonganUtang() {
        return potonganUtang;
    }

    public void setPotonganUtang(double potonganUtang) {
        this.potonganUtang = potonganUtang;
    }

    public double getJumlahPotongan() {
        return jumlahPotongan;
    }

    public void setJumlahPotongan(double jumlahPotongan) {
        this.jumlahPotongan = jumlahPotongan;
    }

    public double getTunjanganPendidikan() {
        return tunjanganPendidikan;
    }

    public void setTunjanganPendidikan(double tunjanganPendidikan) {
        this.tunjanganPendidikan = tunjanganPendidikan;
    }
    
    

}
