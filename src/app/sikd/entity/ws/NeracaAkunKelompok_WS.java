package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "neracaAkunKelompokWS", propOrder = {
    "kodeAkun",
    "namaAkun",
    "nilai",
    "akunJeniss"
})
public class NeracaAkunKelompok_WS implements Serializable {

    private String kodeAkun;
    private String namaAkun;
    private double nilai;
    List<NeracaAkunJenis_WS> akunJeniss;

    public NeracaAkunKelompok_WS() {
    }

    public NeracaAkunKelompok_WS(String kodeAkun, String namaAkun, double nilai) {
        this.kodeAkun = kodeAkun;
        this.namaAkun = namaAkun;
        this.nilai = nilai;
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) throws SIKDServiceException {
        if (kodeAkun == null || kodeAkun.trim().equals("") || kodeAkun.trim().length() > 1) {
            throw new SIKDServiceException("Silahkan isi Data Kode Rekening dengan maximal 1 karakter");
        }
        this.kodeAkun = kodeAkun;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) throws SIKDServiceException {
        if (namaAkun == null || namaAkun.trim().equals("")) {
            throw new SIKDServiceException("Silahkan isi Data Nama Rekening");
        }
        this.namaAkun = namaAkun;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public List<NeracaAkunJenis_WS> getAkunJeniss() {
        return akunJeniss;
    }

    public void setAkunJeniss(List<NeracaAkunJenis_WS> akunJeniss) {
        this.akunJeniss = akunJeniss;
    }
}
