package app.sikd.entity.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */

@XmlRootElement
@XmlType(name = "perubahanSalDetailWS", propOrder = {
    "salAwal",
    "penggunaanSal",
    "silpa",
    "koreksi",
    "lainLain"
})
public class PerubahanSalDetail_WS  implements Serializable{
    private double salAwal;
    private double penggunaanSal;
    private double silpa;
    private double koreksi;
    private double lainLain;
    
    
    public PerubahanSalDetail_WS() {
    }

    public double getSalAwal() {
        return salAwal;
    }

    public void setSalAwal(double salAwal) {
        this.salAwal = salAwal;
    }

    public double getPenggunaanSal() {
        return penggunaanSal;
    }

    public void setPenggunaanSal(double penggunaanSal) {
        this.penggunaanSal = penggunaanSal;
    }

    public double getSilpa() {
        return silpa;
    }

    public void setSilpa(double silpa) {
        this.silpa = silpa;
    }

    public double getKoreksi() {
        return koreksi;
    }

    public void setKoreksi(double koreksi) {
        this.koreksi = koreksi;
    }

    public double getLainLain() {
        return lainLain;
    }

    public void setLainLain(double lainLain) {
        this.lainLain = lainLain;
    }
    
    
}
