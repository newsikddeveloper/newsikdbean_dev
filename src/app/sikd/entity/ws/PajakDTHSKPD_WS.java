package app.sikd.entity.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pajakDthSkpdWS", propOrder = {
    "kodeAkunPajak",
    "namaAkunPajak",
    "jenisPajak",
    "nilaiPotongan"
})
public class PajakDTHSKPD_WS implements Serializable{
    private String kodeAkunPajak;
    private String namaAkunPajak;
    private short jenisPajak;
    private double nilaiPotongan;

    public PajakDTHSKPD_WS() {
    }

    public String getKodeAkunPajak() {
        return kodeAkunPajak;
    }

    public void setKodeAkunPajak(String kodeAkunPajak) {
        this.kodeAkunPajak = kodeAkunPajak;
    }

    public String getNamaAkunPajak() {
        return namaAkunPajak;
    }

    public void setNamaAkunPajak(String namaAkunPajak) {
        this.namaAkunPajak = namaAkunPajak;
    }

    public short getJenisPajak() {
        return jenisPajak;
    }

    public void setJenisPajak(short jenisPajak) {
        this.jenisPajak = jenisPajak;
    }

    public double getNilaiPotongan() {
        return nilaiPotongan;
    }

    public void setNilaiPotongan(double nilaiPotongan) {
        this.nilaiPotongan = nilaiPotongan;
    }
}