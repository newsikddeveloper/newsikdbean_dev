package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "kodeRekeningApbdWS", propOrder = {
    "kodeAkunUtama",
    "namaAkunUtama",
    "kodeAkunKelompok",
    "namaAkunKelompok",
    "kodeAkunJenis",
    "namaAkunJenis",
    "kodeAkunObjek",
    "namaAkunObjek",
    "kodeAkunRincian",
    "namaAkunRincian",
    "kodeAkunSub",
    "namaAkunSub",
    "nilaiAnggaran"
})
public class KodeRekeningAPBD_WS implements Serializable{
    
    private String kodeAkunUtama;
    private String namaAkunUtama;
    private String kodeAkunKelompok;
    private String namaAkunKelompok;
    private String kodeAkunJenis;
    private String namaAkunJenis;
    private String kodeAkunObjek;
    private String namaAkunObjek;
    private String kodeAkunRincian;
    private String namaAkunRincian;
    private String kodeAkunSub;
    private String namaAkunSub;
    private double nilaiAnggaran;
    

    public KodeRekeningAPBD_WS() {
    }

    public KodeRekeningAPBD_WS(String kodeAkunUtama, String namaAkunUtama, String kodeAkunKelompok, String namaAkunKelompok, String kodeAkunJenis, String namaAkunJenis, String kodeAkunObjek, String namaAkunObjek, String kodeAkunRincian, String namaAkunRincian, String kodeAkunSub, String namaAkunSub, double nilaiAnggaran) {
        this.kodeAkunUtama = kodeAkunUtama;
        this.namaAkunUtama = namaAkunUtama;
        this.kodeAkunKelompok = kodeAkunKelompok;
        this.namaAkunKelompok = namaAkunKelompok;
        this.kodeAkunJenis = kodeAkunJenis;
        this.namaAkunJenis = namaAkunJenis;
        this.kodeAkunObjek = kodeAkunObjek;
        this.namaAkunObjek = namaAkunObjek;
        this.kodeAkunRincian = kodeAkunRincian;
        this.namaAkunRincian = namaAkunRincian;
        this.kodeAkunSub = kodeAkunSub;
        this.namaAkunSub = namaAkunSub;
        this.nilaiAnggaran = nilaiAnggaran;
    }

    public double getNilaiAnggaran() {
        return nilaiAnggaran;
    }

    public void setNilaiAnggaran(double nilaiAnggaran) {
        this.nilaiAnggaran = nilaiAnggaran;
    }

    public String getKodeAkunUtama() {
        return kodeAkunUtama;
    }

    public void setKodeAkunUtama(String kodeAkunUtama) throws SIKDServiceException{
        if( kodeAkunUtama == null || kodeAkunUtama.trim().equals("") || kodeAkunUtama.trim().length() > 2)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Utama dengan panjang maksimal 2 digit");
        this.kodeAkunUtama = kodeAkunUtama;
    }

    public String getNamaAkunUtama() {
        return namaAkunUtama;
    }

    public void setNamaAkunUtama(String namaAkunUtama) throws SIKDServiceException{
        if( namaAkunUtama == null || namaAkunUtama.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Akun Utama");
        this.namaAkunUtama = namaAkunUtama;
    }

    public String getKodeAkunKelompok() {
        return kodeAkunKelompok;
    }

    public void setKodeAkunKelompok(String kodeAkunKelompok)  throws SIKDServiceException{
        if( kodeAkunKelompok == null || kodeAkunKelompok.trim().equals("") || kodeAkunKelompok.trim().length() > 2)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Kelompok dengan panjang maksimal 2 digit");
        this.kodeAkunKelompok = kodeAkunKelompok;
    }

    public String getNamaAkunKelompok() {
        return namaAkunKelompok;
    }

    public void setNamaAkunKelompok(String namaAkunKelompok)  throws SIKDServiceException{
        if( namaAkunKelompok == null || namaAkunKelompok.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Akun Kelompok");
        this.namaAkunKelompok = namaAkunKelompok;
    }

    public String getKodeAkunJenis() {
        return kodeAkunJenis;
    }

    public void setKodeAkunJenis(String kodeAkunJenis)  throws SIKDServiceException{
        if( kodeAkunJenis == null || kodeAkunJenis.trim().equals("") || kodeAkunJenis.trim().length() > 3)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Jenis dengan panjang maksimal 3 digit");
        this.kodeAkunJenis = kodeAkunJenis;
    }

    public String getNamaAkunJenis() {
        return namaAkunJenis;
    }

    public void setNamaAkunJenis(String namaAkunJenis)  throws SIKDServiceException{
        if( namaAkunJenis == null || namaAkunJenis.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Akun Jenis");
        this.namaAkunJenis = namaAkunJenis;
    }

    public String getKodeAkunObjek() {
        return kodeAkunObjek;
    }

    public void setKodeAkunObjek(String kodeAkunObjek)  throws SIKDServiceException{
        if( kodeAkunObjek == null || kodeAkunObjek.trim().equals("") || kodeAkunObjek.trim().length() > 3)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Objek dengan panjang maksimal 3 digit");
        this.kodeAkunObjek = kodeAkunObjek;
    }

    public String getNamaAkunObjek() {
        return namaAkunObjek;
    }

    public void setNamaAkunObjek(String namaAkunObjek)  throws SIKDServiceException{
        if( namaAkunObjek == null || namaAkunObjek.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Akun Objek");
        this.namaAkunObjek = namaAkunObjek;
    }

    public String getKodeAkunRincian() {
        return kodeAkunRincian;
    }

    public void setKodeAkunRincian(String kodeAkunRincian)  throws SIKDServiceException{
        if( kodeAkunRincian == null || kodeAkunRincian.trim().equals("") || kodeAkunRincian.trim().length() > 3)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Rincian dengan panjang maksimal 3 digit");
        this.kodeAkunRincian = kodeAkunRincian;
    }

    public String getNamaAkunRincian() {
        return namaAkunRincian;
    }

    public void setNamaAkunRincian(String namaAkunRincian)  throws SIKDServiceException{
        if( namaAkunRincian == null || namaAkunRincian.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi data Nama Akun Rincian");
        this.namaAkunRincian = namaAkunRincian;
    }

    public String getKodeAkunSub() {
        if (kodeAkunSub==null) kodeAkunSub = "";
        return kodeAkunSub;
    }

    public void setKodeAkunSub(String kodeAkunSub)  throws SIKDServiceException{
        if( kodeAkunSub != null && !kodeAkunSub.trim().equals("")){
            if( kodeAkunSub.trim().length() > 3)
            throw new SIKDServiceException("Silahkan isi data Kode Akun Sub dengan panjang maksimal 3 digit");
        }
        if( kodeAkunSub == null ) this.kodeAkunSub = "";
        else  this.kodeAkunSub = kodeAkunSub;
    }

    public String getNamaAkunSub() {
        if( namaAkunSub == null ) namaAkunSub = "";
        return namaAkunSub;
    }

    public void setNamaAkunSub(String namaAkunSub) {
        if( namaAkunSub == null ) this.namaAkunSub = "";
        else this.namaAkunSub = namaAkunSub;
    }
    
}
