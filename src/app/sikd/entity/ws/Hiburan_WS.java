package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlRootElement
@XmlType(name = "hiburanWS", propOrder = {
    "kodeSatker",
    "kodePemda",
    "namaPemda",
    "tahunAnggaran", 
    "userName",
    "password",
    "statusData",
    "namaAplikasi",
    "pengembangAplikasi",
    "rincians"
})
public class Hiburan_WS  implements Serializable{
    
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private String userName;
    private String password;
    private short statusData;
    private String namaAplikasi;
    private String pengembangAplikasi;
    private List<RincianHiburan_WS> rincians;

    public Hiburan_WS() {
    }
    
    public String getKodeSatker() throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public List<RincianHiburan_WS> getRincians() {
        return rincians;
    }

    public void setRincians(List<RincianHiburan_WS> rincians) {
        this.rincians = rincians;
    }
    
    public String getUserName() throws SIKDServiceException{
        if(userName == null || userName.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data UserName");
        return userName;
    }

    public void setUserName(String userName)  throws SIKDServiceException{
        if(userName == null || userName.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data UserName");
        this.userName = userName;
    }

    public String getPassword() throws SIKDServiceException{
        if(password == null || password.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data Password");
        return password;
    }

    public void setPassword(String password) throws SIKDServiceException{
        if(password == null || password.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data Password");
        this.password = password;
    }
    
    public String getNamaAplikasi() throws SIKDServiceException{
        if( namaAplikasi==null || namaAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Nama Aplikasi");
        return namaAplikasi;
    }

    public void setNamaAplikasi(String namaAplikasi) throws SIKDServiceException{
        if( namaAplikasi==null || namaAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Nama Aplikasi");
        this.namaAplikasi = namaAplikasi;
    }

    public String getPengembangAplikasi() throws SIKDServiceException{
        if( pengembangAplikasi==null || pengembangAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Pengembang Aplikasi");
        return pengembangAplikasi;
    }

    public void setPengembangAplikasi(String pengembangAplikasi) throws SIKDServiceException{
        if( pengembangAplikasi==null || pengembangAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Pengembang Aplikasi");
        this.pengembangAplikasi = pengembangAplikasi;
    }

    public short getStatusData() throws SIKDServiceException{
        if(statusData < 0 || statusData > 3 ) throw new SIKDServiceException("Silahkan isi Status Data dengan bilangan 0, 1, 2, atau 3 \n untuk keterangan lebih lanjut silahkan baca developer guide");
        return statusData;
    }

    public void setStatusData(short statusData) throws SIKDServiceException{
        if(statusData < 0 || statusData > 3 ) throw new SIKDServiceException("Silahkan isi Status Data dengan bilangan 0, 1, 2, atau 3 \n untuk keterangan lebih lanjut silahkan baca developer guide");
        this.statusData = statusData;
    }
}
