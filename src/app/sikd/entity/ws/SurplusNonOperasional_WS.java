package app.sikd.entity.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */

@XmlRootElement
@XmlType(name = "surplusNonOperasionalWS", propOrder = {
    "penjualanAsetNonLancar",
    "kewajibanJangkaPanjang",
    "surplusLainnya"
})
public class SurplusNonOperasional_WS  implements Serializable{
    private double penjualanAsetNonLancar;
    private double kewajibanJangkaPanjang;
    private double surplusLainnya;
    
    public SurplusNonOperasional_WS() {
    }

    public double getPenjualanAsetNonLancar() {
        return penjualanAsetNonLancar;
    }

    public void setPenjualanAsetNonLancar(double penjualanAsetNonLancar) {
        this.penjualanAsetNonLancar = penjualanAsetNonLancar;
    }

    public double getKewajibanJangkaPanjang() {
        return kewajibanJangkaPanjang;
    }

    public void setKewajibanJangkaPanjang(double kewajibanJangkaPanjang) {
        this.kewajibanJangkaPanjang = kewajibanJangkaPanjang;
    }

    public double getSurplusLainnya() {
        return surplusLainnya;
    }

    public void setSurplusLainnya(double surplusLainnya) {
        this.surplusLainnya = surplusLainnya;
    }
    
    

}
