package app.sikd.entity.ws;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class Skpd_WS implements Serializable{
    private String kodeSKPD;
    private String namaSKPD;

    public Skpd_WS() {
    }

    public Skpd_WS(String kodeSKPD, String namaSKPD) {
        this.kodeSKPD = kodeSKPD;
        this.namaSKPD = namaSKPD;
    }

    public String getNamaSKPD() {
        return namaSKPD;
    }

    public void setNamaSKPD(String namaSKPD) {
        this.namaSKPD = namaSKPD;
    }

    public String getKodeSKPD() {
        return kodeSKPD;
    }

    public void setKodeSKPD(String kodeSKPD) {
        this.kodeSKPD = kodeSKPD;
    }
}
