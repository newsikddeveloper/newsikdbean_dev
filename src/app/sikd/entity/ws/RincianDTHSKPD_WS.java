package app.sikd.entity.ws;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlRootElement
@XmlType(name = "rincianDthSkpdWS", propOrder = {
    "nomorSPM",
    "nomorSP2D",
    "jenisSP2D",
    "tanggalSP2D",    
    "nilaiSP2D",
    "nilaiTotalPajak",
    "nilaiTotalPotongan",
    "npwpBUD",
    "npwpSKPD",
    "npwpPenerima",
    "namaPenerima",    
    "sumberDana",
    "subSumberDana",
    "tahapSalurDana",
    "keterangan",    
    "kegiatans",
    "pajaks"
})
public class RincianDTHSKPD_WS implements Serializable{
    private String nomorSPM;
    private String nomorSP2D;
    private short jenisSP2D;
    private Date tanggalSP2D;  
    private double nilaiSP2D;
    private double nilaiTotalPajak;
    private double nilaiTotalPotongan;
    private String npwpBUD;
    private String npwpSKPD;
    private String npwpPenerima;
    private String namaPenerima;
    private short sumberDana;
    private String subSumberDana;
    private short tahapSalurDana;
    private String keterangan;
    private List<SP2DKegiatan_WS> kegiatans;
    private List<PajakDTHSKPD_WS> pajaks;

    public RincianDTHSKPD_WS() {
    }

    public String getNomorSPM() {
        return nomorSPM;
    }

    public void setNomorSPM(String nomorSPM) {
        this.nomorSPM = nomorSPM;
    }

    public String getNomorSP2D() {
        return nomorSP2D;
    }

    public void setNomorSP2D(String nomorSP2D) {
        this.nomorSP2D = nomorSP2D;
    }

    public Date getTanggalSP2D() {
        return tanggalSP2D;
    }
    
    public void setTanggalSP2D(Date tanggalSP2D) {
        this.tanggalSP2D = tanggalSP2D;
    }

    public String getNpwpBUD() {
        return npwpBUD;
    }

    public void setNpwpBUD(String npwpBUD) {
        this.npwpBUD = npwpBUD;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public List<SP2DKegiatan_WS> getKegiatans() {
        return kegiatans;
    }

    public void setKegiatans(List<SP2DKegiatan_WS> kegiatans) {
        this.kegiatans = kegiatans;
    }

    public List<PajakDTHSKPD_WS> getPajaks() {
        return pajaks;
    }

    public void setPajaks(List<PajakDTHSKPD_WS> pajaks) {
        this.pajaks = pajaks;
    }

    public short getJenisSP2D() {
        return jenisSP2D;
    }

    public void setJenisSP2D(short jenisSP2D) {
        this.jenisSP2D = jenisSP2D;
    }

    public double getNilaiSP2D() {
        return nilaiSP2D;
    }

    public void setNilaiSP2D(double nilaiSP2D) {
        this.nilaiSP2D = nilaiSP2D;
    }

    public double getNilaiTotalPajak() {
        return nilaiTotalPajak;
    }

    public void setNilaiTotalPajak(double nilaiTotalPajak) {
        this.nilaiTotalPajak = nilaiTotalPajak;
    }

    public String getNpwpSKPD() {
        return npwpSKPD;
    }

    public void setNpwpSKPD(String npwpSKPD) {
        this.npwpSKPD = npwpSKPD;
    }

    public String getNpwpPenerima() {
        return npwpPenerima;
    }

    public void setNpwpPenerima(String npwpPenerima) {
        this.npwpPenerima = npwpPenerima;
    }

    public String getNamaPenerima() {
        return namaPenerima;
    }

    public void setNamaPenerima(String namaPenerima) {
        this.namaPenerima = namaPenerima;
    }

    public short getSumberDana() {
        return sumberDana;
    }

    public void setSumberDana(short sumberDana) {
        this.sumberDana = sumberDana;
    }

    public String getSubSumberDana() {
        return subSumberDana;
    }

    public void setSubSumberDana(String subSumberDana) {
        this.subSumberDana = subSumberDana;
    }

    public short getTahapSalurDana() {
        return tahapSalurDana;
    }

    public void setTahapSalurDana(short tahapSalurDana) {
        this.tahapSalurDana = tahapSalurDana;
    }

    public double getNilaiTotalPotongan() {
        return nilaiTotalPotongan;
    }

    public void setNilaiTotalPotongan(double nilaiTotalPotongan) {
        this.nilaiTotalPotongan = nilaiTotalPotongan;
    }
    
    

}