package app.sikd.entity.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */

@XmlRootElement
@XmlType(name = "perubahanEkuitasDetailWS", propOrder = {
    "ekuitasAwal",
    "surplusDefisitLO",
    "koreksiNilaiPersediaan",
    "selisihRevaluasiAset",
    "lainLain"
})
public class PerubahanEkuitasDetail_WS  implements Serializable{
    private double ekuitasAwal;
    private double surplusDefisitLO;
    private double koreksiNilaiPersediaan;
    private double selisihRevaluasiAset;
    private double lainLain;
    
    
    public PerubahanEkuitasDetail_WS() {
    }

    public double getEkuitasAwal() {
        return ekuitasAwal;
    }

    public void setEkuitasAwal(double ekuitasAwal) {
        this.ekuitasAwal = ekuitasAwal;
    }

    public double getSurplusDefisitLO() {
        return surplusDefisitLO;
    }

    public void setSurplusDefisitLO(double surplusDefisitLO) {
        this.surplusDefisitLO = surplusDefisitLO;
    }

    public double getSelisihRevaluasiAset() {
        return selisihRevaluasiAset;
    }

    public void setSelisihRevaluasiAset(double selisihRevaluasiAset) {
        this.selisihRevaluasiAset = selisihRevaluasiAset;
    }

    public double getLainLain() {
        return lainLain;
    }

    public void setLainLain(double lainLain) {
        this.lainLain = lainLain;
    }

    public double getKoreksiNilaiPersediaan() {
        return koreksiNilaiPersediaan;
    }

    public void setKoreksiNilaiPersediaan(double koreksiNilaiPersediaan) {
        this.koreksiNilaiPersediaan = koreksiNilaiPersediaan;
    }
    
}
