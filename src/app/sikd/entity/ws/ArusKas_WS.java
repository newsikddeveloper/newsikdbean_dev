package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(name = "arusKasWS", propOrder = {
    "kodeSatker",
    "kodePemda",
    "namaPemda",
    "tahunAnggaran",
    "judulArusKas",
    "userName",
    "password",
    "statusData",
    "namaAplikasi",
    "pengembangAplikasi",
    "arusMasukOperasis",
    "arusKeluarOperasis",
    "arusMasukInvestasis",
    "arusKeluarInvestasis",
    "arusMasukPembiayaans",
    "arusKeluarPembiayaans",
    "arusMasukNonAnggarans",
    "arusKeluarNonAnggarans",
    "arusKasSaldo"
})
public class ArusKas_WS  implements Serializable{
    
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private String judulArusKas;
    private String userName;
    private String password;
    private short statusData;
    private String namaAplikasi;
    private String pengembangAplikasi;
    private List<ArusMasukOperasi_WS> arusMasukOperasis;
    private List<ArusKeluarOperasi_WS> arusKeluarOperasis;
    private List<ArusMasukInvestasi_WS> arusMasukInvestasis;
    private List<ArusKeluarInvestasi_WS> arusKeluarInvestasis;
    private List<ArusMasukPembiayaan_WS> arusMasukPembiayaans;
    private List<ArusKeluarPembiayaan_WS> arusKeluarPembiayaans;
    private List<ArusMasukNonAnggaran_WS> arusMasukNonAnggarans;
    private List<ArusKeluarNonAnggaran_WS> arusKeluarNonAnggarans;
    private ArusKasSaldo_WS arusKasSaldo;
    
    
    

    public ArusKas_WS() {
    }
    
    public String getKodeSatker() throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public String getJudulArusKas() {
        return judulArusKas;
    }

    public void setJudulArusKas(String judulArusKas) {
        this.judulArusKas = judulArusKas;
    }

    public List<ArusMasukOperasi_WS> getArusMasukOperasis() {
        return arusMasukOperasis;
    }

    public void setArusMasukOperasis(List<ArusMasukOperasi_WS> arusMasukOperasis) {
        this.arusMasukOperasis = arusMasukOperasis;
    }

    public List<ArusKeluarOperasi_WS> getArusKeluarOperasis() {
        return arusKeluarOperasis;
    }

    public void setArusKeluarOperasis(List<ArusKeluarOperasi_WS> arusKeluarOperasis) {
        this.arusKeluarOperasis = arusKeluarOperasis;
    }

    public List<ArusMasukInvestasi_WS> getArusMasukInvestasis() {
        return arusMasukInvestasis;
    }

    public void setArusMasukInvestasis(List<ArusMasukInvestasi_WS> arusMasukInvestasis) {
        this.arusMasukInvestasis = arusMasukInvestasis;
    }

    public List<ArusKeluarInvestasi_WS> getArusKeluarInvestasis() {
        return arusKeluarInvestasis;
    }

    public void setArusKeluarInvestasis(List<ArusKeluarInvestasi_WS> arusKeluarInvestasis) {
        this.arusKeluarInvestasis = arusKeluarInvestasis;
    }

    public List<ArusMasukPembiayaan_WS> getArusMasukPembiayaans() {
        return arusMasukPembiayaans;
    }

    public void setArusMasukPembiayaans(List<ArusMasukPembiayaan_WS> arusMasukPembiayaans) {
        this.arusMasukPembiayaans = arusMasukPembiayaans;
    }

    public List<ArusKeluarPembiayaan_WS> getArusKeluarPembiayaans() {
        return arusKeluarPembiayaans;
    }

    public void setArusKeluarPembiayaans(List<ArusKeluarPembiayaan_WS> arusKeluarPembiayaans) {
        this.arusKeluarPembiayaans = arusKeluarPembiayaans;
    }

    public List<ArusMasukNonAnggaran_WS> getArusMasukNonAnggarans() {
        return arusMasukNonAnggarans;
    }

    public void setArusMasukNonAnggarans(List<ArusMasukNonAnggaran_WS> arusMasukNonAnggarans) {
        this.arusMasukNonAnggarans = arusMasukNonAnggarans;
    }

    public List<ArusKeluarNonAnggaran_WS> getArusKeluarNonAnggarans() {
        return arusKeluarNonAnggarans;
    }

    public void setArusKeluarNonAnggarans(List<ArusKeluarNonAnggaran_WS> arusKeluarNonAnggarans) {
        this.arusKeluarNonAnggarans = arusKeluarNonAnggarans;
    }

    public ArusKasSaldo_WS getArusKasSaldo() {
        return arusKasSaldo;
    }

    public void setArusKasSaldo(ArusKasSaldo_WS arusKasSaldo) {
        this.arusKasSaldo = arusKasSaldo;
    }
    
    public String getUserName() throws SIKDServiceException{
        if(userName == null || userName.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data UserName");
        return userName;
    }

    public void setUserName(String userName)  throws SIKDServiceException{
        if(userName == null || userName.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data UserName");
        this.userName = userName;
    }

    public String getPassword() throws SIKDServiceException{
        if(password == null || password.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data Password");
        return password;
    }

    public void setPassword(String password) throws SIKDServiceException{
        if(password == null || password.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data Password");
        this.password = password;
    }
    
    public String getNamaAplikasi() throws SIKDServiceException{
        if( namaAplikasi==null || namaAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Nama Aplikasi");
        return namaAplikasi;
    }

    public void setNamaAplikasi(String namaAplikasi) throws SIKDServiceException{
        if( namaAplikasi==null || namaAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Nama Aplikasi");
        this.namaAplikasi = namaAplikasi;
    }

    public String getPengembangAplikasi() throws SIKDServiceException{
        if( pengembangAplikasi==null || pengembangAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Pengembang Aplikasi");
        return pengembangAplikasi;
    }

    public void setPengembangAplikasi(String pengembangAplikasi) throws SIKDServiceException{
        if( pengembangAplikasi==null || pengembangAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Pengembang Aplikasi");
        this.pengembangAplikasi = pengembangAplikasi;
    }

    public short getStatusData() throws SIKDServiceException{
        if(statusData < 0 || statusData > 4 ) throw new SIKDServiceException("Silahkan isi Status Data dengan bilangan 0, 1, 2, 3, atau 4 \n untuk keterangan lebih lanjut silahkan baca developer guide");
        return statusData;
    }

    public void setStatusData(short statusData) throws SIKDServiceException{
        if(statusData < 0 || statusData > 4 ) throw new SIKDServiceException("Silahkan isi Status Data dengan bilangan 0, 1, 2, 3, atau 4 \n untuk keterangan lebih lanjut silahkan baca developer guide");
        this.statusData = statusData;
    }
}
