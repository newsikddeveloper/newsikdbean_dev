package app.sikd.entity.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author detra
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rincianRestoranWS", propOrder = {
    "namaRestoran",
    "alamat",
    "kapasitas",
    "namaPemilik",
    "alamatPemilik",
    "npwpPemilik",
    "kppPemilik",
    "cabangNpwpPemilik",
    "namaPengelola",
    "alamatPengelola",
    "npwpPengelola",
    "kppPengelola",
    "cabangNpwpPengelola",
    "jumlahPajak",
    "jumlahKaryawan"    
})
public class RincianRestoran_WS implements Serializable{    
    private String namaRestoran;
    private String alamat;
    private int kapasitas;
    private String namaPemilik;
    private String alamatPemilik;
    private String npwpPemilik;
    private String kppPemilik;
    private String cabangNpwpPemilik;
    private String namaPengelola;
    private String alamatPengelola;
    private String npwpPengelola;
    private String kppPengelola;
    private String cabangNpwpPengelola;
    private double jumlahPajak;
    private int jumlahKaryawan;

    public RincianRestoran_WS() {
    }
    
    public String getNamaRestoran() {
        return namaRestoran;
    }

    public void setNamaRestoran(String namaRestoran) {
        this.namaRestoran = namaRestoran;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getKapasitas() {
        return kapasitas;
    }

    public void setKapasitas(int kapasitas) {
        this.kapasitas = kapasitas;
    }

    public String getNamaPemilik() {
        return namaPemilik;
    }

    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }

    public String getAlamatPemilik() {
        return alamatPemilik;
    }

    public void setAlamatPemilik(String alamatPemilik) {
        this.alamatPemilik = alamatPemilik;
    }

    public String getNpwpPemilik() {
        return npwpPemilik;
    }

    public void setNpwpPemilik(String npwpPemilik) {
        this.npwpPemilik = npwpPemilik;
    }

    public String getKppPemilik() {
        return kppPemilik;
    }

    public void setKppPemilik(String kppPemilik) {
        this.kppPemilik = kppPemilik;
    }

    public String getCabangNpwpPemilik() {
        return cabangNpwpPemilik;
    }

    public void setCabangNpwpPemilik(String cabangNpwpPemilik) {
        this.cabangNpwpPemilik = cabangNpwpPemilik;
    }

    public String getNamaPengelola() {
        return namaPengelola;
    }

    public void setNamaPengelola(String namaPengelola) {
        this.namaPengelola = namaPengelola;
    }

    public String getAlamatPengelola() {
        return alamatPengelola;
    }

    public void setAlamatPengelola(String alamatPengelola) {
        this.alamatPengelola = alamatPengelola;
    }

    public String getNpwpPengelola() {
        return npwpPengelola;
    }

    public void setNpwpPengelola(String npwpPengelola) {
        this.npwpPengelola = npwpPengelola;
    }

    public String getKppPengelola() {
        return kppPengelola;
    }

    public void setKppPengelola(String kppPengelola) {
        this.kppPengelola = kppPengelola;
    }

    public String getCabangNpwpPengelola() {
        return cabangNpwpPengelola;
    }

    public void setCabangNpwpPengelola(String cabangNpwpPengelola) {
        this.cabangNpwpPengelola = cabangNpwpPengelola;
    }

    public double getJumlahPajak() {
        return jumlahPajak;
    }

    public void setJumlahPajak(double jumlahPajak) {
        this.jumlahPajak = jumlahPajak;
    }

    public int getJumlahKaryawan() {
        return jumlahKaryawan;
    }

    public void setJumlahKaryawan(int jumlahKaryawan) {
        this.jumlahKaryawan = jumlahKaryawan;
    }
}
