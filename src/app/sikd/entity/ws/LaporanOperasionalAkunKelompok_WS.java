package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author sora
 */

@XmlRootElement
@XmlType(name = "laporanOperasionalAkunKelompokWS", propOrder = {
    "kodeAkun",
    "namaAkun",
    "nilai",    
    "akunJeniss"
})
public class LaporanOperasionalAkunKelompok_WS  implements Serializable{
    private String kodeAkun;
    private String namaAkun;
    private double nilai;
    private List<LaporanOperasionalAkunJenis_WS> akunJeniss;
    
    public LaporanOperasionalAkunKelompok_WS() {
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) throws SIKDServiceException{
        if(kodeAkun==null || kodeAkun.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi kode Rekening Kelompok");
        this.kodeAkun = kodeAkun;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) throws SIKDServiceException{
        if( namaAkun == null || namaAkun.trim().equals(""))
            throw new SIKDServiceException("Silahkan isi nama Akun Kelompok");
        this.namaAkun = namaAkun;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public List<LaporanOperasionalAkunJenis_WS> getAkunJeniss() {
        return akunJeniss;
    }

    public void setAkunJeniss(List<LaporanOperasionalAkunJenis_WS> akunJeniss) {
        this.akunJeniss = akunJeniss;
    }
    
    

}
