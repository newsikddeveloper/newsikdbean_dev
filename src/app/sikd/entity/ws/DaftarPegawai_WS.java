package app.sikd.entity.ws;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlType(name = "daftarPegawaiWS", propOrder = {    
    "kodeSatker",
    "kodePemda",
    "namaPemda",
    "tahunAnggaran",
    "bulanGaji",
    "jenisData",
    "statusData",
    "userName",
    "password",
    "namaAplikasi",
    "pengembangAplikasi",
    "skpd"
})
public class DaftarPegawai_WS  implements Serializable{
    
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private short bulanGaji;
    private short jenisData;
    private short statusData;
    private String userName;
    private String password;    
    private String namaAplikasi;
    private String pengembangAplikasi;
    private List<SKPDPegawai_WS> skpd;

    public DaftarPegawai_WS() {
    }
    
    public String getKodeSatker() throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }
    
    public String getUserName() throws SIKDServiceException{
        if(userName == null || userName.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data UserName");
        return userName;
    }

    public void setUserName(String userName)  throws SIKDServiceException{
        if(userName == null || userName.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data UserName");
        this.userName = userName;
    }

    public String getPassword() throws SIKDServiceException{
        if(password == null || password.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data Password");
        return password;
    }

    public void setPassword(String password) throws SIKDServiceException{
        if(password == null || password.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi data Password");
        this.password = password;
    }
    
    public String getNamaAplikasi() throws SIKDServiceException{
        if( namaAplikasi==null || namaAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Nama Aplikasi");
        return namaAplikasi;
    }

    public void setNamaAplikasi(String namaAplikasi) throws SIKDServiceException{
        if( namaAplikasi==null || namaAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Nama Aplikasi");
        this.namaAplikasi = namaAplikasi;
    }

    public String getPengembangAplikasi() throws SIKDServiceException{
        if( pengembangAplikasi==null || pengembangAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Pengembang Aplikasi");
        return pengembangAplikasi;
    }

    public void setPengembangAplikasi(String pengembangAplikasi) throws SIKDServiceException{
        if( pengembangAplikasi==null || pengembangAplikasi.trim().equals("")) throw new SIKDServiceException("Silahkan isi data Pengembang Aplikasi");
        this.pengembangAplikasi = pengembangAplikasi;
    }

    public short getStatusData() throws SIKDServiceException{
        if(statusData < 0 || statusData > 3 ) throw new SIKDServiceException("Silahkan isi Status Data dengan bilangan 0, 1, 2, atau 3 \n untuk keterangan lebih lanjut silahkan baca developer guide");
        return statusData;
    }

    public void setStatusData(short statusData) throws SIKDServiceException{
        if(statusData < 0 || statusData > 3 ) throw new SIKDServiceException("Silahkan isi Status Data dengan bilangan 0, 1, 2, atau 3 \n untuk keterangan lebih lanjut silahkan baca developer guide");
        this.statusData = statusData;
    }

    public List<SKPDPegawai_WS> getSkpd() {
        return skpd;
    }

    public void setSkpd(List<SKPDPegawai_WS> skpd) {
        this.skpd = skpd;
    }

    public short getBulanGaji() {
        return bulanGaji;
    }

    public void setBulanGaji(short bulanGaji) throws SIKDServiceException{
        if( bulanGaji <1 || bulanGaji > 12 ) throw new SIKDServiceException("Silahkan isi data bulan gaji dengan menggunakan bilangan 1 sampai 12");
        this.bulanGaji = bulanGaji;
    }

    public short getJenisData() {
        return jenisData;
    }

    public void setJenisData(short jenisData) throws SIKDServiceException{
        if( (jenisData > 0 && jenisData < 5) || jenisData==91) this.jenisData = jenisData;
        else throw new SIKDServiceException("Silahkan isi data Jenis Data dengan menggunakan bilangan 1, 2, 3, 4, atau  91");
    }
    
    
    
}
