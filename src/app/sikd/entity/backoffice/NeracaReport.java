/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class NeracaReport implements Serializable{
    private String namaAkun;
    private double nilai;
    private double nilaiPra;
    private short type;

    public NeracaReport() {
    }

    public NeracaReport(String namaAkun, double nilai) {
        this.namaAkun = namaAkun;
        this.nilai = nilai;
    }

    public NeracaReport(String namaAkun, double nilai, double nilaiPra) {
        this.namaAkun = namaAkun;
        this.nilai = nilai;
        this.nilaiPra = nilaiPra;
    }
    public NeracaReport(String namaAkun, double nilai, double nilaiPra, short type) {
        this.namaAkun = namaAkun;
        this.nilai = nilai;
        this.nilaiPra = nilaiPra;
        this.type = type;
    }

    public double getNilaiPra() {
        return nilaiPra;
    }

    public void setNilaiPra(double nilaiPra) {
        this.nilaiPra = nilaiPra;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) {
        this.namaAkun = namaAkun;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }
    
}
