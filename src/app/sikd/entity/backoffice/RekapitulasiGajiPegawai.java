/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class RekapitulasiGajiPegawai implements Serializable{
    private String kodegolongan;
    private int pegawai;
    private int pasangan;
    private int anak;
    private double gajiPokok;
    private double tunjanganPasangan;
    private double tunjanganAnak;
    private double tpp;
    private double tunjanganStruktural;
    private double tunjanganUmum;
    private double tunjanganFungsional;
    private double tunjanganKhusus;
    private double tunjanganTerpencil;
    private double tkd;
    private double tunjanganpendidikan;
    private double tunjanganBeras;
    private double tunjanganPajak;
    private double tunjanganAskes;
    private double tunjanganPembulatan;
    private double jumlahKotor;
    private double potonganPajak;
    private double potonganAskes;
    private double iwp;
    private double taperum;
    private double potonganHutang;
    private double potonganBulog;
    private double potonganSewaRumah;
    private double JumlahPotongan;
    private double JumlahBersih;

    public RekapitulasiGajiPegawai() {
    }

    public RekapitulasiGajiPegawai(String kodegolongan, int pegawai, int pasangan, int anak, double gajiPokok, double tunjanganPasangan, double tunjanganAnak, double tpp, double tunjanganStruktural, double tunjanganUmum, double tunjanganFungsional, double tunjanganKhusus, double tunjanganTerpencil, double tkd, double tunjanganpendidikan, double tunjanganBeras, double tunjanganPajak, double tunjanganAskes, double tunjanganPembulatan, double jumlahKotor, double potonganPajak, double potonganAskes, double iwp, double taperum, double potonganHutang, double potonganBulog, double potonganSewaRumah, double JumlahPotongan, double JumlahBersih) {
        this.kodegolongan = kodegolongan;
        this.pegawai = pegawai;
        this.pasangan = pasangan;
        this.anak = anak;
        this.gajiPokok = gajiPokok;
        this.tunjanganPasangan = tunjanganPasangan;
        this.tunjanganAnak = tunjanganAnak;
        this.tpp = tpp;
        this.tunjanganStruktural = tunjanganStruktural;
        this.tunjanganUmum = tunjanganUmum;
        this.tunjanganFungsional = tunjanganFungsional;
        this.tunjanganKhusus = tunjanganKhusus;
        this.tunjanganTerpencil = tunjanganTerpencil;
        this.tkd = tkd;
        this.tunjanganpendidikan = tunjanganpendidikan;
        this.tunjanganBeras = tunjanganBeras;
        this.tunjanganPajak = tunjanganPajak;
        this.tunjanganAskes = tunjanganAskes;
        this.tunjanganPembulatan = tunjanganPembulatan;
        this.jumlahKotor = jumlahKotor;
        this.potonganPajak = potonganPajak;
        this.potonganAskes = potonganAskes;
        this.iwp = iwp;
        this.taperum = taperum;
        this.potonganHutang = potonganHutang;
        this.potonganBulog = potonganBulog;
        this.potonganSewaRumah = potonganSewaRumah;
        this.JumlahPotongan = JumlahPotongan;
        this.JumlahBersih = JumlahBersih;
    }

    public double getJumlahBersih() {
        return JumlahBersih;
    }

    public void setJumlahBersih(double JumlahBersih) {
        this.JumlahBersih = JumlahBersih;
    }

    public String getKodegolongan() {
        return kodegolongan;
    }

    public void setKodegolongan(String kodegolongan) {
        this.kodegolongan = kodegolongan;
    }

    public int getPegawai() {
        return pegawai;
    }

    public void setPegawai(int pegawai) {
        this.pegawai = pegawai;
    }

    public int getPasangan() {
        return pasangan;
    }

    public void setPasangan(int pasangan) {
        this.pasangan = pasangan;
    }

    public int getAnak() {
        return anak;
    }

    public void setAnak(int anak) {
        this.anak = anak;
    }

    public double getGajiPokok() {
        return gajiPokok;
    }

    public void setGajiPokok(double gajiPokok) {
        this.gajiPokok = gajiPokok;
    }

    public double getTunjanganPasangan() {
        return tunjanganPasangan;
    }

    public void setTunjanganPasangan(double tunjanganPasangan) {
        this.tunjanganPasangan = tunjanganPasangan;
    }

    public double getTunjanganAnak() {
        return tunjanganAnak;
    }

    public void setTunjanganAnak(double tunjanganAnak) {
        this.tunjanganAnak = tunjanganAnak;
    }

    public double getTpp() {
        return tpp;
    }

    public void setTpp(double tpp) {
        this.tpp = tpp;
    }

    public double getTunjanganStruktural() {
        return tunjanganStruktural;
    }

    public void setTunjanganStruktural(double tunjanganStruktural) {
        this.tunjanganStruktural = tunjanganStruktural;
    }

    public double getTunjanganUmum() {
        return tunjanganUmum;
    }

    public void setTunjanganUmum(double tunjanganUmum) {
        this.tunjanganUmum = tunjanganUmum;
    }

    public double getTunjanganFungsional() {
        return tunjanganFungsional;
    }

    public void setTunjanganFungsional(double tunjanganFungsional) {
        this.tunjanganFungsional = tunjanganFungsional;
    }

    public double getTunjanganKhusus() {
        return tunjanganKhusus;
    }

    public void setTunjanganKhusus(double tunjanganKhusus) {
        this.tunjanganKhusus = tunjanganKhusus;
    }

    public double getTunjanganTerpencil() {
        return tunjanganTerpencil;
    }

    public void setTunjanganTerpencil(double tunjanganTerpencil) {
        this.tunjanganTerpencil = tunjanganTerpencil;
    }

    public double getTkd() {
        return tkd;
    }

    public void setTkd(double tkd) {
        this.tkd = tkd;
    }

    public double getTunjanganpendidikan() {
        return tunjanganpendidikan;
    }

    public void setTunjanganpendidikan(double tunjanganpendidikan) {
        this.tunjanganpendidikan = tunjanganpendidikan;
    }

    public double getTunjanganBeras() {
        return tunjanganBeras;
    }

    public void setTunjanganBeras(double tunjanganBeras) {
        this.tunjanganBeras = tunjanganBeras;
    }

    public double getTunjanganPajak() {
        return tunjanganPajak;
    }

    public void setTunjanganPajak(double tunjanganPajak) {
        this.tunjanganPajak = tunjanganPajak;
    }

    public double getTunjanganAskes() {
        return tunjanganAskes;
    }

    public void setTunjanganAskes(double tunjanganAskes) {
        this.tunjanganAskes = tunjanganAskes;
    }

    public double getTunjanganPembulatan() {
        return tunjanganPembulatan;
    }

    public void setTunjanganPembulatan(double tunjanganPembulatan) {
        this.tunjanganPembulatan = tunjanganPembulatan;
    }

    public double getJumlahKotor() {
        return jumlahKotor;
    }

    public void setJumlahKotor(double jumlahKotor) {
        this.jumlahKotor = jumlahKotor;
    }

    public double getPotonganPajak() {
        return potonganPajak;
    }

    public void setPotonganPajak(double potonganPajak) {
        this.potonganPajak = potonganPajak;
    }

    public double getPotonganAskes() {
        return potonganAskes;
    }

    public void setPotonganAskes(double potonganAskes) {
        this.potonganAskes = potonganAskes;
    }

    public double getIwp() {
        return iwp;
    }

    public void setIwp(double iwp) {
        this.iwp = iwp;
    }

    public double getTaperum() {
        return taperum;
    }

    public void setTaperum(double taperum) {
        this.taperum = taperum;
    }

    public double getPotonganHutang() {
        return potonganHutang;
    }

    public void setPotonganHutang(double potonganHutang) {
        this.potonganHutang = potonganHutang;
    }

    public double getPotonganBulog() {
        return potonganBulog;
    }

    public void setPotonganBulog(double potonganBulog) {
        this.potonganBulog = potonganBulog;
    }

    public double getPotonganSewaRumah() {
        return potonganSewaRumah;
    }

    public void setPotonganSewaRumah(double potonganSewaRumah) {
        this.potonganSewaRumah = potonganSewaRumah;
    }

    public double getJumlahPotongan() {
        return JumlahPotongan;
    }

    public void setJumlahPotongan(double JumlahPotongan) {
        this.JumlahPotongan = JumlahPotongan;
    }
    
}
