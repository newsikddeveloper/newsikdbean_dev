/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 *
 * @author sora
 */
public class KompilasiApbd1364 implements Serializable{
    private long index;
    private String kodePemda;
    private short tahun;
    private short kodeData;
    private Date tanggalPenyusunan;
    private List<KompilasiKegiatanApbd1364> kegiatans;

    public KompilasiApbd1364() {
    }

    public KompilasiApbd1364(String kodePemda, short tahun, short kodeData, Date tanggalPenyusunan) {
        this.kodePemda = kodePemda;
        this.tahun = tahun;
        this.kodeData = kodeData;
        this.tanggalPenyusunan = tanggalPenyusunan;
    }

    public KompilasiApbd1364(long index, String kodePemda, short tahun, short kodeData, Date tanggalPenyusunan) {
        this.index = index;
        this.kodePemda = kodePemda;
        this.tahun = tahun;
        this.kodeData = kodeData;
        this.tanggalPenyusunan = tanggalPenyusunan;
    }

    public Date getTanggalPenyusunan() {
        return tanggalPenyusunan;
    }

    public void setTanggalPenyusunan(Date tanggalPenyusunan) {
        this.tanggalPenyusunan = tanggalPenyusunan;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public short getTahun() {
        return tahun;
    }

    public void setTahun(short tahun) {
        this.tahun = tahun;
    }

    public short getKodeData() {
        return kodeData;
    }

    public void setKodeData(short kodeData) {
        this.kodeData = kodeData;
    }

    public List<KompilasiKegiatanApbd1364> getKegiatans() {
        return kegiatans;
    }

    public void setKegiatans(List<KompilasiKegiatanApbd1364> kegiatans) {
        this.kegiatans = kegiatans;
    }
    
}
