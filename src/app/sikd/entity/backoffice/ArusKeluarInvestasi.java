/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */

public class ArusKeluarInvestasi extends ArusKasInOut implements Serializable, Comparable<ArusKeluarInvestasi>, Cloneable{

    public ArusKeluarInvestasi() {
    }

    public ArusKeluarInvestasi(String kodeAkun, String namaAkun, double nilai) {
        super(kodeAkun, namaAkun, nilai);    
    }

    public ArusKeluarInvestasi(long index, String kodeAkun, String namaAkun, double nilai) {
        super(index, kodeAkun, namaAkun, nilai);
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final ArusKeluarInvestasi other = (ArusKeluarInvestasi) obj;
        if(!super.getKodeAkun().equals(other.getKodeAkun())) return false;
        return true;
    }

    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(ArusKeluarInvestasi t) {
        return this.getKodeAkun().compareTo(t.getKodeAkun());
    }
    
    /**
     *
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return getNamaAkun();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    
}