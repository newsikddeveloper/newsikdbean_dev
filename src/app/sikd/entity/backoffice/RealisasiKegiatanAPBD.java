package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author detra
 */
public class RealisasiKegiatanAPBD implements Serializable{  
    private long index;
    private String kodeUrusanProgram;
    private String namaUrusanProgram;
    private String kodeUrusanPelaksana;
    private String namaUrusanPelaksana;
    private String kodeSKPD;
    private String namaSKPD;
    private String kodeProgram;
    private String namaProgram;
    private String kodeKegiatan;
    private String namaKegiatan;
    private String kodeFungsi;
    private String namaFungsi;
    
    private List<RealisasiKodeRekeningAPBD> kodeRekenings;

    public RealisasiKegiatanAPBD() {
    }

    public RealisasiKegiatanAPBD(String kodeUrusanProgram, String namaUrusanProgram, String kodeUrusanPelaksana, String namaUrusanPelaksana, String kodeSKPD, String namaSKPD, String kodeProgram, String namaProgram, String kodeKegiatan, String namaKegiatan, String kodeFungsi, String namaFungsi) {        
        this.kodeUrusanProgram = kodeUrusanProgram;
        this.namaUrusanProgram = namaUrusanProgram;
        this.kodeUrusanPelaksana = kodeUrusanPelaksana;
        this.namaUrusanPelaksana = namaUrusanPelaksana;
        this.kodeSKPD = kodeSKPD;
        this.namaSKPD = namaSKPD;
        this.kodeProgram = kodeProgram;
        this.namaProgram = namaProgram;
        this.kodeKegiatan = kodeKegiatan;
        this.namaKegiatan = namaKegiatan;
        this.kodeFungsi = kodeFungsi;
        this.namaFungsi = namaFungsi;
    }    
    
    public RealisasiKegiatanAPBD(RealisasiKegiatanAPBD obj) {
        this.index = obj.getIndex();
        this.kodeUrusanProgram = obj.getKodeUrusanProgram();
        this.namaUrusanProgram = obj.getNamaUrusanProgram();
        this.kodeUrusanPelaksana = obj.getKodeUrusanPelaksana();
        this.namaUrusanPelaksana = obj.getNamaUrusanPelaksana();
        this.kodeSKPD = obj.getKodeSKPD();
        this.namaSKPD = obj.getNamaSKPD();
        this.kodeProgram = obj.getKodeProgram();
        this.namaProgram = obj.getNamaProgram();
        this.kodeKegiatan = obj.getKodeKegiatan();
        this.namaKegiatan = obj.getNamaKegiatan();
        this.kodeFungsi = obj.getKodeFungsi();
        this.namaFungsi = obj.getNamaFungsi();
    }    

    public RealisasiKegiatanAPBD(long index, String kodeUrusanProgram, String namaUrusanProgram, String kodeUrusanPelaksana, String namaUrusanPelaksana, String kodeSKPD, String namaSKPD, String kodeProgram, String namaProgram, String kodeKegiatan, String namaKegiatan, String kodeFungsi, String namaFungsi) {
        this(kodeUrusanProgram, namaUrusanProgram, kodeUrusanPelaksana, namaUrusanPelaksana, kodeSKPD, namaSKPD, kodeProgram, namaProgram, kodeKegiatan, namaKegiatan, kodeFungsi, namaFungsi);        
        this.index = index;
    }
    public RealisasiKegiatanAPBD(String kodeUrusanProgram, String namaUrusanProgram, String kodeUrusanPelaksana, String namaUrusanPelaksana, String kodeSKPD, String namaSKPD, String kodeProgram, String namaProgram, String kodeKegiatan, String namaKegiatan, String kodeFungsi, String namaFungsi, List<RealisasiKodeRekeningAPBD> kodeRekenings) {
        this(kodeUrusanProgram, namaUrusanProgram, kodeUrusanPelaksana, namaUrusanPelaksana, kodeSKPD, namaSKPD, kodeProgram, namaProgram, kodeKegiatan, namaKegiatan, kodeFungsi, namaFungsi);        
        this.kodeRekenings = kodeRekenings;
    }
    public RealisasiKegiatanAPBD(long index, String kodeUrusanProgram, String namaUrusanProgram, String kodeUrusanPelaksana, String namaUrusanPelaksana, String kodeSKPD, String namaSKPD, String kodeProgram, String namaProgram, String kodeKegiatan, String namaKegiatan, String kodeFungsi, String namaFungsi, List<RealisasiKodeRekeningAPBD> kodeRekenings) {
        this(kodeUrusanProgram, namaUrusanProgram, kodeUrusanPelaksana, namaUrusanPelaksana, kodeSKPD, namaSKPD, kodeProgram, namaProgram, kodeKegiatan, namaKegiatan, kodeFungsi, namaFungsi, kodeRekenings);
        this.index = index;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getKodeUrusanProgram() {
        return kodeUrusanProgram;
    }

    public void setKodeUrusanProgram(String kodeUrusanProgram) {
        this.kodeUrusanProgram = kodeUrusanProgram;
    }

    public String getNamaUrusanProgram() {
        return namaUrusanProgram;
    }

    public void setNamaUrusanProgram(String namaUrusanProgram) {
        this.namaUrusanProgram = namaUrusanProgram;
    }

    public String getKodeUrusanPelaksana() {
        return kodeUrusanPelaksana;
    }

    public void setKodeUrusanPelaksana(String kodeUrusanPelaksana) {
        this.kodeUrusanPelaksana = kodeUrusanPelaksana;
    }

    public String getNamaUrusanPelaksana() {
        return namaUrusanPelaksana;
    }

    public void setNamaUrusanPelaksana(String namaUrusanPelaksana) {
        this.namaUrusanPelaksana = namaUrusanPelaksana;
    }

    public String getKodeSKPD() {
        return kodeSKPD;
    }

    public void setKodeSKPD(String kodeSKPD) {
        this.kodeSKPD = kodeSKPD;
    }

    public String getNamaSKPD() {
        return namaSKPD;
    }

    public void setNamaSKPD(String namaSKPD) {
        this.namaSKPD = namaSKPD;
    }

    public String getKodeProgram() {
        return kodeProgram;
    }

    public void setKodeProgram(String kodeProgram) {
        this.kodeProgram = kodeProgram;
    }

    public String getNamaProgram() {
        return namaProgram;
    }

    public void setNamaProgram(String namaProgram) {
        this.namaProgram = namaProgram;
    }

    public String getKodeKegiatan() {
        return kodeKegiatan;
    }

    public void setKodeKegiatan(String kodeKegiatan) {
        this.kodeKegiatan = kodeKegiatan;
    }

    public String getNamaKegiatan() {
        return namaKegiatan;
    }

    public void setNamaKegiatan(String namaKegiatan) {
        this.namaKegiatan = namaKegiatan;
    }

    public String getKodeFungsi() {
        return kodeFungsi;
    }

    public void setKodeFungsi(String kodeFungsi) {
        this.kodeFungsi = kodeFungsi;
    }

    public String getNamaFungsi() {
        return namaFungsi;
    }

    public void setNamaFungsi(String namaFungsi) {
        this.namaFungsi = namaFungsi;
    }

    public List<RealisasiKodeRekeningAPBD> getKodeRekenings() {
        return kodeRekenings;
    }

    public void setKodeRekenings(List<RealisasiKodeRekeningAPBD> kodeRekenings) {
        this.kodeRekenings = kodeRekenings;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (int) (this.index ^ (this.index >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RealisasiKegiatanAPBD other = (RealisasiKegiatanAPBD) obj;
        if (this.index != other.index) {
            return false;
        }
        return true;
    }
    
    
       
    
}