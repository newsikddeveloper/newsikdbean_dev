/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;



/**
 *
 * @author sora
 */
public class PDRBHarga implements Serializable{
    private long index;
  private double pertanian;
  private double tambanggali;
  private double tambangmigas;
  private double pengolahan;
  private double migas;
  private double listrikgasair;
  private double bangunan;
  private double resto;
  private double komunikasi;
  private double keuangan;
  private double jasa;
  private double pdrbmigas;
  private double pdrbsubmigas;
  private double pdrbnonmigas;
  private double pdrbnonsub;

    public PDRBHarga() {
    }

    public PDRBHarga(double pertanian, double tambanggali, double tambangmigas, double pengolahan, double migas, double listrikgasair, double bangunan, double resto, double komunikasi, double keuangan, double jasa, double pdrbmigas, double pdrbsubmigas, double pdrbnonmigas, double pdrbnonsub) {
        this.pertanian = pertanian;
        this.tambanggali = tambanggali;
        this.tambangmigas = tambangmigas;
        this.pengolahan = pengolahan;
        this.migas = migas;
        this.listrikgasair = listrikgasair;
        this.bangunan = bangunan;
        this.resto = resto;
        this.komunikasi = komunikasi;
        this.keuangan = keuangan;
        this.jasa = jasa;
        this.pdrbmigas = pdrbmigas;
        this.pdrbsubmigas = pdrbsubmigas;
        this.pdrbnonmigas = pdrbnonmigas;
        this.pdrbnonsub = pdrbnonsub;
    }

    public PDRBHarga(long index, double pertanian, double tambanggali, double tambangmigas, double pengolahan, double migas, double listrikgasair, double bangunan, double resto, double komunikasi, double keuangan, double jasa, double pdrbmigas, double pdrbsubmigas, double pdrbnonmigas, double pdrbnonsub) {
        this.index = index;
        this.pertanian = pertanian;
        this.tambanggali = tambanggali;
        this.tambangmigas = tambangmigas;
        this.pengolahan = pengolahan;
        this.migas = migas;
        this.listrikgasair = listrikgasair;
        this.bangunan = bangunan;
        this.resto = resto;
        this.komunikasi = komunikasi;
        this.keuangan = keuangan;
        this.jasa = jasa;
        this.pdrbmigas = pdrbmigas;
        this.pdrbsubmigas = pdrbsubmigas;
        this.pdrbnonmigas = pdrbnonmigas;
        this.pdrbnonsub = pdrbnonsub;
    }

    public double getPdrbnonsub() {
        return pdrbnonsub;
    }

    public void setPdrbnonsub(double pdrbnonsub) {
        this.pdrbnonsub = pdrbnonsub;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public double getPertanian() {
        return pertanian;
    }

    public void setPertanian(double pertanian) {
        this.pertanian = pertanian;
    }

    public double getTambanggali() {
        return tambanggali;
    }

    public void setTambanggali(double tambanggali) {
        this.tambanggali = tambanggali;
    }

    public double getTambangmigas() {
        return tambangmigas;
    }

    public void setTambangmigas(double tambangmigas) {
        this.tambangmigas = tambangmigas;
    }

    public double getPengolahan() {
        return pengolahan;
    }

    public void setPengolahan(double pengolahan) {
        this.pengolahan = pengolahan;
    }

    public double getMigas() {
        return migas;
    }

    public void setMigas(double migas) {
        this.migas = migas;
    }

    public double getListrikgasair() {
        return listrikgasair;
    }

    public void setListrikgasair(double listrikgasair) {
        this.listrikgasair = listrikgasair;
    }

    public double getBangunan() {
        return bangunan;
    }

    public void setBangunan(double bangunan) {
        this.bangunan = bangunan;
    }

    public double getResto() {
        return resto;
    }

    public void setResto(double resto) {
        this.resto = resto;
    }

    public double getKomunikasi() {
        return komunikasi;
    }

    public void setKomunikasi(double komunikasi) {
        this.komunikasi = komunikasi;
    }

    public double getKeuangan() {
        return keuangan;
    }

    public void setKeuangan(double keuangan) {
        this.keuangan = keuangan;
    }

    public double getJasa() {
        return jasa;
    }

    public void setJasa(double jasa) {
        this.jasa = jasa;
    }

    public double getPdrbmigas() {
        return pdrbmigas;
    }

    public void setPdrbmigas(double pdrbmigas) {
        this.pdrbmigas = pdrbmigas;
    }

    public double getPdrbsubmigas() {
        return pdrbsubmigas;
    }

    public void setPdrbsubmigas(double pdrbsubmigas) {
        this.pdrbsubmigas = pdrbsubmigas;
    }

    public double getPdrbnonmigas() {
        return pdrbnonmigas;
    }

    public void setPdrbnonmigas(double pdrbnonmigas) {
        this.pdrbnonmigas = pdrbnonmigas;
    }
    
    
  
    
}
