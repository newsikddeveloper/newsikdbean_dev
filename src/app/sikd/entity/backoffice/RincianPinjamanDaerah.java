package app.sikd.entity.backoffice;

import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author detra
 */

public class RincianPinjamanDaerah implements Serializable{
    String sumber;
    String dasarHukum;
    Date tanggalPerjanjian;
    double jumlahPinjaman;
    double jangkaWaktu;
    double bunga;
    String tujuan;
    double bayarPokok;
    double bayarBunga;
    double sisaPokok;
    double sisaBunga;

    public RincianPinjamanDaerah() {
    }

    public RincianPinjamanDaerah(String sumber, String dasarHukum, Date tanggalPerjanjian, double jumlahPinjaman, double jangkaWaktu, double bunga, String tujuan, double bayarPokok, double bayarBunga, double sisaPokok, double sisaBunga) throws SIKDServiceException{        
        this.sumber = sumber;
        this.dasarHukum = dasarHukum;
        this.tanggalPerjanjian = tanggalPerjanjian;
        this.jumlahPinjaman = jumlahPinjaman;
        this.jangkaWaktu = jangkaWaktu;
        this.bunga = bunga;
        this.tujuan = tujuan;
        this.bayarPokok = bayarPokok;
        this.bayarBunga = bayarBunga;
        this.sisaPokok = sisaPokok;
        this.sisaBunga = sisaBunga;
    }
    
    public String getSumber() {
        return sumber;
    }

    public void setSumber(String sumber) throws SIKDServiceException{    
        this.sumber = sumber;
    }

    public String getDasarHukum() {
        return dasarHukum;
    }

    public void setDasarHukum(String dasarHukum) {
        this.dasarHukum = dasarHukum;
    }

    public Date getTanggalPerjanjian() {
        return tanggalPerjanjian;
    }

    public void setTanggalPerjanjian(Date tanggalPerjanjian) {
        this.tanggalPerjanjian = tanggalPerjanjian;
    }

    public double getJumlahPinjaman() {
        return jumlahPinjaman;
    }

    public void setJumlahPinjaman(double jumlahPinjaman) {
        this.jumlahPinjaman = jumlahPinjaman;
    }

    public double getJangkaWaktu() {
        return jangkaWaktu;
    }

    public void setJangkaWaktu(double jangkaWaktu) {
        this.jangkaWaktu = jangkaWaktu;
    }

    public double getBunga() {
        return bunga;
    }

    public void setBunga(double bunga) {
        this.bunga = bunga;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    public double getBayarPokok() {
        return bayarPokok;
    }

    public void setBayarPokok(double bayarPokok) {
        this.bayarPokok = bayarPokok;
    }

    public double getBayarBunga() {
        return bayarBunga;
    }

    public void setBayarBunga(double bayarBunga) {
        this.bayarBunga = bayarBunga;
    }

    public double getSisaPokok() {
        return sisaPokok;
    }

    public void setSisaPokok(double sisaPokok) {
        this.sisaPokok = sisaPokok;
    }

    public double getSisaBunga() {
        return sisaBunga;
    }

    public void setSisaBunga(double sisaBunga) {
        this.sisaBunga = sisaBunga;
    }
    
    public String getBayarBungaAsString(){
        return SIKDUtil.doubleToString(bayarBunga);
    }
    public void setBayarBungaAsString(String bayarBungaAsString){        
        if( bayarBungaAsString!=null && !bayarBungaAsString.trim().equals("") )  bayarBunga = SIKDUtil.stringToDouble(bayarBungaAsString);
        else bayarBunga = 0;
    }
    public String getBayarPokokAsString(){
        return SIKDUtil.doubleToString(bayarPokok);
    }
    public void setBayarPokokAsString(String bayarPokokAsString){        
        if( bayarPokokAsString!=null && !bayarPokokAsString.trim().equals("") )  bayarPokok = SIKDUtil.stringToDouble(bayarPokokAsString);
        else bayarPokok = 0;
    }
    public String getBungaAsString(){
        return SIKDUtil.doubleToString(bunga);
    }
    public void setBungaAsString(String bungaAsString){        
        if( bungaAsString!=null && !bungaAsString.trim().equals("") )  bunga = SIKDUtil.stringToDouble(bungaAsString);
        else bunga = 0;
    }
    public String getJangkaWaktuAsString(){
        return SIKDUtil.doubleToString(jangkaWaktu);
    }
    public void setJangkaWaktuAsString(String jangkaWaktuAsString){        
        if( jangkaWaktuAsString!=null && !jangkaWaktuAsString.trim().equals("") )  jangkaWaktu = SIKDUtil.stringToDouble(jangkaWaktuAsString);
        else jangkaWaktu = 0;
    }
    public String getJumlahPinjamanAsString(){
        return SIKDUtil.doubleToString(jumlahPinjaman);
    }
    public void setJumlahPinjamanAsString(String jumlahPinjamanAsString){        
        if( jumlahPinjamanAsString!=null && !jumlahPinjamanAsString.trim().equals("") )  jumlahPinjaman = SIKDUtil.stringToDouble(jumlahPinjamanAsString);
        else jumlahPinjaman = 0;
    }
    public String getSisaBungaAsString(){
        return SIKDUtil.doubleToString(sisaBunga);
    }
    public void setSisaBungaAsString(String sisaBungaAsString){        
        if( sisaBungaAsString!=null && !sisaBungaAsString.trim().equals("") )  sisaBunga = SIKDUtil.stringToDouble(sisaBungaAsString);
        else sisaBunga = 0;
    }
    public String getSisaPokokAsString(){
        return SIKDUtil.doubleToString(sisaPokok);
    }
    public void setSisaPokokAsString(String sisaPokokAsString){        
        if( sisaPokokAsString!=null && !sisaPokokAsString.trim().equals("") )  sisaPokok = SIKDUtil.stringToDouble(sisaPokokAsString);
        else sisaPokok = 0;
    }
    public String getTanggalPerjanjianAsString(){        
        return SIKDUtil.dateToString(tanggalPerjanjian);
    }
    public void setTanggalPerjanjianAsString(String tanggalPerjanjianAsString){        
        if( tanggalPerjanjianAsString!=null && !tanggalPerjanjianAsString.trim().equals("") )  tanggalPerjanjian = SIKDUtil.stringToDate(tanggalPerjanjianAsString);
        else tanggalPerjanjian = null;
    }
}
