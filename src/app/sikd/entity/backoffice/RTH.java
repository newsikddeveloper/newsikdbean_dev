/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;

/**
 *
 * @author sora
 */
public class RTH implements Serializable{
    private String noUrut;
    private String skpd;
    private int jumlahSPM;
    private double jumlahBelanjaSPM;
    private int jumlahSP2D;
    private double jumlahBelanjaSP2D;
    private double jumlahPajak;
    private double jumlahPotongan;
    private String keterangan;
    
    private String style;

    public RTH() {
    }

    public RTH(String noUrut, String skpd, int jumlahSPM, double jumlahBelanjaSPM, int jumlahSP2D, double jumlahBelanjaSP2D, double jumlahPajak, double jumlahPotongan, String keterangan) {
        this.noUrut = noUrut;
        this.skpd = skpd;
        this.jumlahSPM = jumlahSPM;
        this.jumlahBelanjaSPM = jumlahBelanjaSPM;
        this.jumlahSP2D = jumlahSP2D;
        this.jumlahBelanjaSP2D = jumlahBelanjaSP2D;
        this.jumlahPajak = jumlahPajak;
        this.jumlahPotongan = jumlahPotongan;
        this.keterangan = keterangan;
    }
    
    public RTH(String noUrut, String skpd, int jumlahSPM, double jumlahBelanjaSPM, int jumlahSP2D, double jumlahBelanjaSP2D, double jumlahPajak, double jumlahPotongan, String keterangan, String style) {
        this.noUrut = noUrut;
        this.skpd = skpd;
        this.jumlahSPM = jumlahSPM;
        this.jumlahBelanjaSPM = jumlahBelanjaSPM;
        this.jumlahSP2D = jumlahSP2D;
        this.jumlahBelanjaSP2D = jumlahBelanjaSP2D;
        this.jumlahPajak = jumlahPajak;
        this.jumlahPotongan = jumlahPotongan;
        this.keterangan = keterangan;
        this.style = style;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(String noUrut) {
        this.noUrut = noUrut;
    }

    public String getSkpd() {
        return skpd;
    }

    public void setSkpd(String skpd) {
        this.skpd = skpd;
    }

    public int getJumlahSPM() {
        return jumlahSPM;
    }

    public void setJumlahSPM(int jumlahSPM) {
        this.jumlahSPM = jumlahSPM;
    }

    public double getJumlahBelanjaSPM() {
        return jumlahBelanjaSPM;
    }

    public void setJumlahBelanjaSPM(double jumlahBelanjaSPM) {
        this.jumlahBelanjaSPM = jumlahBelanjaSPM;
    }

    public int getJumlahSP2D() {
        return jumlahSP2D;
    }

    public void setJumlahSP2D(int jumlahSP2D) {
        this.jumlahSP2D = jumlahSP2D;
    }

    public double getJumlahBelanjaSP2D() {
        return jumlahBelanjaSP2D;
    }

    public void setJumlahBelanjaSP2D(double jumlahBelanjaSP2D) {
        this.jumlahBelanjaSP2D = jumlahBelanjaSP2D;
    }

    public double getJumlahPajak() {
        return jumlahPajak;
    }

    public void setJumlahPajak(double jumlahPajak) {
        this.jumlahPajak = jumlahPajak;
    }

    public String getJumlahSPMAsString() {
        if( (noUrut==null || noUrut.trim().equals("")) && (skpd==null || skpd.trim().equals("")) ){
            return "";
        }
        return String.valueOf(jumlahSPM);
//        return jumlahSPMAsString;
    }

    public void setJumlahSPMAsString(String jumlahSPMAsString) {
//        this.jumlahSPMAsString = jumlahSPMAsString;
    }

    public String getJumlahBelanjaSPMAsString() {
        if( (noUrut==null || noUrut.trim().equals("")) && (skpd==null || skpd.trim().equals("")) ){
            return "";
        }
        return SIKDUtil.doubleToString(jumlahBelanjaSPM);
//        return jumlahBelanjaSPMAsString;
    }

    public void setJumlahBelanjaSPMAsString(String jumlahBelanjaSPMAsString) {
//        this.jumlahBelanjaSPMAsString = jumlahBelanjaSPMAsString;
    }

    public String getJumlahSP2DAsString() {
        if( (noUrut==null || noUrut.trim().equals("")) && (skpd==null || skpd.trim().equals("")) ){
            return "";
        }
        return String.valueOf(jumlahSP2D);
//        return jumlahSP2DAsString;
    }

    public void setJumlahSP2DAsString(String jumlahSP2DAsString) {
//        this.jumlahSP2DAsString = jumlahSP2DAsString;
    }

    public String getJumlahBelanjaSP2DAsString() {
        if( (noUrut==null || noUrut.trim().equals("")) && (skpd==null || skpd.trim().equals("")) ){
            return "";
        }
        return SIKDUtil.doubleToString(jumlahBelanjaSP2D);
//        return jumlahBelanjaSP2DAsString;
    }

    public void setJumlahBelanjaSP2DAsString(String jumlahBelanjaSP2DAsString) {
//        this.jumlahBelanjaSP2DAsString = jumlahBelanjaSP2DAsString;
    }

    public String getJumlahPajakAsString() {
        if( (noUrut==null || noUrut.trim().equals("")) && (skpd==null || skpd.trim().equals("")) ){
            return "";
        }
        return SIKDUtil.doubleToString(jumlahPajak);
//        return jumlahPajakAsString;
    }

    public void setJumlahPajakAsString(String jumlahPajakAsString) {
//        this.jumlahPajakAsString = jumlahPajakAsString;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public double getJumlahPotongan() {
        return jumlahPotongan;
    }

    public void setJumlahPotongan(double jumlahPotongan) {
        this.jumlahPotongan = jumlahPotongan;
    }
    
    public String getJumlahPotonganAsString() {
        if( (noUrut==null || noUrut.trim().equals("")) && (skpd==null || skpd.trim().equals("")) ){
            return "";
        }
        return SIKDUtil.doubleToString(jumlahPotongan);
//        return jumlahPajakAsString;
    }

    public void setJumlahPotonganAsString(String jumlahPotonganAsString) {
//        this.jumlahPajakAsString = jumlahPajakAsString;
    }
    
}
