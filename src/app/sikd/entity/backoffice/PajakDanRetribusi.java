/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class PajakDanRetribusi  implements Serializable{
    private long index;
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short statusData;
    private String namaAplikasi;
    private String pengembangAplikasi;
    private short tahunAnggaran;
    
    private List<RincianPajakDanRetribusi> rincians;

    public PajakDanRetribusi() {
    }

    public PajakDanRetribusi(String kodeSatker, String kodePemda, String namaPemda, short statusData, String namaAplikasi, String pengembangAplikasi, short tahunAnggaran, List<RincianPajakDanRetribusi> rincians) {
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.statusData = statusData;
        this.namaAplikasi = namaAplikasi;
        this.pengembangAplikasi = pengembangAplikasi;
        this.tahunAnggaran = tahunAnggaran;
        this.rincians = rincians;
    }

    public PajakDanRetribusi(long index, String kodeSatker, String kodePemda, String namaPemda, short statusData, String namaAplikasi, String pengembangAplikasi, short tahunAnggaran, List<RincianPajakDanRetribusi> rincians) {
        this.index = index;
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.statusData = statusData;
        this.namaAplikasi = namaAplikasi;
        this.pengembangAplikasi = pengembangAplikasi;
        this.tahunAnggaran = tahunAnggaran;
        this.rincians = rincians;
    }
    
    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker){
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda){
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda){
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public List<RincianPajakDanRetribusi> getRincians() {
        return rincians;
    }

    public void setRincians(List<RincianPajakDanRetribusi> rincians) {
        this.rincians = rincians;
    }
    
    public String getNamaAplikasi() {
        return namaAplikasi;
    }

    public void setNamaAplikasi(String namaAplikasi){
        this.namaAplikasi = namaAplikasi;
    }

    public String getPengembangAplikasi() {
        return pengembangAplikasi;
    }

    public void setPengembangAplikasi(String pengembangAplikasi){
        this.pengembangAplikasi = pengembangAplikasi;
    }

    public short getStatusData() {
        return statusData;
    }

    public void setStatusData(short statusData){
        this.statusData = statusData;
    }    

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }
}
