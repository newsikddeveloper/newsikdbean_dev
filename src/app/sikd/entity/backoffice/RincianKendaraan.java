package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author detra
 */

public class RincianKendaraan implements Serializable{
    private String nomorPolisi;
    private String pemilik;
    private String alamat;
    private String npwp;
    private String kpp;
    private String cabangNpwp;
    private short tahunPembuatan;
    private double njkb;
    private String jenis;
    private String merk;
    private String tipe;
    private String cc;
    private String bahanBakar;

    public RincianKendaraan() {
    }

    public RincianKendaraan(String nomorPolisi, String pemilik, String alamat, String npwp, String kpp, String cabangNpwp, short tahunPembuatan, double njkb, String jenis, String merk, String tipe, String cc, String bahanBakar) {
        this.nomorPolisi = nomorPolisi;
        this.pemilik = pemilik;
        this.alamat = alamat;
        this.npwp = npwp;
        this.kpp = kpp;
        this.cabangNpwp = cabangNpwp;
        this.tahunPembuatan = tahunPembuatan;
        this.njkb = njkb;
        this.jenis = jenis;
        this.merk = merk;
        this.tipe = tipe;
        this.cc = cc;
        this.bahanBakar = bahanBakar;
    }
    
    public String getNomorPolisi() {
        return nomorPolisi;
    }

    public void setNomorPolisi(String nomorPolisi) {
        this.nomorPolisi = nomorPolisi;
    }

    public String getPemilik() {
        return pemilik;
    }

    public void setPemilik(String pemilik) {
        this.pemilik = pemilik;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public String getCabangNpwp() {
        return cabangNpwp;
    }

    public void setCabangNpwp(String cabangNpwp) {
        this.cabangNpwp = cabangNpwp;
    }

    public short getTahunPembuatan() {
        return tahunPembuatan;
    }

    public void setTahunPembuatan(short tahunPembuatan) {
        this.tahunPembuatan = tahunPembuatan;
    }

    public double getNjkb() {
        return njkb;
    }

    public void setNjkb(double njkb) {
        this.njkb = njkb;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBahanBakar() {
        return bahanBakar;
    }

    public void setBahanBakar(String bahanBakar) {
        this.bahanBakar = bahanBakar;
    }    
}
