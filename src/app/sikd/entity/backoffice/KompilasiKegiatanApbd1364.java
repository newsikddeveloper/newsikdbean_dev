/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class KompilasiKegiatanApbd1364 implements Serializable{
    private long index;
    private String kodeFungsi;
    
    private List<KompilasiAkunApbd1364> akuns;

    public KompilasiKegiatanApbd1364() {
    }

    public KompilasiKegiatanApbd1364(String kodeFungsi) {
        this.kodeFungsi = kodeFungsi;
    }

    public KompilasiKegiatanApbd1364(long index, String kodeFungsi) {
        this.index = index;
        this.kodeFungsi = kodeFungsi;
    }

    public String getKodeFungsi() {
        return kodeFungsi;
    }

    public void setKodeFungsi(String kodeFungsi) {
        this.kodeFungsi = kodeFungsi;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public List<KompilasiAkunApbd1364> getAkuns() {
        return akuns;
    }

    public void setAkuns(List<KompilasiAkunApbd1364> akuns) {
        this.akuns = akuns;
    }
    
}
