package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author detra
 */

public class KegiatanAPBD implements Serializable{
    private long index;
    private String kodeUrusanProgram;
    private String namaUrusanProgram;
    private String kodeUrusanPelaksana;
    private String namaUrusanPelaksana;
    private String kodeSKPD;
    private String namaSKPD;
    private String kodeProgram;
    private String namaProgram;
    private String kodeKegiatan;
    private String namaKegiatan;
    private String kodeFungsi;
    private String namaFungsi;
    
    private List<KodeRekeningAPBD> kodeRekenings;

    public KegiatanAPBD() {
    }

    public KegiatanAPBD(String kodeUrusanProgram, String namaUrusanProgram, String kodeUrusanPelaksana, String namaUrusanPelaksana, String kodeSKPD, String namaSKPD, String kodeProgram, String namaProgram, String kodeKegiatan, String namaKegiatan, String kodeFungsi, String namaFungsi) {
        this.kodeUrusanProgram = kodeUrusanProgram;
        this.namaUrusanProgram = namaUrusanProgram;
        this.kodeUrusanPelaksana = kodeUrusanPelaksana;
        this.namaUrusanPelaksana = namaUrusanPelaksana;
        this.kodeSKPD = kodeSKPD;
        this.namaSKPD = namaSKPD;
        this.kodeProgram = kodeProgram;
        this.namaProgram = namaProgram;
        this.kodeKegiatan = kodeKegiatan;
        this.namaKegiatan = namaKegiatan;
        this.kodeFungsi = kodeFungsi;
        this.namaFungsi = namaFungsi;
    }    
    
    public KegiatanAPBD(KegiatanAPBD keg) {
        this.index = keg.getIndex();
        this.kodeUrusanProgram = keg.getKodeUrusanProgram();
        this.namaUrusanProgram = keg.getNamaUrusanProgram();
        this.kodeUrusanPelaksana = keg.getKodeUrusanPelaksana();
        this.namaUrusanPelaksana = keg.getNamaUrusanPelaksana();
        this.kodeSKPD = keg.getKodeSKPD();
        this.namaSKPD = keg.getNamaSKPD();
        this.kodeProgram = keg.getKodeProgram();
        this.namaProgram = keg.getNamaProgram();
        this.kodeKegiatan = keg.getKodeKegiatan();
        this.namaKegiatan = keg.getNamaKegiatan();
        this.kodeFungsi = keg.getKodeFungsi();
        this.namaFungsi = keg.getNamaFungsi();
    }

    public KegiatanAPBD(String kodeUrusanProgram, String namaUrusanProgram, String kodeUrusanPelaksana, String namaUrusanPelaksana, String kodeSKPD, String namaSKPD, String kodeProgram, String namaProgram, String kodeKegiatan, String namaKegiatan, String kodeFungsi, String namaFungsi, List<KodeRekeningAPBD> kodeRekenings) {
        this(kodeUrusanProgram, namaUrusanProgram, kodeUrusanPelaksana, namaUrusanPelaksana, kodeSKPD, namaSKPD, kodeProgram, namaProgram, kodeKegiatan, namaKegiatan, kodeFungsi, namaFungsi);
        this.kodeRekenings = kodeRekenings;
    }
    public KegiatanAPBD(long index, String kodeUrusanProgram, String namaUrusanProgram, String kodeUrusanPelaksana, String namaUrusanPelaksana, String kodeSKPD, String namaSKPD, String kodeProgram, String namaProgram, String kodeKegiatan, String namaKegiatan, String kodeFungsi, String namaFungsi) {
        this(kodeUrusanProgram, namaUrusanProgram, kodeUrusanPelaksana, namaUrusanPelaksana, kodeSKPD, namaSKPD, kodeProgram, namaProgram, kodeKegiatan, namaKegiatan, kodeFungsi, namaFungsi);
        this.index = index;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getKodeUrusanProgram() {
        return kodeUrusanProgram;
    }

    public void setKodeUrusanProgram(String kodeUrusanProgram) {
        this.kodeUrusanProgram = kodeUrusanProgram;
    }

    public String getNamaUrusanProgram() {
        return namaUrusanProgram;
    }

    public void setNamaUrusanProgram(String namaUrusanProgram) {
        this.namaUrusanProgram = namaUrusanProgram;
    }

    public String getKodeUrusanPelaksana() {
        return kodeUrusanPelaksana;
    }

    public void setKodeUrusanPelaksana(String kodeUrusanPelaksana) {
        this.kodeUrusanPelaksana = kodeUrusanPelaksana;
    }

    public String getNamaUrusanPelaksana() {
        return namaUrusanPelaksana;
    }

    public void setNamaUrusanPelaksana(String namaUrusanPelaksana) {
        this.namaUrusanPelaksana = namaUrusanPelaksana;
    }

    public String getKodeSKPD() {
        return kodeSKPD;
    }

    public void setKodeSKPD(String kodeSKPD) {
        this.kodeSKPD = kodeSKPD;
    }

    public String getNamaSKPD() {
        return namaSKPD;
    }

    public void setNamaSKPD(String namaSKPD) {
        this.namaSKPD = namaSKPD;
    }

    public String getKodeProgram() {
        return kodeProgram;
    }

    public void setKodeProgram(String kodeProgram) {
        this.kodeProgram = kodeProgram;
    }

    public String getNamaProgram() {
        return namaProgram;
    }

    public void setNamaProgram(String namaProgram) {
        this.namaProgram = namaProgram;
    }

    public String getKodeKegiatan() {
        return kodeKegiatan;
    }

    public void setKodeKegiatan(String kodeKegiatan) {
        this.kodeKegiatan = kodeKegiatan;
    }

    public String getNamaKegiatan() {
        return namaKegiatan;
    }

    public void setNamaKegiatan(String namaKegiatan) {
        this.namaKegiatan = namaKegiatan;
    }

    public String getKodeFungsi() {
        return kodeFungsi;
    }

    public void setKodeFungsi(String kodeFungsi) {
        this.kodeFungsi = kodeFungsi;
    }

    public String getNamaFungsi() {
        return namaFungsi;
    }

    public void setNamaFungsi(String namaFungsi) {
        this.namaFungsi = namaFungsi;
    }

    public List<KodeRekeningAPBD> getKodeRekenings() {
        return kodeRekenings;
    }

    public void setKodeRekenings(List<KodeRekeningAPBD> kodeRekenings) {
        this.kodeRekenings = kodeRekenings;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.index ^ (this.index >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KegiatanAPBD other = (KegiatanAPBD) obj;
        if (this.index != other.index) {
            return false;
        }
        return true;
    }
    
    
}