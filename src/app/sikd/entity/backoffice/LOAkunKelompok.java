/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class LOAkunKelompok extends ObjNeracaAkun implements Serializable, Comparable<LOAkunKelompok>, Cloneable {
    List<LOAkunJenis> akunJeniss;

    public LOAkunKelompok() {
    }
    
    public LOAkunKelompok(String kodeAkun, String namaAkun, double nilai) {
        super(kodeAkun, namaAkun, nilai);
    }
    
    public LOAkunKelompok(long index, String kodeAkun, String namaAkun, double nilai) {
        super(index, kodeAkun, namaAkun, nilai);
    }

    public List<LOAkunJenis> getAkunJeniss() {
        return akunJeniss;
    }

    public void setAkunJeniss(List<LOAkunJenis> akunJeniss) {
        this.akunJeniss = akunJeniss;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return getNamaAkun();
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final LOAkunJenis other = (LOAkunJenis) obj;
        if(!super.getKodeAkun().equals(other.getKodeAkun())) return false;
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(LOAkunKelompok t) {
        return this.getKodeAkun().compareTo(t.getKodeAkun());
    }
    
    /**
     *
     * @return
     * @throws java.lang.CloneNotSupportedException
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}