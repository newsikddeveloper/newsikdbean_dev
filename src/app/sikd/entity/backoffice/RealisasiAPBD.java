package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class RealisasiAPBD implements Serializable{
    private long index;
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private short periode;
    private short kodeData;
    private short jenisCOA;
    private short statusData;
    private String nomorPerda;
    private Date tanggalPerda;
    private String userName;
    private String password;
    private String namaAplikasi;
    private String pengembangAplikasi;
    private List<RealisasiKegiatanAPBD> kegiatans;    

    public RealisasiAPBD() {
    }

    public RealisasiAPBD(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short periode, short kodeData, short jenisCOA, short statusData, String nomorPerda, Date tanggalPerda, String userName, String password, String namaAplikasi, String pengembangAplikasi){    
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.periode = periode;
        this.kodeData = kodeData;
        this.jenisCOA = jenisCOA;
        this.statusData = statusData;
        this.nomorPerda = nomorPerda;
        this.tanggalPerda = tanggalPerda;
        this.userName = userName;
        this.password = password;
        this.namaAplikasi = namaAplikasi;
        this.pengembangAplikasi = pengembangAplikasi;
    }
    
    public RealisasiAPBD(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short periode, short kodeData, short jenisCOA, short statusData, String nomorPerda, Date tanggalPerda, String namaAplikasi, String pengembangAplikasi){    
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.periode = periode;
        this.kodeData = kodeData;
        this.jenisCOA = jenisCOA;
        this.statusData = statusData;
        this.nomorPerda = nomorPerda;
        this.tanggalPerda = tanggalPerda;
        this.namaAplikasi = namaAplikasi;
        this.pengembangAplikasi = pengembangAplikasi;
    }
    
    public RealisasiAPBD(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short periode, short kodeData, short jenisCOA, short statusData, String nomorPerda, Date tanggalPerda, String userName, String password, String namaAplikasi, String pengembangAplikasi){    
        this(kodeSatker, kodePemda, namaPemda, tahunAnggaran, periode, kodeData, jenisCOA, statusData, nomorPerda, tanggalPerda, userName, password, namaAplikasi, pengembangAplikasi);
        this.index = index;
    }
    
    public RealisasiAPBD(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short periode, short kodeData, short jenisCOA, short statusData, String nomorPerda, Date tanggalPerda, String namaAplikasi, String pengembangAplikasi){    
        this(kodeSatker, kodePemda, namaPemda, tahunAnggaran, periode, kodeData, jenisCOA, statusData, nomorPerda, tanggalPerda, "", "", namaAplikasi, pengembangAplikasi);
        this.index = index;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) {
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public short getPeriode() {
        return periode;
    }

    public void setPeriode(short periode) {
        this.periode = periode;
    }

    public short getKodeData() {
        return kodeData;
    }

    public void setKodeData(short kodeData) {
        this.kodeData = kodeData;
    }

    public short getJenisCOA() {
        return jenisCOA;
    }

    public void setJenisCOA(short jenisCOA) {
        this.jenisCOA = jenisCOA;
    }

    public short getStatusData() {
        return statusData;
    }

    public void setStatusData(short statusData) {
        this.statusData = statusData;
    }

    public String getNomorPerda() {
        return nomorPerda;
    }

    public void setNomorPerda(String nomorPerda) {
        this.nomorPerda = nomorPerda;
    }

    public Date getTanggalPerda() {
        return tanggalPerda;
    }

    public void setTanggalPerda(Date tanggalPerda) {
        this.tanggalPerda = tanggalPerda;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNamaAplikasi() {
        return namaAplikasi;
    }

    public void setNamaAplikasi(String namaAplikasi) {
        this.namaAplikasi = namaAplikasi;
    }

    public String getPengembangAplikasi() {
        return pengembangAplikasi;
    }

    public void setPengembangAplikasi(String pengembangAplikasi) {
        this.pengembangAplikasi = pengembangAplikasi;
    }

    public List<RealisasiKegiatanAPBD> getKegiatans() {
        return kegiatans;
    }

    public void setKegiatans(List<RealisasiKegiatanAPBD> kegiatans) {
        this.kegiatans = kegiatans;
    }
    
    
    
        
}
