/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class AkunKompilasiGFS extends AkunKompilasi implements Serializable{
    private String akunGFS;

    public AkunKompilasiGFS() {
    }

    public AkunKompilasiGFS(String akunGFS, String kodeAkun, String namaAkun, double nilai) {
        super(kodeAkun, namaAkun, nilai);
        this.akunGFS = akunGFS;
    }

    public String getAkunGFS() {
        return akunGFS;
    }

    public void setAkunGFS(String akunGFS) {
        this.akunGFS = akunGFS;
    }
    
}
