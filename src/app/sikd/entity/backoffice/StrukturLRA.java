/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class StrukturLRA implements Serializable{
    private String kode;
    private String uraian;
    private List<String> kodeAkun;
    private List<StrukturLRA> anaks;
    private String jumlah;

    public StrukturLRA(String kode, String uraian, String jumlah) {
        this.kode = kode;
        this.uraian = uraian;
        this.jumlah = jumlah;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public List<String> getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(List<String> kodeAkun) {
        this.kodeAkun = kodeAkun;
    }

    public List<StrukturLRA> getAnaks() {
        return anaks;
    }

    public void setAnaks(List<StrukturLRA> anaks) {
        this.anaks = anaks;
    }
    
    
    
    
    
}
