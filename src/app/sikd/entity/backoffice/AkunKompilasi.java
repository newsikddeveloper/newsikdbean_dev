/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;

/**
 *
 * @author sora
 */
public class AkunKompilasi implements Serializable{
    private short tahun;
    private String kodesatker;
    private String kodeAkun;
    private String namaAkun;
    private double nilai;
    private String style;

    public AkunKompilasi() {
    }

    public AkunKompilasi(String kodeAkun, String namaAkun, double nilai) {
        this.kodeAkun = kodeAkun;
        this.namaAkun = namaAkun;
        this.nilai = nilai;
    }

    public AkunKompilasi(short tahun, String kodesatker, String kodeAkun, String namaAkun, double nilai) {
        this(kodeAkun, namaAkun, nilai);
        this.tahun = tahun;
        this.kodesatker = kodesatker;
    }
    

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }
    
    public String getNilaiAsString() {
        return SIKDUtil.doubleToString(nilai);
    }

    public void setNilaiAsString(String nilaiAsString) {
        nilai = SIKDUtil.stringToDouble(nilaiAsString);
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) {
        this.namaAkun = namaAkun;
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) {
        this.kodeAkun = kodeAkun;
    }

    public short getTahun() {
        return tahun;
    }

    public void setTahun(short tahun) {
        this.tahun = tahun;
    }

    public String getKodesatker() {
        return kodesatker;
    }

    public void setKodesatker(String kodesatker) {
        this.kodesatker = kodesatker;
    }     

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
    
    
    
}
