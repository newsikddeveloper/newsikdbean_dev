package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author detra
 */

public class RincianBPHTB implements Serializable{    
    private String namaPenerima;
    private String alamatPenerima;
    private String npwpPenerima;
    private String kppPenerima;
    private String cabangNpwpPenerima;
    private String alamatObjek;
    private double nilaiPerolehan;
    private double luasTanah;
    private double luasBangunan;
    private Date tanggalTransaksi;
    private double nilaiBphtb;

    public RincianBPHTB() {
    }    

    public RincianBPHTB(String namaPenerima, String alamatPenerima, String npwpPenerima, String kppPenerima, String cabangNpwpPenerima, String alamatObjek, double nilaiPerolehan, double luasTanah, double luasBangunan, Date tanggalTransaksi, double nilaiBphtb) {
        this.namaPenerima = namaPenerima;
        this.alamatPenerima = alamatPenerima;
        this.npwpPenerima = npwpPenerima;
        this.kppPenerima = kppPenerima;
        this.cabangNpwpPenerima = cabangNpwpPenerima;
        this.alamatObjek = alamatObjek;
        this.nilaiPerolehan = nilaiPerolehan;
        this.luasTanah = luasTanah;
        this.luasBangunan = luasBangunan;
        this.tanggalTransaksi = tanggalTransaksi;
        this.nilaiBphtb = nilaiBphtb;
    }
    
    public String getNamaPenerima() {
        return namaPenerima;
    }

    public void setNamaPenerima(String namaPenerima) {
        this.namaPenerima = namaPenerima;
    }

    public String getAlamatPenerima() {
        return alamatPenerima;
    }

    public void setAlamatPenerima(String alamatPenerima) {
        this.alamatPenerima = alamatPenerima;
    }

    public String getNpwpPenerima() {
        return npwpPenerima;
    }

    public void setNpwpPenerima(String npwpPenerima) {
        this.npwpPenerima = npwpPenerima;
    }

    public String getKppPenerima() {
        return kppPenerima;
    }

    public void setKppPenerima(String kppPenerima) {
        this.kppPenerima = kppPenerima;
    }

    public String getCabangNpwpPenerima() {
        return cabangNpwpPenerima;
    }

    public void setCabangNpwpPenerima(String cabangNpwpPenerima) {
        this.cabangNpwpPenerima = cabangNpwpPenerima;
    }

    public String getAlamatObjek() {
        return alamatObjek;
    }

    public void setAlamatObjek(String alamatObjek) {
        this.alamatObjek = alamatObjek;
    }

    public double getNilaiPerolehan() {
        return nilaiPerolehan;
    }

    public void setNilaiPerolehan(double nilaiPerolehan) {
        this.nilaiPerolehan = nilaiPerolehan;
    }

    public double getLuasTanah() {
        return luasTanah;
    }

    public void setLuasTanah(double luasTanah) {
        this.luasTanah = luasTanah;
    }

    public double getLuasBangunan() {
        return luasBangunan;
    }

    public void setLuasBangunan(double luasBangunan) {
        this.luasBangunan = luasBangunan;
    }

    public Date getTanggalTransaksi() {
        return tanggalTransaksi;
    }

    public void setTanggalTransaksi(Date tanggalTransaksi) {
        this.tanggalTransaksi = tanggalTransaksi;
    }

    public double getNilaiBphtb() {
        return nilaiBphtb;
    }

    public void setNilaiBphtb(double nilaiBphtb) {
        this.nilaiBphtb = nilaiBphtb;
    }
    
}
