/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class LOReport implements Serializable{
    private String no;
    private String uraian;
    private String nilai;
    private String nilaiMin;
    private String naikTurun;
    private String persen;
    private String fontStyle;

    public LOReport() {
    }

    public LOReport(String no, String uraian, String nilai, String nilaiMin, String naikTurun, String persen, String fontStyle) {
        this.no = no;
        this.uraian = uraian;
        this.nilai = nilai;
        this.nilaiMin = nilaiMin;
        this.naikTurun = naikTurun;
        this.persen = persen;
        this.fontStyle = fontStyle;
    }

    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public String getNilaiMin() {
        return nilaiMin;
    }

    public void setNilaiMin(String nilaiMin) {
        this.nilaiMin = nilaiMin;
    }

    public String getNaikTurun() {
        return naikTurun;
    }

    public void setNaikTurun(String naikTurun) {
        this.naikTurun = naikTurun;
    }

    public String getPersen() {
        return persen;
    }

    public void setPersen(String persen) {
        this.persen = persen;
    }
}
