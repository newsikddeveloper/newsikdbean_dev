/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;

/**
 *
 * @author sora
 */
public class LOSurplusNonOperasional implements Serializable, Cloneable{
    private long index;
    private double penjualanAsetNonLancar;
    private double kewajibanJangkaPanjang;
    private double surplusLainnya;
    
    public LOSurplusNonOperasional() {
    }

    public LOSurplusNonOperasional(double penjualanAsetNonLancar, double kewajibanJangkaPanjang, double surplusLainnya) {
        this.penjualanAsetNonLancar = penjualanAsetNonLancar;
        this.kewajibanJangkaPanjang = kewajibanJangkaPanjang;
        this.surplusLainnya = surplusLainnya;
    }

    public LOSurplusNonOperasional(long index, double penjualanAsetNonLancar, double kewajibanJangkaPanjang, double surplusLainnya) {
        this.index = index;
        this.penjualanAsetNonLancar = penjualanAsetNonLancar;
        this.kewajibanJangkaPanjang = kewajibanJangkaPanjang;
        this.surplusLainnya = surplusLainnya;
    }

    public double getSurplusLainnya() {
        return surplusLainnya;
    }

    public void setSurplusLainnya(double surplusLainnya) {
        this.surplusLainnya = surplusLainnya;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public double getPenjualanAsetNonLancar() {
        return penjualanAsetNonLancar;
    }

    public void setPenjualanAsetNonLancar(double penjualanAsetNonLancar) {
        this.penjualanAsetNonLancar = penjualanAsetNonLancar;
    }

    public double getKewajibanJangkaPanjang() {
        return kewajibanJangkaPanjang;
    }

    public void setKewajibanJangkaPanjang(double kewajibanJangkaPanjang) {
        this.kewajibanJangkaPanjang = kewajibanJangkaPanjang;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getPenjualanAsetNonLancarAsString() {
        return SIKDUtil.doubleToString(penjualanAsetNonLancar);
    }

    public void setPenjualanAsetNonLancarAsString(String penjualanAsetNonLancarAsString) {
        this.penjualanAsetNonLancar = SIKDUtil.stringToDouble(penjualanAsetNonLancarAsString);
    }

    public String getKewajibanJangkaPanjangAsString() {
        return SIKDUtil.doubleToString(kewajibanJangkaPanjang);
    }

    public void setKewajibanJangkaPanjangAsString(String kewajibanJangkaPanjangAsString) {
        this.kewajibanJangkaPanjang = SIKDUtil.stringToDouble(kewajibanJangkaPanjangAsString);
    }

    public String getSurplusLainnyaAsString() {
        return SIKDUtil.doubleToString(surplusLainnya);
    }

    public void setSurplusLainnyaAsString(String surplusLainnyaAsString) {
        this.surplusLainnya = SIKDUtil.stringToDouble(surplusLainnyaAsString);
    }
    
}
