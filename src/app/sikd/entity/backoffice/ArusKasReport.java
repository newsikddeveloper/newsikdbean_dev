/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class ArusKasReport implements Serializable{
    private String namaAkun;
    private double nilai;
    private double nilaiPra;
    private String strNilai;
    private String strNilaiPra;
    private short type;
    
    public static final short TYPE_PLAIN = 0;
    public static final short TYPE_BOLD = 1;
    public static final short TYPE_BOLD_BACKGROUND = 2;

    public ArusKasReport() {
    }

    public ArusKasReport(String namaAkun, double nilai) {
        this.namaAkun = namaAkun;
        this.nilai = nilai;
    }

    public ArusKasReport(String namaAkun, double nilai, double nilaiPra) {
        this.namaAkun = namaAkun;
        this.nilai = nilai;
        this.nilaiPra = nilaiPra;
    }
    public ArusKasReport(String namaAkun, double nilai, double nilaiPra, short type) {
        this.namaAkun = namaAkun;
        this.nilai = nilai;
        this.nilaiPra = nilaiPra;
        this.type = type;
    }

    public ArusKasReport(String namaAkun, String strNilai, String strNilaiPra, short type) {
        this.namaAkun = namaAkun;
        this.strNilai = strNilai;
        this.strNilaiPra = strNilaiPra;
        this.type = type;
    }

    public double getNilaiPra() {
        return nilaiPra;
    }

    public void setNilaiPra(double nilaiPra) {
        this.nilaiPra = nilaiPra;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) {
        this.namaAkun = namaAkun;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }
    
    public String getTypeAsString(){
        if( type == TYPE_BOLD ) return "font-weight: bold;";
        else if(type==TYPE_BOLD_BACKGROUND) return "font-weight: bold; background-color: activecaption;";
        else return "";
    }
    
    public void setTypeAsString(){ }

    public String getStrNilai() {
        return strNilai;
    }

    public void setStrNilai(String strNilai) {
        this.strNilai = strNilai;
    }

    public String getStrNilaiPra() {
        return strNilaiPra;
    }

    public void setStrNilaiPra(String strNilaiPra) {
        this.strNilaiPra = strNilaiPra;
    }
    
    
    
}
