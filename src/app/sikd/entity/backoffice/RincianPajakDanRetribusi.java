/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class RincianPajakDanRetribusi implements Serializable{    
    private String namaPungutan;
    private short jenisPungutan;
    private String dasarHukum;
    private double jumlahPungutan;

    public RincianPajakDanRetribusi() {
    }        

    public RincianPajakDanRetribusi(String namaPungutan, short jenisPungutan, String dasarHukum, double jumlahPungutan) {
        this.namaPungutan = namaPungutan;
        this.jenisPungutan = jenisPungutan;
        this.dasarHukum = dasarHukum;
        this.jumlahPungutan = jumlahPungutan;
    }
    
    public String getNamaPungutan() {
        return namaPungutan;
    }

    public void setNamaPungutan(String namaPungutan) {
        this.namaPungutan = namaPungutan;
    }
    
    public short getJenisPungutan() {
        return jenisPungutan;
    }

    public void setJenisPungutan(short jenisPungutan) {
        this.jenisPungutan = jenisPungutan;
    }

    public String getDasarHukum() {
        return dasarHukum;
    }

    public void setDasarHukum(String dasarHukum) {
        this.dasarHukum = dasarHukum;
    }

    public double getJumlahPungutan() {
        return jumlahPungutan;
    }

    public void setJumlahPungutan(double jumlahPungutan) {
        this.jumlahPungutan = jumlahPungutan;
    }
}