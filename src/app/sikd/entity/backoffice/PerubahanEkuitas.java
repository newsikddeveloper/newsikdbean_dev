/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sora
 */
public class PerubahanEkuitas implements Serializable, Cloneable{
    private long index;
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private double ekuitasAwal;
    private double surplusDefisitLO;
    private double koreksiNilaiPersediaan;
    private double selisihRevaluasiAset;
    private double lainLain;

    public PerubahanEkuitas() {
    }

    public PerubahanEkuitas(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran) {
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
    }

    public PerubahanEkuitas(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran) {
        this(kodeSatker, kodePemda, namaPemda, tahunAnggaran);
        this.index = index;
    }

    public PerubahanEkuitas(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, double ekuitasAwal, double surplusDefisitLO, double koreksiNilaiPersediaan, double selisihRevaluasiAset, double lainLain) {
        this(kodeSatker, kodePemda, namaPemda, tahunAnggaran);
        this.ekuitasAwal = ekuitasAwal;
        this.surplusDefisitLO = surplusDefisitLO;
        this.koreksiNilaiPersediaan = koreksiNilaiPersediaan;
        this.selisihRevaluasiAset = selisihRevaluasiAset;
        this.lainLain = lainLain;
    }

    public PerubahanEkuitas(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, double ekuitasAwal, double surplusDefisitLO, double koreksiNilaiPersediaan, double selisihRevaluasiAset, double lainLain) {
        this(kodeSatker, kodePemda, namaPemda, tahunAnggaran, ekuitasAwal, surplusDefisitLO, koreksiNilaiPersediaan, selisihRevaluasiAset, lainLain);
        this.index = index;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) {
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public double getEkuitasAwal() {
        return ekuitasAwal;
    }

    public void setEkuitasAwal(double ekuitasAwal) {
        this.ekuitasAwal = ekuitasAwal;
    }

    public double getSurplusDefisitLO() {
        return surplusDefisitLO;
    }

    public void setSurplusDefisitLO(double surplusDefisitLO) {
        this.surplusDefisitLO = surplusDefisitLO;
    }

    public double getSelisihRevaluasiAset() {
        return selisihRevaluasiAset;
    }

    public void setSelisihRevaluasiAset(double selisihRevaluasiAset) {
        this.selisihRevaluasiAset = selisihRevaluasiAset;
    }

    public double getLainLain() {
        return lainLain;
    }

    public void setLainLain(double lainLain) {
        this.lainLain = lainLain;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + (int) (this.index ^ (this.index >>> 32));
        hash = 37 * hash + Objects.hashCode(this.kodeSatker);
        hash = 37 * hash + Objects.hashCode(this.kodePemda);
        hash = 37 * hash + Objects.hashCode(this.namaPemda);
        hash = 37 * hash + this.tahunAnggaran;
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.ekuitasAwal) ^ (Double.doubleToLongBits(this.ekuitasAwal) >>> 32));
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.surplusDefisitLO) ^ (Double.doubleToLongBits(this.surplusDefisitLO) >>> 32));
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.selisihRevaluasiAset) ^ (Double.doubleToLongBits(this.selisihRevaluasiAset) >>> 32));
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.lainLain) ^ (Double.doubleToLongBits(this.lainLain) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PerubahanEkuitas other = (PerubahanEkuitas) obj;
        if (!Objects.equals(this.kodeSatker, other.kodeSatker)) {
            return false;
        }
        if (!Objects.equals(this.kodePemda, other.kodePemda)) {
            return false;
        }
        if (!Objects.equals(this.namaPemda, other.namaPemda)) {
            return false;
        }
        if (this.tahunAnggaran != other.tahunAnggaran) {
            return false;
        }
        if (Double.doubleToLongBits(this.ekuitasAwal) != Double.doubleToLongBits(other.ekuitasAwal)) {
            return false;
        }
        if (Double.doubleToLongBits(this.surplusDefisitLO) != Double.doubleToLongBits(other.surplusDefisitLO)) {
            return false;
        }
        if (Double.doubleToLongBits(this.selisihRevaluasiAset) != Double.doubleToLongBits(other.selisihRevaluasiAset)) {
            return false;
        }
        if (Double.doubleToLongBits(this.lainLain) != Double.doubleToLongBits(other.lainLain)) {
            return false;
        }
        return true;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    
    
    public String getEkuitasAwalAsString(){
        return SIKDUtil.doubleToString(ekuitasAwal);
    }
    public void setEkuitasAwalAsString(String ekuitasAwalAsString){
//        if(ekuitasAwalAsString != null && !ekuitasAwalAsString.trim().equals(""))
            ekuitasAwal = SIKDUtil.stringToDouble(ekuitasAwalAsString);
//        ekuitasAwal = 0;
    }
    
    public String getSurplusDefisitLOAsString(){
        return SIKDUtil.doubleToString(surplusDefisitLO);
    }
    public void setSurplusDefisitLOAsString(String surplusDefisitLOAsString){
//        if(surplusDefisitLOAsString != null && !surplusDefisitLOAsString.trim().equals(""))
            surplusDefisitLO = SIKDUtil.stringToDouble(surplusDefisitLOAsString);
    }
    
    public String getSelisihRevaluasiAsetAsString(){
        return SIKDUtil.doubleToString(selisihRevaluasiAset);
    }
    public void setSelisihRevaluasiAsetAsString(String selisihRevaluasiAsetAsString){
//        if(surplusDefisitLOAsString != null && !surplusDefisitLOAsString.trim().equals(""))
            selisihRevaluasiAset = SIKDUtil.stringToDouble(selisihRevaluasiAsetAsString);
    }
    
    public String getLainLainAsString(){
        return SIKDUtil.doubleToString(lainLain);
    }
    public void setLainLainAsString(String lainLainAsString){
//        if(surplusDefisitLOAsString != null && !surplusDefisitLOAsString.trim().equals(""))
            lainLain = SIKDUtil.stringToDouble(lainLainAsString);
    }

    public double getKoreksiNilaiPersediaan() {
        return koreksiNilaiPersediaan;
    }

    public void setKoreksiNilaiPersediaan(double koreksiNilaiPersediaan) {
        this.koreksiNilaiPersediaan = koreksiNilaiPersediaan;
    }
    
    public String getKoreksiNilaiPersediaanAsString(){
        return SIKDUtil.doubleToString(koreksiNilaiPersediaan);
    }
    public void setKoreksiNilaiPersediaanAsString(String koreksiNilaiPersediaanAsString){
//        if(surplusDefisitLOAsString != null && !surplusDefisitLOAsString.trim().equals(""))
            koreksiNilaiPersediaan = SIKDUtil.stringToDouble(koreksiNilaiPersediaanAsString);
    }
    
}
