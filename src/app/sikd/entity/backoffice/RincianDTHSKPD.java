package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author detra
 */

public class RincianDTHSKPD implements Serializable{    
    private long indexRincian;
    private String nomorSPM;
    private String nomorSP2D;
    private short jenisSP2D;
    private Date tanggalSP2D;  
    private double nilaiSP2D;
    private double nilaiTotalPajak;
    private double nilaiTotalPotongan;
    private String npwpBUD;
    private String npwpSKPD;
    private String npwpVendor;
    private String namaVendor;
    private String kodeProgram;
    private String namaProgram;
    private String kodeKegiatan;
    private String namaKegiatan;
    private short sumberDanaSP2D;
    private String subSumberDanaSP2D;
    private short tahapSalurSumberDana;
    private String keterangan;
    private List<AkunDTHSKPD> akuns;
    private List<PajakDTHSKPD> pajaks;
    private List<PotonganDTHSKPD> potongans;

    public RincianDTHSKPD() {
    }

    public RincianDTHSKPD(String nomorSPM, String nomorSP2D, short jenisSP2D, Date tanggalSP2D, double nilaiSP2D, double nilaiTotalPajak, double nilaiTotalPotongan, String npwpBUD, String npwpSKPD, String npwpVendor, String namaVendor, String kodeProgram, String namaProgram, String kodeKegiatan, String namaKegiatan, short sumberDanaSP2D, String subSumberDanaSP2D, short tahapSalurSumberDana, String keterangan) {
        this.nomorSPM = nomorSPM;
        this.nomorSP2D = nomorSP2D;
        this.jenisSP2D = jenisSP2D;
        this.tanggalSP2D = tanggalSP2D;
        this.nilaiSP2D = nilaiSP2D;
        this.nilaiTotalPajak = nilaiTotalPajak;
        this.nilaiTotalPotongan = nilaiTotalPotongan;
        this.npwpBUD = npwpBUD;
        this.npwpSKPD = npwpSKPD;
        this.npwpVendor = npwpVendor;
        this.namaVendor = namaVendor;
        this.kodeProgram = kodeProgram;
        this.namaProgram = namaProgram;
        this.kodeKegiatan = kodeKegiatan;
        this.namaKegiatan = namaKegiatan;
        this.sumberDanaSP2D = sumberDanaSP2D;
        this.subSumberDanaSP2D = subSumberDanaSP2D;
        this.tahapSalurSumberDana = tahapSalurSumberDana;
        this.keterangan = keterangan;
    }

    public RincianDTHSKPD(long indexRincian, String nomorSPM, String nomorSP2D, short jenisSP2D, Date tanggalSP2D, double nilaiSP2D, double nilaiTotalPajak, double nilaiTotalPotongan, String npwpBUD, String npwpSKPD, String npwpVendor, String namaVendor, String kodeProgram, String namaProgram, String kodeKegiatan, String namaKegiatan, short sumberDanaSP2D, String subSumberDanaSP2D, short tahapSalurSumberDana, String keterangan) {
        this.indexRincian = indexRincian;
        this.nomorSPM = nomorSPM;
        this.nomorSP2D = nomorSP2D;
        this.jenisSP2D = jenisSP2D;
        this.tanggalSP2D = tanggalSP2D;
        this.nilaiSP2D = nilaiSP2D;
        this.nilaiTotalPajak = nilaiTotalPajak;
        this.nilaiTotalPotongan = nilaiTotalPotongan;
        this.npwpBUD = npwpBUD;
        this.npwpSKPD = npwpSKPD;
        this.npwpVendor = npwpVendor;
        this.namaVendor = namaVendor;
        this.kodeProgram = kodeProgram;
        this.namaProgram = namaProgram;
        this.kodeKegiatan = kodeKegiatan;
        this.namaKegiatan = namaKegiatan;
        this.sumberDanaSP2D = sumberDanaSP2D;
        this.subSumberDanaSP2D = subSumberDanaSP2D;
        this.tahapSalurSumberDana = tahapSalurSumberDana;
        this.keterangan = keterangan;
    }

    public List<AkunDTHSKPD> getAkuns() {
        return akuns;
    }

    public void setAkuns(List<AkunDTHSKPD> akuns) {
        this.akuns = akuns;
    }

    public List<PajakDTHSKPD> getPajaks() {
        return pajaks;
    }

    public void setPajaks(List<PajakDTHSKPD> pajaks) {
        this.pajaks = pajaks;
    }

    public List<PotonganDTHSKPD> getPotongans() {
        return potongans;
    }

    public void setPotongans(List<PotonganDTHSKPD> potongans) {
        this.potongans = potongans;
    }

    public long getIndexRincian() {
        return indexRincian;
    }

    public void setIndexRincian(long indexRincian) {
        this.indexRincian = indexRincian;
    }

    public String getNomorSPM() {
        return nomorSPM;
    }

    public void setNomorSPM(String nomorSPM) {
        this.nomorSPM = nomorSPM;
    }

    public String getNomorSP2D() {
        return nomorSP2D;
    }

    public void setNomorSP2D(String nomorSP2D) {
        this.nomorSP2D = nomorSP2D;
    }

    public short getJenisSP2D() {
        return jenisSP2D;
    }

    public void setJenisSP2D(short jenisSP2D) {
        this.jenisSP2D = jenisSP2D;
    }

    public Date getTanggalSP2D() {
        return tanggalSP2D;
    }

    public void setTanggalSP2D(Date tanggalSP2D) {
        this.tanggalSP2D = tanggalSP2D;
    }

    public double getNilaiSP2D() {
        return nilaiSP2D;
    }

    public void setNilaiSP2D(double nilaiSP2D) {
        this.nilaiSP2D = nilaiSP2D;
    }

    public double getNilaiTotalPajak() {
        return nilaiTotalPajak;
    }

    public void setNilaiTotalPajak(double nilaiTotalPajak) {
        this.nilaiTotalPajak = nilaiTotalPajak;
    }

    public double getNilaiTotalPotongan() {
        return nilaiTotalPotongan;
    }

    public void setNilaiTotalPotongan(double nilaiTotalPotongan) {
        this.nilaiTotalPotongan = nilaiTotalPotongan;
    }

    public String getNpwpBUD() {
        return npwpBUD;
    }

    public void setNpwpBUD(String npwpBUD) {
        this.npwpBUD = npwpBUD;
    }

    public String getNpwpSKPD() {
        return npwpSKPD;
    }

    public void setNpwpSKPD(String npwpSKPD) {
        this.npwpSKPD = npwpSKPD;
    }

    public String getNpwpVendor() {
        return npwpVendor;
    }

    public void setNpwpVendor(String npwpVendor) {
        this.npwpVendor = npwpVendor;
    }

    public String getNamaVendor() {
        return namaVendor;
    }

    public void setNamaVendor(String namaVendor) {
        this.namaVendor = namaVendor;
    }

    public String getKodeProgram() {
        return kodeProgram;
    }

    public void setKodeProgram(String kodeProgram) {
        this.kodeProgram = kodeProgram;
    }

    public String getNamaProgram() {
        return namaProgram;
    }

    public void setNamaProgram(String namaProgram) {
        this.namaProgram = namaProgram;
    }

    public String getKodeKegiatan() {
        return kodeKegiatan;
    }

    public void setKodeKegiatan(String kodeKegiatan) {
        this.kodeKegiatan = kodeKegiatan;
    }

    public String getNamaKegiatan() {
        return namaKegiatan;
    }

    public void setNamaKegiatan(String namaKegiatan) {
        this.namaKegiatan = namaKegiatan;
    }

    public short getSumberDanaSP2D() {
        return sumberDanaSP2D;
    }
    

    public void setSumberDanaSP2D(short sumberDanaSP2D) {
        this.sumberDanaSP2D = sumberDanaSP2D;
    }

    public String getSubSumberDanaSP2D() {
        return subSumberDanaSP2D;
    }

    public void setSubSumberDanaSP2D(String subSumberDanaSP2D) {
        this.subSumberDanaSP2D = subSumberDanaSP2D;
    }

    public short getTahapSalurSumberDana() {
        return tahapSalurSumberDana;
    }

    public void setTahapSalurSumberDana(short tahapSalurSumberDana) {
        this.tahapSalurSumberDana = tahapSalurSumberDana;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    
}
