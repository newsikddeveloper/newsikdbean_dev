package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author detra
 */

public class RincianIzinUsaha implements Serializable{    
    private String nomorIzin;
    private Date tanggalIzin;
    private String namaPerusahaan;
    private String alamatPerusahaan;
    private String npwpPerusahaan;
    private String kppPerusahaan;
    private String cabangNpwpPerusahaan;
    private String jenisUsaha;
    private String namaPemilik;
    private String alamatPemilik;
    private String npwpPemilik;
    private String kppPemilik;
    private String cabangNpwpPemilik;
    private String klasifikasi;
    private double modal;
    private int jumlahKaryawan;
    private short masaBerlaku;

    public RincianIzinUsaha() {
    }        

    public RincianIzinUsaha(String nomorIzin, Date tanggalIzin, String namaPerusahaan, String alamatPerusahaan, String npwpPerusahaan, String kppPerusahaan, String cabangNpwpPerusahaan, String jenisUsaha, String namaPemilik, String alamatPemilik, String npwpPemilik, String kppPemilik, String cabangNpwpPemilik, String klasifikasi, double modal, int jumlahKaryawan, short masaBerlaku) {
        this.nomorIzin = nomorIzin;
        this.tanggalIzin = tanggalIzin;
        this.namaPerusahaan = namaPerusahaan;
        this.alamatPerusahaan = alamatPerusahaan;
        this.npwpPerusahaan = npwpPerusahaan;
        this.kppPerusahaan = kppPerusahaan;
        this.cabangNpwpPerusahaan = cabangNpwpPerusahaan;
        this.jenisUsaha = jenisUsaha;
        this.namaPemilik = namaPemilik;
        this.alamatPemilik = alamatPemilik;
        this.npwpPemilik = npwpPemilik;
        this.kppPemilik = kppPemilik;
        this.cabangNpwpPemilik = cabangNpwpPemilik;
        this.klasifikasi = klasifikasi;
        this.modal = modal;
        this.jumlahKaryawan = jumlahKaryawan;
        this.masaBerlaku = masaBerlaku;
    }
    
    

    public String getNomorIzin() {
        return nomorIzin;
    }

    public void setNomorIzin(String nomorIzin) {
        this.nomorIzin = nomorIzin;
    }

    public Date getTanggalIzin() {
        return tanggalIzin;
    }

    public void setTanggalIzin(Date tanggalIzin) {
        this.tanggalIzin = tanggalIzin;
    }

    public String getNamaPerusahaan() {
        return namaPerusahaan;
    }

    public void setNamaPerusahaan(String namaPerusahaan) {
        this.namaPerusahaan = namaPerusahaan;
    }

    public String getAlamatPerusahaan() {
        return alamatPerusahaan;
    }

    public void setAlamatPerusahaan(String alamatPerusahaan) {
        this.alamatPerusahaan = alamatPerusahaan;
    }

    public String getNpwpPerusahaan() {
        return npwpPerusahaan;
    }

    public void setNpwpPerusahaan(String npwpPerusahaan) {
        this.npwpPerusahaan = npwpPerusahaan;
    }

    public String getKppPerusahaan() {
        return kppPerusahaan;
    }

    public void setKppPerusahaan(String kppPerusahaan) {
        this.kppPerusahaan = kppPerusahaan;
    }

    public String getCabangNpwpPerusahaan() {
        return cabangNpwpPerusahaan;
    }

    public void setCabangNpwpPerusahaan(String cabangNpwpPerusahaan) {
        this.cabangNpwpPerusahaan = cabangNpwpPerusahaan;
    }

    public String getJenisUsaha() {
        return jenisUsaha;
    }

    public void setJenisUsaha(String jenisUsaha) {
        this.jenisUsaha = jenisUsaha;
    }

    public String getNamaPemilik() {
        return namaPemilik;
    }

    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }

    public String getAlamatPemilik() {
        return alamatPemilik;
    }

    public void setAlamatPemilik(String alamatPemilik) {
        this.alamatPemilik = alamatPemilik;
    }

    public String getNpwpPemilik() {
        return npwpPemilik;
    }

    public void setNpwpPemilik(String npwpPemilik) {
        this.npwpPemilik = npwpPemilik;
    }

    public String getKppPemilik() {
        return kppPemilik;
    }

    public void setKppPemilik(String kppPemilik) {
        this.kppPemilik = kppPemilik;
    }

    public String getCabangNpwpPemilik() {
        return cabangNpwpPemilik;
    }

    public void setCabangNpwpPemilik(String cabangNpwpPemilik) {
        this.cabangNpwpPemilik = cabangNpwpPemilik;
    }

    public String getKlasifikasi() {
        return klasifikasi;
    }

    public void setKlasifikasi(String klasifikasi) {
        this.klasifikasi = klasifikasi;
    }

    public double getModal() {
        return modal;
    }

    public void setModal(double modal) {
        this.modal = modal;
    }

    public int getJumlahKaryawan() {
        return jumlahKaryawan;
    }

    public void setJumlahKaryawan(int jumlahKaryawan) {
        this.jumlahKaryawan = jumlahKaryawan;
    }

    public short getMasaBerlaku() {
        return masaBerlaku;
    }

    public void setMasaBerlaku(short masaBerlaku) {
        this.masaBerlaku = masaBerlaku;
    }
    
}
