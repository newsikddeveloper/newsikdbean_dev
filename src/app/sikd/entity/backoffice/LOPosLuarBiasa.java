/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;

/**
 *
 * @author sora
 */
public class LOPosLuarBiasa implements Serializable, Cloneable{
    private long index;
    private double pendapatan;
    private double beban;
    
    public LOPosLuarBiasa() {
    }

    public LOPosLuarBiasa(double pendapatan, double beban) {
        this.pendapatan = pendapatan;
        this.beban = beban;
    }

    public LOPosLuarBiasa(long index, double pendapatan, double beban) {
        this.index = index;
        this.pendapatan = pendapatan;
        this.beban = beban;
    }



    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public double getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(double pendapatan) {
        this.pendapatan = pendapatan;
    }

    public double getBeban() {
        return beban;
    }

    public void setBeban(double beban) {
        this.beban = beban;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getPendapatanAsString() {
        return SIKDUtil.doubleToString(pendapatan);
    }

    public void setPendapatanAsString(String pendapatanAsString) {
        this.pendapatan = SIKDUtil.stringToDouble(pendapatanAsString);
    }

    public String getBebanAsString() {
        return SIKDUtil.doubleToString(beban);
    }

    public void setBebanAsString(String bebanAsString) {
        this.beban = SIKDUtil.stringToDouble(bebanAsString);
    }
    
}
