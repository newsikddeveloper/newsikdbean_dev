/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class PotonganDTHSKPD implements Serializable{
    private long indexPotongan;
    private String kodeAkunPotongan;
    private String namaAkunPotongan;
    private short jenisPotongan;
    private double nilaiPotongan;

    public PotonganDTHSKPD() {
    }

    public PotonganDTHSKPD(String kodeAkunPotongan, String namaAkunPotongan, short jenisPotongan, double nilaiPotongan) {
        this.kodeAkunPotongan = kodeAkunPotongan;
        this.namaAkunPotongan = namaAkunPotongan;
        this.jenisPotongan = jenisPotongan;
        this.nilaiPotongan = nilaiPotongan;
    }
    public PotonganDTHSKPD(long indexPotongan, String kodeAkunPotongan, String namaAkunPotongan, short jenisPotongan, double nilaiPotongan) {
        this.indexPotongan = indexPotongan;
        this.kodeAkunPotongan = kodeAkunPotongan;
        this.namaAkunPotongan = namaAkunPotongan;
        this.jenisPotongan = jenisPotongan;
        this.nilaiPotongan = nilaiPotongan;
    }

    public long getIndexPotongan() {
        return indexPotongan;
    }

    public void setIndexPotongan(long indexPotongan) {
        this.indexPotongan = indexPotongan;
    }

    public String getKodeAkunPotongan() {
        return kodeAkunPotongan;
    }

    public void setKodeAkunPotongan(String kodeAkunPotongan) {
        this.kodeAkunPotongan = kodeAkunPotongan;
    }
    
    public short getJenisPotongan() {
        return jenisPotongan;
    }

    public void setJenisPotongan(short jenisPotongan) {
        this.jenisPotongan = jenisPotongan;
    }

    public double getNilaiPotongan() {
        return nilaiPotongan;
    }

    public void setNilaiPotongan(double nilaiPotongan) {
        this.nilaiPotongan = nilaiPotongan;
    }

    public String getNamaAkunPotongan() {
        return namaAkunPotongan;
    }

    public void setNamaAkunPotongan(String namaAkunPotongan) {
        this.namaAkunPotongan = namaAkunPotongan;
    }
}
