/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;

/**
 *
 * @author sora
 */
public class ArusKasInOut implements Serializable{
    private long index;
    private String kodeAkun;
    private String namaAkun;
    private double nilai;

    public ArusKasInOut() {
    }

    public ArusKasInOut(String kodeAkun, String namaAkun, double nilai) {
        this.kodeAkun = kodeAkun;
        this.namaAkun = namaAkun;
        this.nilai = nilai;
    }

    public ArusKasInOut(long index, String kodeAkun, String namaAkun, double nilai) {
        this(kodeAkun, namaAkun, nilai);
        this.index = index;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }
    
    public String getNilaiAsString() {
        return SIKDUtil.doubleToString(nilai);
    }

    public void setNilaiAsString(String nilaiAsString) {
        nilai = SIKDUtil.stringToDouble(nilaiAsString);
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) {
        this.namaAkun = namaAkun;
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) {
        this.kodeAkun = kodeAkun;
    }
    
}
