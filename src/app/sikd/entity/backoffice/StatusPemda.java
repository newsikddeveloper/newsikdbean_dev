package app.sikd.entity.backoffice;

import app.sikd.entity.Pemda;
import app.sikd.entity.backoffice.setting.StatusData;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sora
 */
public class StatusPemda implements Serializable{
    private Pemda pemda;
    private StatusData statusData;
    private String ikd;
    private String style;

    public StatusPemda() {
    }

    public StatusPemda(Pemda pemda, StatusData statusData, String ikd, String style) {
        this.pemda = pemda;
        this.statusData = statusData;
        this.ikd = ikd;
        this.style= style;
    }
    public StatusPemda(Pemda pemda, String ikd, String style) {
        this.pemda = pemda;
        this.ikd = ikd;
        this.style = style;
    }

    public String getIkd() {
        return ikd;
    }

    public void setIkd(String ikd) {
        this.ikd = ikd;
    }

    public Pemda getPemda() {
        return pemda;
    }

    public void setPemda(Pemda pemda) {
        this.pemda = pemda;
    }

    public StatusData getStatusData() {
        return statusData;
    }

    public void setStatusData(StatusData statusData) {
        this.statusData = statusData;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.pemda);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StatusPemda other = (StatusPemda) obj;
        if (!Objects.equals(this.pemda, other.pemda)) {
            return false;
        }
        return true;
    }
    
    
    

    @Override
    public String toString() {
        return pemda.toString();
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
    
}
