/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class NeracaAkunJenis extends ObjNeracaAkun implements Serializable, Comparable<NeracaAkunJenis>, Cloneable {
    List<NeracaAkunObjek> akunObjeks;

    public NeracaAkunJenis() {
    }
    
    public NeracaAkunJenis(String kodeAkun, String namaAkun, double nilai) {
        super(kodeAkun, namaAkun, nilai);
    }
    
    public NeracaAkunJenis(long index, String kodeAkun, String namaAkun, double nilai) {
        super(index, kodeAkun, namaAkun, nilai);
    }
    public NeracaAkunJenis(long index, String kodeAkun, String namaAkun, double nilai, short level) {
        super(index, kodeAkun, namaAkun, nilai, level);
    }

    public List<NeracaAkunObjek> getAkunObjeks() {
        return akunObjeks;
    }

    public void setAkunObjeks(List<NeracaAkunObjek> akunObjeks) {
        this.akunObjeks = akunObjeks;
    }

    @Override
    public String toString() {
        return getNamaAkun();
    }
    
    @Override
    public int compareTo(NeracaAkunJenis t) {
        return this.getKodeAkun().compareTo(t.getKodeAkun());
    }
    
    /**
     *
     * @return
     * @throws java.lang.CloneNotSupportedException
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}