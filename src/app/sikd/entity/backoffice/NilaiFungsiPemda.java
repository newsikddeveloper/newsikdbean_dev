/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class NilaiFungsiPemda implements Serializable{
    private String kodeFungsi;
    private String namaFungsi;
    private String kodePemda;
    private String namaPemda;
    private double nilai;

    public NilaiFungsiPemda() {
    }

    public NilaiFungsiPemda(String kodePemda, String namaPemda, double nilai) {
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.nilai = nilai;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }
    
    public String getKodeFungsi() {
        return kodeFungsi;
    }

    public void setKodeFungsi(String kodeFungsi) {
        this.kodeFungsi = kodeFungsi;
    }

    public String getNamaFungsi() {
        return namaFungsi;
    }

    public void setNamaFungsi(String namaFungsi) {
        this.namaFungsi = namaFungsi;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }
}
