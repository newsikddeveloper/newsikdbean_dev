/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.entity.backoffice.setting;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class Account implements Serializable{
    private long index;
    private String code;
    private String name;
    private short level;
    private List<Account> subAccount;
    private String style;
    private boolean group;
    private boolean reloadSub;
    

    public Account() {
    }

    public Account(String code, String name, short level, boolean group) {
        this.code = code;
        this.name = name;
        this.level = level;
        this.group = group;
    }

    public Account(String code, String name, short level, String style, boolean group) {
        this.code = code;
        this.name = name;
        this.level = level;
        this.style = style;
        this.group = group;
    }

    public Account(long index, String code, String name, short level, boolean group) {
        this.index = index;
        this.code = code;
        this.name = name;
        this.level = level;
        this.group = group;
    }

    public Account(long index, String code, String name, short level, String style, boolean group) {
        this.index = index;
        this.code = code;
        this.name = name;
        this.level = level;
        this.style = style;
        this.group = group;
    }

    public boolean isGroup() {
        return group;
    }

    public void setGroup(boolean group) {
        this.group = group;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public List<Account> getSubAccount() {
        return subAccount;
    }

    public void setSubAccount(List<Account> subAccount) {
        this.subAccount = subAccount;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public String toString() {
        return code + " " + name;
    }

    public boolean isReloadSub() {
        return reloadSub;
    }

    public void setReloadSub(boolean reloadSub) {
        this.reloadSub = reloadSub;
    }
    
    
}
