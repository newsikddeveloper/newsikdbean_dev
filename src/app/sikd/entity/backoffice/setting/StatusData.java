/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.entity.backoffice.setting;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class StatusData implements Serializable{
    private short kode;
    private String nama;

    public StatusData() {
    }

    public StatusData(short kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getNama() {
        if(kode==-1) nama="Belum Mengirimkan Data";
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public short getKode() {
        return kode;
    }

    public void setKode(short kode) {
        this.kode = kode;
    }
    
    public String toString(){
        return nama;
    }
}
