/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class NeracaAkunKelompok extends ObjNeracaAkun implements Serializable, Comparable<NeracaAkunKelompok>, Cloneable {
    List<NeracaAkunJenis> akunJeniss;

    public NeracaAkunKelompok() {
    }
    
    public NeracaAkunKelompok(String kodeAkun, String namaAkun, double nilai) {
        super(kodeAkun, namaAkun, nilai);
    }
    
    public NeracaAkunKelompok(long index, String kodeAkun, String namaAkun, double nilai) {
        super(index, kodeAkun, namaAkun, nilai);
    }
    
    public NeracaAkunKelompok(long index, String kodeAkun, String namaAkun, double nilai, short level) {
        super(index, kodeAkun, namaAkun, nilai, level);
    }

    public List<NeracaAkunJenis> getAkunJeniss() {
        return akunJeniss;
    }

    public void setAkunJeniss(List<NeracaAkunJenis> akunJeniss) {
        this.akunJeniss = akunJeniss;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return getNamaAkun();
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final NeracaAkunJenis other = (NeracaAkunJenis) obj;
        if(!super.getKodeAkun().equals(other.getKodeAkun())) return false;
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(NeracaAkunKelompok t) {
        return this.getKodeAkun().compareTo(t.getKodeAkun());
    }
    
    /**
     *
     * @return
     * @throws java.lang.CloneNotSupportedException
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}