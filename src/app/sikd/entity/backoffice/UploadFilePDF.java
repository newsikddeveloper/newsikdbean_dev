/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author sora
 */
public class UploadFilePDF  implements Serializable{
    
    private long id;
    private String kdsatker;
    private String filename;
    private Date tglupload;

    public UploadFilePDF() {
    }

    public UploadFilePDF(String kdsatker, String filename, Date tglupload) {
        this.kdsatker = kdsatker;
        this.filename = filename;
        this.tglupload = tglupload;
    }

    public UploadFilePDF(long id, String kdsatker, String filename, Date tglupload) {
        this.id = id;
        this.kdsatker = kdsatker;
        this.filename = filename;
        this.tglupload = tglupload;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKdsatker() {
        return kdsatker;
    }

    public void setKdsatker(String kdsatker) {
        this.kdsatker = kdsatker;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Date getTglupload() {
        return tglupload;
    }

    public void setTglupload(Date tglupload) {
        this.tglupload = tglupload;
    }
    
    
    
    
    
    
}
