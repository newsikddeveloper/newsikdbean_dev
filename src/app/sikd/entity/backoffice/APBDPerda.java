/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author sora
 */
public class APBDPerda implements Serializable{
    private String nomor;
    private Date date;

    public APBDPerda() {
    }

    public APBDPerda(String nomor, Date date) {
        this.nomor = nomor;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }
    
    
    
}
