package app.sikd.entity.backoffice;


import java.io.Serializable;

/**
 *
 * @author detra
 */

public class RincianHiburan implements Serializable{    
    private String namaHiburan;
    private String alamat;
    private String namaPemilik;
    private String alamatPemilik;
    private String npwpPemilik;
    private String kppPemilik;
    private String cabangNpwpPemilik;
    private String namaPengelola;
    private String alamatPengelola;
    private String npwpPengelola;
    private String kppPengelola;
    private String cabangNpwpPengelola;
    private double jumlahPajak;

    public RincianHiburan() {
    }

    public RincianHiburan(String namaHiburan, String alamat, String namaPemilik, String alamatPemilik, String npwpPemilik, String kppPemilik, String cabangNpwpPemilik, String namaPengelola, String alamatPengelola, String npwpPengelola, String kppPengelola, String cabangNpwpPengelola, double jumlahPajak) {
        this.namaHiburan = namaHiburan;
        this.alamat = alamat;
        this.namaPemilik = namaPemilik;
        this.alamatPemilik = alamatPemilik;
        this.npwpPemilik = npwpPemilik;
        this.kppPemilik = kppPemilik;
        this.cabangNpwpPemilik = cabangNpwpPemilik;
        this.namaPengelola = namaPengelola;
        this.alamatPengelola = alamatPengelola;
        this.npwpPengelola = npwpPengelola;
        this.kppPengelola = kppPengelola;
        this.cabangNpwpPengelola = cabangNpwpPengelola;
        this.jumlahPajak = jumlahPajak;
    }
    
    
    public String getNamaHiburan() {
        return namaHiburan;
    }

    public void setNamaHiburan(String namaHiburan) {
        this.namaHiburan = namaHiburan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNamaPemilik() {
        return namaPemilik;
    }

    public void setNamaPemilik(String namaPemilik) {
        this.namaPemilik = namaPemilik;
    }

    public String getAlamatPemilik() {
        return alamatPemilik;
    }

    public void setAlamatPemilik(String alamatPemilik) {
        this.alamatPemilik = alamatPemilik;
    }

    public String getNpwpPemilik() {
        return npwpPemilik;
    }

    public void setNpwpPemilik(String npwpPemilik) {
        this.npwpPemilik = npwpPemilik;
    }

    public String getKppPemilik() {
        return kppPemilik;
    }

    public void setKppPemilik(String kppPemilik) {
        this.kppPemilik = kppPemilik;
    }

    public String getCabangNpwpPemilik() {
        return cabangNpwpPemilik;
    }

    public void setCabangNpwpPemilik(String cabangNpwpPemilik) {
        this.cabangNpwpPemilik = cabangNpwpPemilik;
    }

    public String getNamaPengelola() {
        return namaPengelola;
    }

    public void setNamaPengelola(String namaPengelola) {
        this.namaPengelola = namaPengelola;
    }

    public String getAlamatPengelola() {
        return alamatPengelola;
    }

    public void setAlamatPengelola(String alamatPengelola) {
        this.alamatPengelola = alamatPengelola;
    }

    public String getNpwpPengelola() {
        return npwpPengelola;
    }

    public void setNpwpPengelola(String npwpPengelola) {
        this.npwpPengelola = npwpPengelola;
    }

    public String getKppPengelola() {
        return kppPengelola;
    }

    public void setKppPengelola(String kppPengelola) {
        this.kppPengelola = kppPengelola;
    }

    public String getCabangNpwpPengelola() {
        return cabangNpwpPengelola;
    }

    public void setCabangNpwpPengelola(String cabangNpwpPengelola) {
        this.cabangNpwpPengelola = cabangNpwpPengelola;
    }

    public double getJumlahPajak() {
        return jumlahPajak;
    }

    public void setJumlahPajak(double jumlahPajak) {
        this.jumlahPajak = jumlahPajak;
    }
    
}
