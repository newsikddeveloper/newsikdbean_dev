/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author USER
 */
public class DataDaerah implements Serializable{
    private String kodeSatker;
    private String kodePemda;
    private short tahunAnggaran;
    private String namaPemda;
    private DataDaerahDetail detail;

    public DataDaerah() {
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) {
        this.kodeSatker = kodeSatker;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public DataDaerahDetail getDetail() {
        return detail;
    }

    public void setDetail(DataDaerahDetail detail) {
        this.detail = detail;
    }
    
    
    
    
}
