package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author detra
 */

public class AkunDTHSKPD implements Serializable{
    private long indexAkun;
    private String kodeAkunUtama;
    private String namaAkunUtama;
    private String kodeAkunKelompok;
    private String namaAkunKelompok;
    private String kodeAkunJenis;
    private String namaAkunJenis;
    private String kodeAkunObjek;
    private String namaAkunObjek;
    private String kodeAkunRincian;
    private String namaAkunRincian;
    private double nilai;
    private String kodeAkunSubRincian;
    private String namaAkunSubRincian;

    public AkunDTHSKPD() {
    }
    
    public AkunDTHSKPD(String kodeAkunUtama, double nilai) { 
        this.kodeAkunUtama = kodeAkunUtama;
        this.nilai = nilai;
        
    }

    public AkunDTHSKPD(String kodeAkunUtama, String namaAkunUtama, String kodeAkunKelompok, String namaAkunKelompok, String kodeAkunJenis, String namaAkunJenis, String kodeAkunObjek, String namaAkunObjek, String kodeAkunRincian, String namaAkunRincian, double nilai) {
        this.kodeAkunUtama = kodeAkunUtama;
        this.namaAkunUtama = namaAkunUtama;
        this.kodeAkunKelompok = kodeAkunKelompok;
        this.namaAkunKelompok = namaAkunKelompok;
        this.kodeAkunJenis = kodeAkunJenis;
        this.namaAkunJenis = namaAkunJenis;
        this.kodeAkunObjek = kodeAkunObjek;
        this.namaAkunObjek = namaAkunObjek;
        this.kodeAkunRincian = kodeAkunRincian;
        this.namaAkunRincian = namaAkunRincian;
        this.nilai = nilai;
    }

    public AkunDTHSKPD(long indexAkun, String kodeAkunUtama, String namaAkunUtama, String kodeAkunKelompok, String namaAkunKelompok, String kodeAkunJenis, String namaAkunJenis, String kodeAkunObjek, String namaAkunObjek, String kodeAkunRincian, String namaAkunRincian, double nilai) {
        this(kodeAkunUtama, namaAkunUtama, kodeAkunKelompok, namaAkunKelompok, kodeAkunJenis, namaAkunJenis, kodeAkunObjek, namaAkunObjek, kodeAkunRincian, namaAkunRincian, nilai);
        this.indexAkun = indexAkun;
    }
    
    public AkunDTHSKPD(String kodeAkunUtama, String namaAkunUtama, String kodeAkunKelompok, String namaAkunKelompok, String kodeAkunJenis, String namaAkunJenis, String kodeAkunObjek, String namaAkunObjek, String kodeAkunRincian, String namaAkunRincian, String kodeAkunSubRincian, String namaAkunSubRincian, double nilai) {
        this(kodeAkunUtama, namaAkunUtama, kodeAkunKelompok, namaAkunKelompok, kodeAkunJenis, namaAkunJenis, kodeAkunObjek, namaAkunObjek, kodeAkunRincian, namaAkunRincian, nilai);
        this.kodeAkunSubRincian = kodeAkunSubRincian;
        this.namaAkunSubRincian = namaAkunSubRincian;
    }
    
    public AkunDTHSKPD(long indexAkun, String kodeAkunUtama, String namaAkunUtama, String kodeAkunKelompok, String namaAkunKelompok, String kodeAkunJenis, String namaAkunJenis, String kodeAkunObjek, String namaAkunObjek, String kodeAkunRincian, String namaAkunRincian, String kodeAkunSubRincian, String namaAkunSubRincian, double nilai) {
        this(kodeAkunUtama, namaAkunUtama, kodeAkunKelompok, namaAkunKelompok, kodeAkunJenis, namaAkunJenis, kodeAkunObjek, namaAkunObjek, kodeAkunRincian, namaAkunRincian, kodeAkunSubRincian, namaAkunSubRincian, nilai);
        this.indexAkun = indexAkun;
    }

    public long getIndexAkun() {
        return indexAkun;
    }

    public void setIndexAkun(long indexAkun) {
        this.indexAkun = indexAkun;
    }
    
    public String getKodeAkunUtama() {
        return kodeAkunUtama;
    }

    public void setKodeAkunUtama(String kodeAkunUtama){        
        this.kodeAkunUtama = kodeAkunUtama;
    }

    public String getNamaAkunUtama() {
        return namaAkunUtama;
    }

    public void setNamaAkunUtama(String namaAkunUtama){
        this.namaAkunUtama = namaAkunUtama;
    }

    public String getKodeAkunKelompok() {
        return kodeAkunKelompok;
    }

    public void setKodeAkunKelompok(String kodeAkunKelompok) {
        this.kodeAkunKelompok = kodeAkunKelompok;
    }

    public String getNamaAkunKelompok() {
        return namaAkunKelompok;
    }

    public void setNamaAkunKelompok(String namaAkunKelompok){
        this.namaAkunKelompok = namaAkunKelompok;
    }

    public String getKodeAkunJenis() {
        return kodeAkunJenis;
    }

    public void setKodeAkunJenis(String kodeAkunJenis){
        this.kodeAkunJenis = kodeAkunJenis;
    }

    public String getNamaAkunJenis() {
        return namaAkunJenis;
    }

    public void setNamaAkunJenis(String namaAkunJenis) {
        this.namaAkunJenis = namaAkunJenis;
    }

    public String getKodeAkunObjek() {
        return kodeAkunObjek;
    }

    public void setKodeAkunObjek(String kodeAkunObjek) {
        this.kodeAkunObjek = kodeAkunObjek;
    }

    public String getNamaAkunObjek() {
        return namaAkunObjek;
    }

    public void setNamaAkunObjek(String namaAkunObjek){
        this.namaAkunObjek = namaAkunObjek;
    }

    public String getKodeAkunRincian() {
        return kodeAkunRincian;
    }

    public void setKodeAkunRincian(String kodeAkunRincian) {
        this.kodeAkunRincian = kodeAkunRincian;
    }

    public String getNamaAkunRincian() {
        return namaAkunRincian;
    }

    public void setNamaAkunRincian(String namaAkunRincian){
        this.namaAkunRincian = namaAkunRincian;
    }
    
    

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public String getKodeAkunSubRincian() {
        return kodeAkunSubRincian;
    }

    public void setKodeAkunSubRincian(String kodeAkunSubRincian) {
        this.kodeAkunSubRincian = kodeAkunSubRincian;
    }

    public String getNamaAkunSubRincian() {
        return namaAkunSubRincian;
    }

    public void setNamaAkunSubRincian(String namaAkunSubRincian) {
        this.namaAkunSubRincian = namaAkunSubRincian;
    }
}
