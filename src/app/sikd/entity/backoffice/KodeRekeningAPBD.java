package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author detra
 */

public class KodeRekeningAPBD implements Serializable{
    private String kodeAkunUtama;
    private String namaAkunUtama;
    private String kodeAkunKelompok;
    private String namaAkunKelompok;
    private String kodeAkunJenis;
    private String namaAkunJenis;
    private String kodeAkunObjek;
    private String namaAkunObjek;
    private String kodeAkunRincian;
    private String namaAkunRincian;
    private String kodeAkunSub;
    private String namaAkunSub;
    private double nilaiAnggaran;
    private long indexKegiatan;
    

    public KodeRekeningAPBD() {
    }

    public KodeRekeningAPBD(String kodeAkunUtama, String namaAkunUtama, String kodeAkunKelompok, String namaAkunKelompok, String kodeAkunJenis, String namaAkunJenis, String kodeAkunObjek, String namaAkunObjek, String kodeAkunRincian, String namaAkunRincian, String kodeAkunSub, String namaAkunSub, double nilaiAnggaran) {
        this.kodeAkunUtama = kodeAkunUtama;
        this.namaAkunUtama = namaAkunUtama;
        this.kodeAkunKelompok = kodeAkunKelompok;
        this.namaAkunKelompok = namaAkunKelompok;
        this.kodeAkunJenis = kodeAkunJenis;
        this.namaAkunJenis = namaAkunJenis;
        this.kodeAkunObjek = kodeAkunObjek;
        this.namaAkunObjek = namaAkunObjek;
        this.kodeAkunRincian = kodeAkunRincian;
        this.namaAkunRincian = namaAkunRincian;
        this.kodeAkunSub = kodeAkunSub;
        this.namaAkunSub = namaAkunSub;
        this.nilaiAnggaran = nilaiAnggaran;
    }
    
    public KodeRekeningAPBD(String kodeAkunUtama, String namaAkunUtama, String kodeAkunKelompok, String namaAkunKelompok, String kodeAkunJenis, String namaAkunJenis, String kodeAkunObjek, String namaAkunObjek, String kodeAkunRincian, String namaAkunRincian, String kodeAkunSub, String namaAkunSub, double nilaiAnggaran, long indexKegiatan) {
        this.kodeAkunUtama = kodeAkunUtama;
        this.namaAkunUtama = namaAkunUtama;
        this.kodeAkunKelompok = kodeAkunKelompok;
        this.namaAkunKelompok = namaAkunKelompok;
        this.kodeAkunJenis = kodeAkunJenis;
        this.namaAkunJenis = namaAkunJenis;
        this.kodeAkunObjek = kodeAkunObjek;
        this.namaAkunObjek = namaAkunObjek;
        this.kodeAkunRincian = kodeAkunRincian;
        this.namaAkunRincian = namaAkunRincian;
        this.kodeAkunSub = kodeAkunSub;
        this.namaAkunSub = namaAkunSub;
        this.nilaiAnggaran = nilaiAnggaran;
        this.indexKegiatan = indexKegiatan;
    }
    
    public KodeRekeningAPBD(KodeRekeningAPBD acc) {
        this.kodeAkunUtama = acc.getKodeAkunUtama();
        this.namaAkunUtama = acc.getNamaAkunUtama();
        this.kodeAkunKelompok = acc.getKodeAkunKelompok();
        this.namaAkunKelompok = acc.getNamaAkunKelompok();
        this.kodeAkunJenis = acc.getKodeAkunJenis();
        this.namaAkunJenis = acc.getNamaAkunJenis();
        this.kodeAkunObjek = acc.getKodeAkunObjek();
        this.namaAkunObjek = acc.getNamaAkunObjek();
        this.kodeAkunRincian = acc.getKodeAkunRincian();
        this.namaAkunRincian = acc.getNamaAkunRincian();
        this.kodeAkunSub = acc.getKodeAkunSub();
        this.namaAkunSub = acc.getNamaAkunSub();
        this.nilaiAnggaran = acc.getNilaiAnggaran();
        this.indexKegiatan = acc.getIndexKegiatan();
    }

    public String getKodeAkunUtama() {
        return kodeAkunUtama;
    }

    public void setKodeAkunUtama(String kodeAkunUtama) {
        this.kodeAkunUtama = kodeAkunUtama;
    }

    public String getNamaAkunUtama() {
        return namaAkunUtama;
    }

    public void setNamaAkunUtama(String namaAkunUtama) {
        this.namaAkunUtama = namaAkunUtama;
    }

    public String getKodeAkunKelompok() {
        return kodeAkunKelompok;
    }

    public void setKodeAkunKelompok(String kodeAkunKelompok) {
        this.kodeAkunKelompok = kodeAkunKelompok;
    }

    public String getNamaAkunKelompok() {
        return namaAkunKelompok;
    }

    public void setNamaAkunKelompok(String namaAkunKelompok) {
        this.namaAkunKelompok = namaAkunKelompok;
    }

    public String getKodeAkunJenis() {
        return kodeAkunJenis;
    }

    public void setKodeAkunJenis(String kodeAkunJenis) {
        this.kodeAkunJenis = kodeAkunJenis;
    }

    public String getNamaAkunJenis() {
        return namaAkunJenis;
    }

    public void setNamaAkunJenis(String namaAkunJenis) {
        this.namaAkunJenis = namaAkunJenis;
    }

    public String getKodeAkunObjek() {
        return kodeAkunObjek;
    }

    public void setKodeAkunObjek(String kodeAkunObjek) {
        this.kodeAkunObjek = kodeAkunObjek;
    }

    public String getNamaAkunObjek() {
        return namaAkunObjek;
    }

    public void setNamaAkunObjek(String namaAkunObjek) {
        this.namaAkunObjek = namaAkunObjek;
    }

    public String getKodeAkunRincian() {
        return kodeAkunRincian;
    }

    public void setKodeAkunRincian(String kodeAkunRincian) {
        this.kodeAkunRincian = kodeAkunRincian;
    }

    public String getNamaAkunRincian() {
        return namaAkunRincian;
    }

    public void setNamaAkunRincian(String namaAkunRincian) {
        this.namaAkunRincian = namaAkunRincian;
    }

    public String getKodeAkunSub() {
        return kodeAkunSub;
    }

    public void setKodeAkunSub(String kodeAkunSub) {
        this.kodeAkunSub = kodeAkunSub;
    }

    public String getNamaAkunSub() {
        return namaAkunSub;
    }

    public void setNamaAkunSub(String namaAkunSub) {
        this.namaAkunSub = namaAkunSub;
    }

    public double getNilaiAnggaran() {
        return nilaiAnggaran;
    }

    public void setNilaiAnggaran(double nilaiAnggaran) {
        this.nilaiAnggaran = nilaiAnggaran;
    }
    
    public String getNilaiAnggaranAsString() {
        return SIKDUtil.doubleToString(nilaiAnggaran);
    }

    public void setNilaiAnggaranAsString(String nilaiAnggaranAsString) {
        if(nilaiAnggaranAsString==null) nilaiAnggaranAsString="0";
        this.nilaiAnggaran = SIKDUtil.stringToDouble(nilaiAnggaranAsString);
    }

    public long getIndexKegiatan() {
        return indexKegiatan;
    }

    public void setIndexKegiatan(long indexKegiatan) {
        this.indexKegiatan = indexKegiatan;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.kodeAkunUtama);
        hash = 47 * hash + Objects.hashCode(this.kodeAkunKelompok);
        hash = 47 * hash + Objects.hashCode(this.kodeAkunJenis);
        hash = 47 * hash + Objects.hashCode(this.kodeAkunObjek);
        hash = 47 * hash + Objects.hashCode(this.kodeAkunRincian);
        hash = 47 * hash + Objects.hashCode(this.kodeAkunSub);
        hash = 47 * hash + (int) (this.indexKegiatan ^ (this.indexKegiatan >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KodeRekeningAPBD other = (KodeRekeningAPBD) obj;
        if (!Objects.equals(this.kodeAkunUtama, other.kodeAkunUtama)) {
            return false;
        }
        if (!Objects.equals(this.kodeAkunKelompok, other.kodeAkunKelompok)) {
            return false;
        }
        if (!Objects.equals(this.kodeAkunJenis, other.kodeAkunJenis)) {
            return false;
        }
        if (!Objects.equals(this.kodeAkunObjek, other.kodeAkunObjek)) {
            return false;
        }
        if (!Objects.equals(this.kodeAkunRincian, other.kodeAkunRincian)) {
            return false;
        }
        if (!Objects.equals(this.kodeAkunSub, other.kodeAkunSub)) {
            return false;
        }
        if (this.indexKegiatan != other.indexKegiatan) {
            return false;
        }
        return true;
    }
    
    
}
