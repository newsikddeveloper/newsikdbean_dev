/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */

public class LOAkunUtama extends ObjNeracaAkun implements Serializable, Comparable<LOAkunUtama>, Cloneable{
  List<LOAkunKelompok> akunKelompoks;

    public LOAkunUtama() {
    }

    public LOAkunUtama(String kodeAkun, String namaAkun, double nilai) {
        super(kodeAkun, namaAkun, nilai);
    }

    public LOAkunUtama(long index, String kodeAkun, String namaAkun, double nilai) {
        super(index, kodeAkun, namaAkun, nilai);
    }

    public List<LOAkunKelompok> getAkunKelompoks() {
        return akunKelompoks;
    }

    public void setAkunKelompoks(List<LOAkunKelompok> akunKelompoks) {
        this.akunKelompoks = akunKelompoks;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return getNamaAkun();
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final LOAkunUtama other = (LOAkunUtama) obj;
        if(!super.getKodeAkun().equals(other.getKodeAkun())) return false;
        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    /**
     *
     * @param t
     * @return
     */
    @Override
    public int compareTo(LOAkunUtama t) {
        return this.getKodeAkun().compareTo(t.getKodeAkun());
    }
    
    /**
     *
     * @return
     * @throws java.lang.CloneNotSupportedException
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

