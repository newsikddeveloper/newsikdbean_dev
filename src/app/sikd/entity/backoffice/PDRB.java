/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author USER
 */
public class PDRB implements Serializable{
    private long index;
    private String kodeSatker;
    private String kodePemda;
    private short tahunAnggaran;
    private String namaPemda;
    private PDRBHarga hargaBerlaku;
    private PDRBHarga hargaKonstan;

    public PDRB() {
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) {
        this.kodeSatker = kodeSatker;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public PDRBHarga getHargaBerlaku() {
        return hargaBerlaku;
    }

    public void setHargaBerlaku(PDRBHarga hargaBerlaku) {
        this.hargaBerlaku = hargaBerlaku;
    }

    public PDRBHarga getHargaKonstan() {
        return hargaKonstan;
    }

    public void setHargaKonstan(PDRBHarga hargaKonstan) {
        this.hargaKonstan = hargaKonstan;
    }

    
    
}
