/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;

/**
 *
 * @author sora
 */

public class ArusKasSaldo implements Serializable{
    private long index;
    private double kasBUDAwal;
    private double kasBUDAkhir;
    private double kasBendaharaPengeluaranAwal;
    private double kasBendaharaPenerimaanAwal;
    private double kasLainnya;

    public ArusKasSaldo() {
    }

    public ArusKasSaldo(double kasBUDAwal, double kasBUDAkhir, double kasBendaharaPengeluaranAwal, double kasBendaharaPenerimaanAwal, double kasLainnya) {
        this.kasBUDAwal = kasBUDAwal;
        this.kasBUDAkhir = kasBUDAkhir;
        this.kasBendaharaPengeluaranAwal = kasBendaharaPengeluaranAwal;
        this.kasBendaharaPenerimaanAwal = kasBendaharaPenerimaanAwal;
        this.kasLainnya = kasLainnya;
    }

    public ArusKasSaldo(long index, double kasBUDAwal, double kasBUDAkhir, double kasBendaharaPengeluaranAwal, double kasBendaharaPenerimaanAwal, double kasLainnya) {
        this.index = index;
        this.kasBUDAwal = kasBUDAwal;
        this.kasBUDAkhir = kasBUDAkhir;
        this.kasBendaharaPengeluaranAwal = kasBendaharaPengeluaranAwal;
        this.kasBendaharaPenerimaanAwal = kasBendaharaPenerimaanAwal;
        this.kasLainnya = kasLainnya;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public double getKasLainnya() {
        return kasLainnya;
    }

    public void setKasLainnya(double kasLainnya) {
        this.kasLainnya = kasLainnya;
    }

    public double getKasBUDAwal() {
        return kasBUDAwal;
    }

    public void setKasBUDAwal(double kasBUDAwal) {
        this.kasBUDAwal = kasBUDAwal;
    }

    public double getKasBUDAkhir() {
        return kasBUDAkhir;
    }

    public void setKasBUDAkhir(double kasBUDAkhir) {
        this.kasBUDAkhir = kasBUDAkhir;
    }

    public double getKasBendaharaPengeluaranAwal() {
        return kasBendaharaPengeluaranAwal;
    }

    public void setKasBendaharaPengeluaranAwal(double kasBendaharaPengeluaranAwal) {
        this.kasBendaharaPengeluaranAwal = kasBendaharaPengeluaranAwal;
    }

    public double getKasBendaharaPenerimaanAwal() {
        return kasBendaharaPenerimaanAwal;
    }

    public void setKasBendaharaPenerimaanAwal(double kasBendaharaPenerimaanAwal) {
        this.kasBendaharaPenerimaanAwal = kasBendaharaPenerimaanAwal;
    }
    
    public String getKasLainnyaAsString() {
        return SIKDUtil.doubleToString(kasLainnya);
    }

    public void setKasLainnyaAsString(String kasLainnyaAsString) {        
        kasLainnya = SIKDUtil.stringToDouble(kasLainnyaAsString);
    }

    public String getKasBUDAwalAsString() {
        return SIKDUtil.doubleToString(kasBUDAwal);
    }

    public void setKasBUDAwalAsString(String kasBUDAwalAsString) {        
        this.kasBUDAwal = SIKDUtil.stringToDouble(kasBUDAwalAsString);
    }

    public String getKasBUDAkhirAsString() {
        return SIKDUtil.doubleToString(kasBUDAkhir);
    }

    public void setKasBUDAkhirAsString(String kasBUDAkhirAsString) {
        this.kasBUDAkhir = SIKDUtil.stringToDouble(kasBUDAkhirAsString);
    }

    public String getKasBendaharaPengeluaranAwalAsString() {
        return SIKDUtil.doubleToString(kasBendaharaPengeluaranAwal);
    }

    public void setKasBendaharaPengeluaranAwalAsString(String kasBendaharaPengeluaranAwalAsString) {
        this.kasBendaharaPengeluaranAwal = SIKDUtil.stringToDouble(kasBendaharaPengeluaranAwalAsString);
    }

    public String getKasBendaharaPenerimaanAwalAsString() {
        return SIKDUtil.doubleToString(kasBendaharaPenerimaanAwal);
    }

    public void setKasBendaharaPenerimaanAwalAsString(String kasBendaharaPenerimaanAwalAsString) {
        this.kasBendaharaPenerimaanAwal  = SIKDUtil.stringToDouble(kasBendaharaPenerimaanAwalAsString);
    }
    
}
