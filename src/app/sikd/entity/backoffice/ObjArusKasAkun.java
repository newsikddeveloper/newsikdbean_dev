/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sora
 */
public class ObjArusKasAkun implements Serializable {
    private long index;
    private String kodeAkun;
    private String namaAkun;
    private double nilai;
    private String ui;
    private String style;
    
    public static final String KAS_OPERASI="*o*";
    public static final String KAS_OPERASI_IN="*oi*";
    public static final String KAS_OPERASI_OUT="*oo*";
    public static final String KAS_IVESTASI="*i*";
    public static final String KAS_INVESTASI_IN="*ii*";
    public static final String KAS_INVESTASI_OUT="*io*";
    public static final String KAS_NONANGGARAN="*n*";
    public static final String KAS_NONANGGARAN_IN="*ni*";
    public static final String KAS_NONANGGARAN_OUT="*no*";
    public static final String KAS_PEMBIAYAAN="*p*";
    public static final String KAS_PEMBIAYAAN_IN="*pi*";
    public static final String KAS_PEMBIAYAAN_OUT="*po*";
    

    public ObjArusKasAkun() {
    }
    
    public ObjArusKasAkun(String kodeAkun, String namaAkun, double nilai) {
        this.kodeAkun = kodeAkun;
        this.namaAkun = namaAkun;
        this.nilai = nilai;
    }
    
    public ObjArusKasAkun(long index, String kodeAkun, String namaAkun, double nilai) {
        this(kodeAkun, namaAkun, nilai);
        this.index = index;
    }
    
    public ObjArusKasAkun(long index, String kodeAkun, String namaAkun, double nilai, String ui) {
        this(kodeAkun, namaAkun, nilai);
        this.index = index;
        this.ui = ui;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) {
        this.namaAkun = namaAkun;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }
    
    public String getNilaiAsString() {
        return SIKDUtil.doubleToString(nilai);
    }

    public void setNilaiAsString(String nilaiAsString) {
            this.nilai = SIKDUtil.stringToDouble(nilaiAsString);
    }
    
    public String getNilaiKosongAsString() {
        if(nilai==0) return "";
        else return SIKDUtil.doubleToString(nilai);
    }

    public void setNilaiKosongAsString(String nilaiAsString) {
            this.nilai = SIKDUtil.stringToDouble(nilaiAsString);
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) {
        this.kodeAkun = kodeAkun;
    }

    @Override
    public String toString() {
        return namaAkun;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (int) (this.index ^ (this.index >>> 32));
        hash = 29 * hash + Objects.hashCode(this.kodeAkun);
        hash = 29 * hash + Objects.hashCode(this.namaAkun);
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.nilai) ^ (Double.doubleToLongBits(this.nilai) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final NeracaAkunJenis other = (NeracaAkunJenis) obj;
        if(!this.kodeAkun.equals(other.getKodeAkun())) return false;
        return true;
    }
    
    /**
     *
     * @return
     * @throws java.lang.CloneNotSupportedException
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getUi() {
        return ui;
    }

    public void setUi(String ui) {
        this.ui = ui;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
    
    

    
    
}