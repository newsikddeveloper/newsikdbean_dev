package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */

public class Neraca  implements Serializable{
    private long index;
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private short semester;
    private short statusData;
    private String judulNeraca;
    private List<NeracaAkunUtama> akunUtamas;

    public Neraca() {
    }

    public Neraca(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short semester, String judulNeraca, short statusData) {
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.semester = semester;
        this.judulNeraca = judulNeraca;
        this.statusData = statusData;
    }

    public Neraca(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short semester, String judulNeraca, short statusData) {
        this.index = index;
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.semester = semester;
        this.judulNeraca = judulNeraca;
        this.statusData = statusData;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }
    
    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) {
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public short getSemester() {
        return semester;
    }

    public void setSemester(short semester) {
        this.semester = semester;
    }

    public String getJudulNeraca() {
        return judulNeraca;
    }

    public void setJudulNeraca(String judulNeraca) {
        this.judulNeraca = judulNeraca;
    }

    public List<NeracaAkunUtama> getAkunUtamas() {
        return akunUtamas;
    }

    public void setAkunUtamas(List<NeracaAkunUtama> akunUtamas) {
        this.akunUtamas = akunUtamas;
    }
    
    
    
    @Override
    public String toString(){
        return judulNeraca;
    }

    public short getStatusData() {
        return statusData;
    }

    public void setStatusData(short statusData) {
        this.statusData = statusData;
    }
}