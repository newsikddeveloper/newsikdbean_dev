package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author detra
 */

public class DTHSKPD implements Serializable{
    private long index;
    private String kodeUrusanProgram;
    private String namaUrusanProgram;
    private String kodeUrusanPelaksana;
    private String namaUrusanPelaksana;
    private String kodeSKPD;
    private String namaSKPD;
    private List<RincianDTHSKPD> rincians;
    
    public DTHSKPD() {
    }
    
    public String getKodeUrusanProgram() {
        return kodeUrusanProgram;
    }

    public void setKodeUrusanProgram(String kodeUrusanProgram){
        this.kodeUrusanProgram = kodeUrusanProgram;
    }

    public String getNamaUrusanProgram() {
        return namaUrusanProgram;
    }

    public void setNamaUrusanProgram(String namaUrusanProgram){
        this.namaUrusanProgram = namaUrusanProgram;
    }

    public String getKodeUrusanPelaksana() {
        return kodeUrusanPelaksana;
    }

    public void setKodeUrusanPelaksana(String kodeUrusanPelaksana){
        this.kodeUrusanPelaksana = kodeUrusanPelaksana;
    }

    public String getNamaUrusanPelaksana() {
        return namaUrusanPelaksana;
    }

    public void setNamaUrusanPelaksana(String namaUrusanPelaksana) {
        this.namaUrusanPelaksana = namaUrusanPelaksana;
    }

    public String getKodeSKPD() {
        return kodeSKPD;
    }

    public void setKodeSKPD(String kodeSKPD) {
        this.kodeSKPD = kodeSKPD;
    }

    public String getNamaSKPD() {
        return namaSKPD;
    }

    public void setNamaSKPD(String namaSKPD) {
        this.namaSKPD = namaSKPD;
    }

    public List<RincianDTHSKPD> getRincians() {
        return rincians;
    }

    public void setRincians(List<RincianDTHSKPD> rincians) {
        this.rincians = rincians;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }
}
