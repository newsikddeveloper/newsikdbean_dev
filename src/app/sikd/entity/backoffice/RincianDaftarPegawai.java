package app.sikd.entity.backoffice;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;

/**
 *
 * @author detra
 */
public class RincianDaftarPegawai implements Serializable{    
    private short kodeGolongan;
    private short kodeGrade;
    private short kodeEselon;
    private int jumlah;

    public RincianDaftarPegawai() {
    }

    public RincianDaftarPegawai(short kodeGolongan, short kodeGrade, short kodeEselon, int jumlah) {
        this.kodeGolongan = kodeGolongan;
        this.kodeGrade = kodeGrade;
        this.kodeEselon = kodeEselon;
        this.jumlah = jumlah;
    }

    public short getKodeGolongan() {
        return kodeGolongan;
    }

    public void setKodeGolongan(short kodeGolongan) throws SIKDServiceException{
        this.kodeGolongan = kodeGolongan;
    }

    public short getKodeGrade() {
        return kodeGrade;
    }

    public void setKodeGrade(short kodeGrade) throws SIKDServiceException{
        this.kodeGrade = kodeGrade;
    }

    public short getKodeEselon() {
        return kodeEselon;
    }

    public void setKodeEselon(short kodeEselon) throws SIKDServiceException{
        this.kodeEselon = kodeEselon;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }
}
