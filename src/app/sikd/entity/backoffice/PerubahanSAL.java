/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;


import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sora
 */
public class PerubahanSAL implements Serializable, Cloneable{
    private long index;
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private double salAwal;
    private double penggunaanSal;
    private double silpa;
    private double koreksi;
    private double lainLain;

    public PerubahanSAL() {
    }

    public PerubahanSAL(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran) {
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
    }

    public PerubahanSAL(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran) {
        this(kodeSatker, kodePemda, namaPemda, tahunAnggaran);
        this.index = index;
    }

    public PerubahanSAL(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, double salAwal, double penggunaanSal, double silpa, double koreksi, double lainLain) {
        this(kodeSatker, kodePemda, namaPemda, tahunAnggaran);
        this.salAwal = salAwal;
        this.penggunaanSal = penggunaanSal;
        this.silpa = silpa;
        this.koreksi = koreksi;
        this.lainLain = lainLain;
    }

    public PerubahanSAL(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, double salAwal, double penggunaanSal, double silpa, double koreksi, double lainLain) {
        this(kodeSatker, kodePemda, namaPemda, tahunAnggaran, salAwal, penggunaanSal, silpa, koreksi, lainLain);
        this.index = index;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) {
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public double getSalAwal() {
        return salAwal;
    }

    public void setSalAwal(double salAwal) {
        this.salAwal = salAwal;
    }

    public double getPenggunaanSal() {
        return penggunaanSal;
    }

    public void setPenggunaanSal(double penggunaanSal) {
        this.penggunaanSal = penggunaanSal;
    }

    public double getSilpa() {
        return silpa;
    }

    public void setSilpa(double silpa) {
        this.silpa = silpa;
    }

    public double getKoreksi() {
        return koreksi;
    }

    public void setKoreksi(double koreksi) {
        this.koreksi = koreksi;
    }

    public double getLainLain() {
        return lainLain;
    }

    public void setLainLain(double lainLain) {
        this.lainLain = lainLain;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.index ^ (this.index >>> 32));
        hash = 97 * hash + Objects.hashCode(this.kodeSatker);
        hash = 97 * hash + Objects.hashCode(this.kodePemda);
        hash = 97 * hash + Objects.hashCode(this.namaPemda);
        hash = 97 * hash + this.tahunAnggaran;
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.salAwal) ^ (Double.doubleToLongBits(this.salAwal) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.penggunaanSal) ^ (Double.doubleToLongBits(this.penggunaanSal) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.silpa) ^ (Double.doubleToLongBits(this.silpa) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.koreksi) ^ (Double.doubleToLongBits(this.koreksi) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.lainLain) ^ (Double.doubleToLongBits(this.lainLain) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PerubahanSAL other = (PerubahanSAL) obj;
        if (!Objects.equals(this.kodeSatker, other.kodeSatker)) {
            return false;
        }
        if (!Objects.equals(this.kodePemda, other.kodePemda)) {
            return false;
        }
        if (!Objects.equals(this.namaPemda, other.namaPemda)) {
            return false;
        }
        if (this.tahunAnggaran != other.tahunAnggaran) {
            return false;
        }
        if (Double.doubleToLongBits(this.salAwal) != Double.doubleToLongBits(other.salAwal)) {
            return false;
        }
        if (Double.doubleToLongBits(this.penggunaanSal) != Double.doubleToLongBits(other.penggunaanSal)) {
            return false;
        }
        if (Double.doubleToLongBits(this.silpa) != Double.doubleToLongBits(other.silpa)) {
            return false;
        }
        if (Double.doubleToLongBits(this.koreksi) != Double.doubleToLongBits(other.koreksi)) {
            return false;
        }
        if (Double.doubleToLongBits(this.lainLain) != Double.doubleToLongBits(other.lainLain)) {
            return false;
        }
        return true;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    public String getSalAwalAsString(){
        return SIKDUtil.doubleToString(salAwal);
    }
    
    public void setSalAwalAsString(String salAwalAsString){
        salAwal = SIKDUtil.stringToDouble(salAwalAsString);
    }
    
    public String getPenggunaanSalAsString(){
        return SIKDUtil.doubleToString(penggunaanSal);
    }
    
    public void setPenggunaanSalAsString(String penggunaanSalAsString){
        salAwal = SIKDUtil.stringToDouble(penggunaanSalAsString);
    }
    
    public String getSilpaAsString(){
        return SIKDUtil.doubleToString(silpa);
    }
    
    public void setSilpaAsString(String silpaAsString){
        silpa = SIKDUtil.stringToDouble(silpaAsString);
    }
    
    public String getKoreksiAsString(){
        return SIKDUtil.doubleToString(koreksi);
    }
    
    public void setKoreksiAsString(String koreksiAsString){
        koreksi = SIKDUtil.stringToDouble(koreksiAsString);
    }

    public String getLainLainAsString(){
        return SIKDUtil.doubleToString(lainLain);
    }
    public void setLainLainAsString(String lainLainAsString){
//        if(surplusDefisitLOAsString != null && !surplusDefisitLOAsString.trim().equals(""))
            lainLain = SIKDUtil.stringToDouble(lainLainAsString);
    }
            
    
    
    
}
