/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;


import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class LOAkunJenis extends ObjNeracaAkun implements Serializable, Comparable<LOAkunJenis>, Cloneable {
    List<LOAkunObjek> akunObjeks;

    public LOAkunJenis() {
    }
    
    public LOAkunJenis(String kodeAkun, String namaAkun, double nilai) {
        super(kodeAkun, namaAkun, nilai);
    }
    
    public LOAkunJenis(long index, String kodeAkun, String namaAkun, double nilai) {
        super(index, kodeAkun, namaAkun, nilai);
    }

    public List<LOAkunObjek> getAkunObjeks() {
        return akunObjeks;
    }

    public void setAkunObjeks(List<LOAkunObjek> akunObjeks) {
        this.akunObjeks = akunObjeks;
    }

    @Override
    public String toString() {
        return getNamaAkun();
    }
    
    @Override
    public int compareTo(LOAkunJenis t) {
        return this.getKodeAkun().compareTo(t.getKodeAkun());
    }
    
    /**
     *
     * @return
     * @throws java.lang.CloneNotSupportedException
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}