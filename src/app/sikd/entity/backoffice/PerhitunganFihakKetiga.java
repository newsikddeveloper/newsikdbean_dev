package app.sikd.entity.backoffice;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;


public class PerhitunganFihakKetiga  implements Serializable{    
    private long id;
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private short statusData;
    private String namaAplikasi;
    private String pengembangAplikasi;
    private List<RincianPerhitunganFihakKetiga> rincians;
 
    public PerhitunganFihakKetiga() {
    }

    public PerhitunganFihakKetiga(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short statusData, String namaAplikasi, String pengembangAplikasi) {
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.statusData = statusData;
        this.namaAplikasi = namaAplikasi;
        this.pengembangAplikasi = pengembangAplikasi;
    }
    
    public PerhitunganFihakKetiga(long id, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short statusData, String namaAplikasi, String pengembangAplikasi) {
        this(kodeSatker, kodePemda, namaPemda, tahunAnggaran, statusData, namaAplikasi, pengembangAplikasi);
        this.id = id;        
    }
    
    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public List<RincianPerhitunganFihakKetiga> getRincians() {
        return rincians;
    }

    public void setRincians(List<RincianPerhitunganFihakKetiga> rincians) {
        this.rincians = rincians;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public short getStatusData() {
        return statusData;
    }

    public void setStatusData(short statusData) {
        this.statusData = statusData;
    }

    public String getNamaAplikasi() {
        return namaAplikasi;
    }

    public void setNamaAplikasi(String namaAplikasi) {
        this.namaAplikasi = namaAplikasi;
    }

    public String getPengembangAplikasi() {
        return pengembangAplikasi;
    }

    public void setPengembangAplikasi(String pengembangAplikasi) {
        this.pengembangAplikasi = pengembangAplikasi;
    }
    
    
    

    
}
