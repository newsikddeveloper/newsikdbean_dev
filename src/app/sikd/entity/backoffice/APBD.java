package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class APBD implements Serializable{
    private long index;
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private short kodeData;
    private short jenisCOA;
    private short statusData;
    private String nomorPerda;
    private Date tanggalPerda;    
    private String userName;
    private String password;
    private String namaAplikasi;
    private String pengembangAplikasi;
    private List<KegiatanAPBD> kegiatans;    

    public APBD() {
    }
    
    public APBD(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short kodeData, short jenisCOA, short statusData, String nomorPerda, Date tanggalPerda, String userName, String password, String namaAplikasi, String pengembangAplikasi){
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda =  namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.kodeData = kodeData;
        this.jenisCOA = jenisCOA;
        this.statusData = statusData;
        this.nomorPerda = nomorPerda;
        this.tanggalPerda = tanggalPerda;
        this.userName = userName;
        this.password = password;
        this.namaAplikasi = namaAplikasi;
        this.pengembangAplikasi = pengembangAplikasi;
    }
    public APBD(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short kodeData, short jenisCOA, short statusData, String nomorPerda, Date tanggalPerda, String namaAplikasi, String pengembangAplikasi){
        this.index = index;
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda =  namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.kodeData = kodeData;
        this.jenisCOA = jenisCOA;
        this.statusData = statusData;
        this.nomorPerda = nomorPerda;
        this.tanggalPerda = tanggalPerda;
        this.namaAplikasi = namaAplikasi;
        this.pengembangAplikasi = pengembangAplikasi;
    }
    public APBD(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short kodeData, short jenisCOA, short statusData, String nomorPerda, Date tanggalPerda, String userName, String password, String namaAplikasi, String pengembangAplikasi){
        this(kodeSatker, kodePemda, namaPemda, tahunAnggaran, kodeData, jenisCOA, statusData, nomorPerda, tanggalPerda, userName, password, namaAplikasi, pengembangAplikasi);
        this.index = index;
    }
    
    public String getKodeSatker(){
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) {
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda(){
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public short getKodeData() {
        return kodeData;
    }
    public void setKodeData(short kodeData){
        this.kodeData = kodeData;
    }

    public List<KegiatanAPBD> getKegiatans() {
        return kegiatans;
    }

    public void setKegiatans(List<KegiatanAPBD> kegiatans) {
        this.kegiatans = kegiatans;
    }
    
    public short getStatusData(){
        return statusData;
    }

    public void setStatusData(short statusData){
        this.statusData = statusData;
    }

    public String getUserName(){
        return userName;
    }

    public void setUserName(String userName){
        this.userName = userName;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public short getJenisCOA(){        
        return jenisCOA;
    }

    public void setJenisCOA(short jenisCOA){
        this.jenisCOA = jenisCOA;
    }

    public String getNomorPerda() {
        if(nomorPerda == null ) nomorPerda="";
        return nomorPerda;
    }

    public void setNomorPerda(String nomorPerda) {
        if(nomorPerda == null ) nomorPerda="";
        this.nomorPerda = nomorPerda;
    }

    public Date getTanggalPerda() {
        return tanggalPerda;
    }

    public void setTanggalPerda(Date tanggalPerda) {
        this.tanggalPerda = tanggalPerda;
    }

    public String getNamaAplikasi(){
        return namaAplikasi;
    }

    public void setNamaAplikasi(String namaAplikasi) {
        this.namaAplikasi = namaAplikasi;
    }

    public String getPengembangAplikasi() {
        return pengembangAplikasi;
    }

    public void setPengembangAplikasi(String pengembangAplikasi){
        this.pengembangAplikasi = pengembangAplikasi;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }
    
}
