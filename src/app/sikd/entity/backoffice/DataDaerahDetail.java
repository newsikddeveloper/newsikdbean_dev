/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;


import java.io.Serializable;
/**
 *
 * @author sora
 */

public class DataDaerahDetail implements Serializable{
    private long index;
    private int jumlahPenduduk;
    private double persenMiskin;
    private int garisMiskin;
    private double ipm;
    private int pengangguran;
    private double tingkatPengangguran;

    public DataDaerahDetail() {
    }

    public DataDaerahDetail(int jumlahPenduduk, double persenMiskin, int garisMiskin, double ipm, int pengangguran, double tingkatPengangguran) {
        this.jumlahPenduduk = jumlahPenduduk;
        this.persenMiskin = persenMiskin;
        this.garisMiskin = garisMiskin;
        this.ipm = ipm;
        this.pengangguran = pengangguran;
        this.tingkatPengangguran = tingkatPengangguran;
    }

    public DataDaerahDetail(long index, int jumlahPenduduk, double persenMiskin, int garisMiskin, double ipm, int pengangguran, double tingkatPengangguran) {
        this.index = index;
        this.jumlahPenduduk = jumlahPenduduk;
        this.persenMiskin = persenMiskin;
        this.garisMiskin = garisMiskin;
        this.ipm = ipm;
        this.pengangguran = pengangguran;
        this.tingkatPengangguran = tingkatPengangguran;
    }
    
    public int getGarisMiskin() {
        return garisMiskin;
    }

    public void setGarisMiskin(int garisMiskin) {
        this.garisMiskin = garisMiskin;
    }

    public double getIpm() {
        return ipm;
    }

    public void setIpm(double ipm) {
        this.ipm = ipm;
    }

    public int getJumlahPenduduk() {
        return jumlahPenduduk;
    }

    public void setJumlahPenduduk(int jumlahPenduduk) {
        this.jumlahPenduduk = jumlahPenduduk;
    }

    public int getPengangguran() {
        return pengangguran;
    }

    public void setPengangguran(int pengangguran) {
        this.pengangguran = pengangguran;
    }

    public double getPersenMiskin() {
        return persenMiskin;
    }

    public void setPersenMiskin(double persenMiskin) {
        this.persenMiskin = persenMiskin;
    }

    public double getTingkatPengangguran() {
        return tingkatPengangguran;
    }

    public void setTingkatPengangguran(double tingkatPengangguran) {
        this.tingkatPengangguran = tingkatPengangguran;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }
}
