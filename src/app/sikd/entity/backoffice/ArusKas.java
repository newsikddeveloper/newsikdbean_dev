package app.sikd.entity.backoffice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ArusKas  implements Serializable{
    private long index;
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private String judulArusKas;
    private short statusData;
    
    
    private List<ArusMasukOperasi> arusMasukOperasis;
    private List<ArusKeluarOperasi> arusKeluarOperasis;
    private List<ArusMasukInvestasi> arusMasukInvestasis;
    private List<ArusKeluarInvestasi> arusKeluarInvestasis;
    private List<ArusMasukPembiayaan> arusMasukPembiayaans;
    private List<ArusKeluarPembiayaan> arusKeluarPembiayaans;
    private List<ArusMasukNonAnggaran> arusMasukNonAnggarans;
    private List<ArusKeluarNonAnggaran> arusKeluarNonAnggarans;
    private ArusKasSaldo arusKasSaldo;
    
    
    

    public ArusKas() {
    }

    public ArusKas(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, String judulArusKas, short statusData) {
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.judulArusKas = judulArusKas;
        this.statusData = statusData;
    }

    public ArusKas(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, String judulArusKas, short statusData) {
        this.index = index;
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.judulArusKas = judulArusKas;
        this.statusData = statusData;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }
    
    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker){
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public String getJudulArusKas() {
        return judulArusKas;
    }

    public void setJudulArusKas(String judulArusKas) {
        this.judulArusKas = judulArusKas;
    }

    public List<ArusMasukOperasi> getArusMasukOperasis() {
        if( arusMasukOperasis == null ) arusMasukOperasis = new ArrayList();
        return arusMasukOperasis;
    }

    public void setArusMasukOperasis(List<ArusMasukOperasi> arusMasukOperasis) {
        this.arusMasukOperasis = arusMasukOperasis;
    }

    public List<ArusKeluarOperasi> getArusKeluarOperasis() {
        if( arusKeluarOperasis == null ) arusKeluarOperasis = new ArrayList();
        return arusKeluarOperasis;
    }

    public void setArusKeluarOperasis(List<ArusKeluarOperasi> arusKeluarOperasis) {
        this.arusKeluarOperasis = arusKeluarOperasis;
    }

    public List<ArusMasukInvestasi> getArusMasukInvestasis() {
        if( arusMasukInvestasis == null ) arusMasukInvestasis = new ArrayList();
        return arusMasukInvestasis;
    }

    public void setArusMasukInvestasis(List<ArusMasukInvestasi> arusMasukInvestasis) {
        this.arusMasukInvestasis = arusMasukInvestasis;
    }

    public List<ArusKeluarInvestasi> getArusKeluarInvestasis() {
        if( arusKeluarInvestasis == null ) arusKeluarInvestasis = new ArrayList();
        return arusKeluarInvestasis;
    }

    public void setArusKeluarInvestasis(List<ArusKeluarInvestasi> arusKeluarInvestasis) {
        this.arusKeluarInvestasis = arusKeluarInvestasis;
    }

    public List<ArusMasukPembiayaan> getArusMasukPembiayaans() {
        if( arusMasukPembiayaans == null ) arusMasukPembiayaans = new ArrayList();
        return arusMasukPembiayaans;
    }

    public void setArusMasukPembiayaans(List<ArusMasukPembiayaan> arusMasukPembiayaans) {
        this.arusMasukPembiayaans = arusMasukPembiayaans;
    }

    public List<ArusKeluarPembiayaan> getArusKeluarPembiayaans() {
        if( arusKeluarPembiayaans == null ) arusKeluarPembiayaans = new ArrayList();
        return arusKeluarPembiayaans;
    }

    public void setArusKeluarPembiayaans(List<ArusKeluarPembiayaan> arusKeluarPembiayaans) {
        this.arusKeluarPembiayaans = arusKeluarPembiayaans;
    }

    public List<ArusMasukNonAnggaran> getArusMasukNonAnggarans() {
        if( arusMasukNonAnggarans == null ) arusMasukNonAnggarans = new ArrayList();
        return arusMasukNonAnggarans;
    }

    public void setArusMasukNonAnggarans(List<ArusMasukNonAnggaran> arusMasukNonAnggarans) {
        this.arusMasukNonAnggarans = arusMasukNonAnggarans;
    }

    public List<ArusKeluarNonAnggaran> getArusKeluarNonAnggarans() {
        if( arusKeluarNonAnggarans == null ) arusKeluarNonAnggarans = new ArrayList();
        return arusKeluarNonAnggarans;
    }

    public void setArusKeluarNonAnggarans(List<ArusKeluarNonAnggaran> arusKeluarNonAnggarans) {
        this.arusKeluarNonAnggarans = arusKeluarNonAnggarans;
    }

    public ArusKasSaldo getArusKasSaldo() {
        if( arusKasSaldo == null ) arusKasSaldo = new ArusKasSaldo();
        return arusKasSaldo;
    }

    public void setArusKasSaldo(ArusKasSaldo arusKasSaldo) {
        this.arusKasSaldo = arusKasSaldo;
    }

    public short getStatusData() {
        return statusData;
    }

    public void setStatusData(short statusData) {
        this.statusData = statusData;
    }
    
}
