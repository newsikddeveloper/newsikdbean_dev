/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

/**
 *
 * @author sora
 */
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sora
 */
public class ObjNeracaAkun implements Serializable {
    private long index;
    private String kodeAkun;
    private String namaAkun;
    private double nilai;
    private short level;

    public ObjNeracaAkun() {
    }
    
    public ObjNeracaAkun(String kodeAkun, String namaAkun, double nilai) {
        this.kodeAkun = kodeAkun;
        this.namaAkun = namaAkun;
        this.nilai = nilai;
    }
    
    public ObjNeracaAkun(long index, String kodeAkun, String namaAkun, double nilai) {
        this(kodeAkun, namaAkun, nilai);
        this.index = index;
    }
    
    public ObjNeracaAkun(long index, String kodeAkun, String namaAkun, double nilai, short level) {
        this(kodeAkun, namaAkun, nilai);
        this.index = index;
        this.level = level;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getNamaAkun() {
        return namaAkun;
    }

    public void setNamaAkun(String namaAkun) {
        this.namaAkun = namaAkun;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }
    
    public String getNilaiAsString() {
        return SIKDUtil.doubleToString(nilai);
    }

    public void setNilaiAsString(String nilaiAsString) {
            this.nilai = SIKDUtil.stringToDouble(nilaiAsString);
    }
    
    public String getNilaiKosongAsString() {
        if(nilai==0) return "";
        else return SIKDUtil.doubleToString(nilai);
    }

    public void setNilaiKosongAsString(String nilaiAsString) {
            this.nilai = SIKDUtil.stringToDouble(nilaiAsString);
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) {
        this.kodeAkun = kodeAkun;
    }

    @Override
    public String toString() {
        return namaAkun;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (int) (this.index ^ (this.index >>> 32));
        hash = 29 * hash + Objects.hashCode(this.kodeAkun);
        hash = 29 * hash + Objects.hashCode(this.namaAkun);
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.nilai) ^ (Double.doubleToLongBits(this.nilai) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final NeracaAkunJenis other = (NeracaAkunJenis) obj;
        if(!this.kodeAkun.equals(other.getKodeAkun())) return false;
        return true;
    }
//    
//    
//    @Override
//    public int compareTo(ObjNeracaAkun t) {
//        return this.getKodeAkun().compareTo(t.getKodeAkun());
//    }
    
    /**
     *
     * @return
     * @throws java.lang.CloneNotSupportedException
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }
    
    
}