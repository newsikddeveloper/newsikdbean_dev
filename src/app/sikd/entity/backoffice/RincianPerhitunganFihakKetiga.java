package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;

/**
 *
 * @author detra
 */

public class RincianPerhitunganFihakKetiga implements Serializable{
    String uraian;
    private double pungutan;
    private double setoran;
    
    
    public RincianPerhitunganFihakKetiga() {
    }

    public RincianPerhitunganFihakKetiga(String uraian, double pungutan, double setoran) {
        this.uraian = uraian;
        this.pungutan = pungutan;
        this.setoran = setoran;
    }

    public String getUraian() {
        if( uraian!=null ) return uraian.trim();
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    
    
    public String getPungutanAsString(){
        return SIKDUtil.doubleToString(pungutan);
    }
    public void setPungutanAsString(String pungutanAsString){        
        if( pungutanAsString!=null && !pungutanAsString.trim().equals("") )  pungutan = SIKDUtil.stringToDouble(pungutanAsString);
        else pungutan = 0;
    }
    
    public String getSetoranAsString(){
        return SIKDUtil.doubleToString(setoran);
    }
    public void setSetoranAsString(String setoranAsString){        
        if( setoranAsString!=null && !setoranAsString.trim().equals("") )  setoran = SIKDUtil.stringToDouble(setoranAsString);
        else pungutan = 0;
    }

    public double getPungutan() {
        return pungutan;
    }

    public void setPungutan(double pungutan) {
        this.pungutan = pungutan;
    }

    public double getSetoran() {
        return setoran;
    }

    public void setSetoran(double setoran) {
        this.setoran = setoran;
    }
    
    
}
