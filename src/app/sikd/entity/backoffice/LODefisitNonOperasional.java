/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import app.sikd.util.SIKDUtil;
import java.io.Serializable;

/**
 *
 * @author sora
 */
public class LODefisitNonOperasional implements Serializable, Cloneable{
    private long index;
    private double penjualanAsetNonLancar;
    private double kewajibanJangkaPanjang;
    private double defisitLainnya;
    
    public LODefisitNonOperasional() {
    }

    public LODefisitNonOperasional(double penjualanAsetNonLancar, double kewajibanJangkaPanjang, double defisitLainnya) {
        this.penjualanAsetNonLancar = penjualanAsetNonLancar;
        this.kewajibanJangkaPanjang = kewajibanJangkaPanjang;
        this.defisitLainnya = defisitLainnya;
    }

    public LODefisitNonOperasional(long index, double penjualanAsetNonLancar, double kewajibanJangkaPanjang, double defisitLainnya) {
        this.index = index;
        this.penjualanAsetNonLancar = penjualanAsetNonLancar;
        this.kewajibanJangkaPanjang = kewajibanJangkaPanjang;
        this.defisitLainnya = defisitLainnya;
    }

    public double getDefisitLainnya() {
        return defisitLainnya;
    }

    public void setDefisitLainnya(double defisitLainnya) {
        this.defisitLainnya = defisitLainnya;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public double getPenjualanAsetNonLancar() {
        return penjualanAsetNonLancar;
    }

    public void setPenjualanAsetNonLancar(double penjualanAsetNonLancar) {
        this.penjualanAsetNonLancar = penjualanAsetNonLancar;
    }

    public double getKewajibanJangkaPanjang() {
        return kewajibanJangkaPanjang;
    }

    public void setKewajibanJangkaPanjang(double kewajibanJangkaPanjang) {
        this.kewajibanJangkaPanjang = kewajibanJangkaPanjang;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getPenjualanAsetNonLancarAsString() {
        return SIKDUtil.doubleToString(penjualanAsetNonLancar);
    }

    public void setPenjualanAsetNonLancarAsString(String penjualanAsetNonLancarAsString) {
        this.penjualanAsetNonLancar = SIKDUtil.stringToDouble(penjualanAsetNonLancarAsString);
    }

    public String getKewajibanJangkaPanjangAsString() {
        return SIKDUtil.doubleToString(kewajibanJangkaPanjang);
    }

    public void setKewajibanJangkaPanjangAsString(String kewajibanJangkaPanjangAsString) {
        this.kewajibanJangkaPanjang = SIKDUtil.stringToDouble(kewajibanJangkaPanjangAsString);
    }

    public String getDefisitLainnyaAsString() {
        return SIKDUtil.doubleToString(defisitLainnya);
    }

    public void setDefisitLainnyaAsString(String defisitLainnyaAsString) {
        this.defisitLainnya = SIKDUtil.stringToDouble(defisitLainnyaAsString);
    }
    
}
