/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class PajakDTHSKPD implements Serializable{
    private long indexPajak;
    private String namaAkunPajak;
    private String kodeAkunPajak;
    private short jenisPajak;
    private double nilaiPotongan;

    public PajakDTHSKPD() {
    }

    public PajakDTHSKPD(String kodeAkunPajak, String namaAkunPajak, short jenisPajak, double nilaiPotongan) {
        this.kodeAkunPajak = kodeAkunPajak;
        this.namaAkunPajak = namaAkunPajak;
        this.jenisPajak = jenisPajak;
        this.nilaiPotongan = nilaiPotongan;
    }
    public PajakDTHSKPD(long indexPajak, String kodeAkunPajak, String namaAkunPajak, short jenisPajak, double nilaiPotongan) {
        this.indexPajak = indexPajak;
        this.kodeAkunPajak = kodeAkunPajak;
        this.namaAkunPajak = namaAkunPajak;
        this.jenisPajak = jenisPajak;
        this.nilaiPotongan = nilaiPotongan;
    }

    public long getIndexPajak() {
        return indexPajak;
    }

    public void setIndexPajak(long indexPajak) {
        this.indexPajak = indexPajak;
    }

    public String getKodeAkunPajak() {
        return kodeAkunPajak;
    }

    public void setKodeAkunPajak(String kodeAkunPajak) {
        this.kodeAkunPajak = kodeAkunPajak;
    }
    
    public short getJenisPajak() {
        return jenisPajak;
    }

    public void setJenisPajak(short jenisPajak) {
        this.jenisPajak = jenisPajak;
    }

    public double getNilaiPotongan() {
        return nilaiPotongan;
    }

    public void setNilaiPotongan(double nilaiPotongan) {
        this.nilaiPotongan = nilaiPotongan;
    }

    public String getNamaAkunPajak() {
        return namaAkunPajak;
    }

    public void setNamaAkunPajak(String namaAkunPajak) {
        this.namaAkunPajak = namaAkunPajak;
    }
}
