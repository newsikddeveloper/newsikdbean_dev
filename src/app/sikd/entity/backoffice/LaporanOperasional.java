/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import app.sikd.entity.ws.fault.SIKDServiceException;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */

public class LaporanOperasional  implements Serializable, Cloneable{
    private long index;
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private short triwulan;
    private List<LOAkunUtama> akunUtamas;
    private LOSurplusNonOperasional surplusNonOP;
    private LODefisitNonOperasional defisitNonOP;
    private LOPosLuarBiasa posLB;

    public LaporanOperasional() {
    }

    public LaporanOperasional(String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short triwulan) {
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.triwulan = triwulan;
    }

    public LaporanOperasional(long index, String kodeSatker, String kodePemda, String namaPemda, short tahunAnggaran, short triwulan) {
        this.index = index;
        this.kodeSatker = kodeSatker;
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.tahunAnggaran = tahunAnggaran;
        this.triwulan = triwulan;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }
    
    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }

    public List<LOAkunUtama> getAkunUtamas() {
        return akunUtamas;
    }

    public void setAkunUtamas(List<LOAkunUtama> akunUtamas) {
        this.akunUtamas = akunUtamas;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public LOSurplusNonOperasional getSurplusNonOP() {
        return surplusNonOP;
    }

    public void setSurplusNonOP(LOSurplusNonOperasional surplusNonOP) {
        this.surplusNonOP = surplusNonOP;
    }

    public LODefisitNonOperasional getDefisitNonOP() {
        return defisitNonOP;
    }

    public void setDefisitNonOP(LODefisitNonOperasional defisitNonOP) {
        this.defisitNonOP = defisitNonOP;
    }

    public LOPosLuarBiasa getPosLB() {
        return posLB;
    }

    public void setPosLB(LOPosLuarBiasa posLB) {
        this.posLB = posLB;
    }

    public short getTriwulan() {
        return triwulan;
    }

    public void setTriwulan(short triwulan) {
        this.triwulan = triwulan;
    }
    
}
