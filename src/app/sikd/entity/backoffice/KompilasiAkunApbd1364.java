/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.backoffice;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class KompilasiAkunApbd1364 implements Serializable{
    private String kodeAkun;
    private double nilai;

    public KompilasiAkunApbd1364() {
    }

    public KompilasiAkunApbd1364(String kodeAkun, double nilai) {
        this.kodeAkun = kodeAkun;
        this.nilai = nilai;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public String getKodeAkun() {
        return kodeAkun;
    }

    public void setKodeAkun(String kodeAkun) {
        this.kodeAkun = kodeAkun;
    }

    
    
    
    
}
