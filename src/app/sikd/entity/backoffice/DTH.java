package app.sikd.entity.backoffice;

import app.sikd.entity.ws.fault.SIKDServiceException;
import app.sikd.util.SIKDUtil;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlTransient;


public class DTH  implements Serializable{    
    private String kodeSatker;
    private String kodePemda;
    private String namaPemda;
    private short tahunAnggaran;
    private String periode;
    private List<DTHSKPD> dthSkpd;
    private short statusData;
    private String userName;
    private String password;    
    private String namaAplikasi;
    private String pengembangAplikasi;    
    
    
    public DTH() {
    }

    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) throws SIKDServiceException{
        if( kodeSatker == null || kodeSatker.trim().length()!=6 ) 
            throw new SIKDServiceException("Silahkan isi kode Satker dengan panjang 6 digit");
        this.kodeSatker = kodeSatker;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) throws SIKDServiceException{
        if( kodePemda == null || kodePemda.trim().length() != 5 ) 
            throw new SIKDServiceException("Silahkan isi Kode Pemda dengan panjang 5 digit");
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) throws SIKDServiceException{
        if( namaPemda == null || namaPemda.trim().equals("") ) 
            throw new SIKDServiceException("Silahkan isi Data Nama Pemda");
        this.namaPemda = namaPemda;
    }

    public short getTahunAnggaran() {
        return tahunAnggaran;
    }

    public void setTahunAnggaran(short tahunAnggaran) {
        this.tahunAnggaran = tahunAnggaran;
    }
    
    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) throws SIKDServiceException{
        this.periode = SIKDUtil.setBulan(periode, "Periode");
    }
    @XmlTransient
    public void setShortPeriode(short periode) throws SIKDServiceException{
        if( periode >0 && periode < SIKDUtil.BULANS.length )
            this.periode = SIKDUtil.BULANS[periode];
        else throw new SIKDServiceException("Silahkan isi data Periode dengan angka di antara 1 sampai 12");
    }
    
    public short getShortPeriode(){        
        return SIKDUtil.getShortBulan(periode);
    }

    public List<DTHSKPD> getDthSkpd() {
        return dthSkpd;
    }

    public void setDthSkpd(List<DTHSKPD> dthSkpd) {
        this.dthSkpd = dthSkpd;
    }

    public short getStatusData() {
        return statusData;
    }

    public void setStatusData(short statusData) {
        this.statusData = statusData;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNamaAplikasi() {
        return namaAplikasi;
    }

    public void setNamaAplikasi(String namaAplikasi) {
        this.namaAplikasi = namaAplikasi;
    }

    public String getPengembangAplikasi() {
        return pengembangAplikasi;
    }

    public void setPengembangAplikasi(String pengembangAplikasi) {
        this.pengembangAplikasi = pengembangAplikasi;
    }
    
    
    
    
}
