package app.sikd.entity;

/**
 *
 * @author detra
 */
public interface IData {
    public static final String[] USER_TYPES = new String[]{"DJPK", "PEMDA", "KEMENKEU", "Lainnya"};
    public static final String USER_TYPE_DJPK = "DJPK";
    public static final String USER_TYPE_PEMDA = "PEMDA";
    public static final String USER_TYPE_KEMENKEU = "KEMENKEU";
    public static final String USER_TYPE_LAINNYA = "Lainnya";
    
    public static final short USER_TYPE_DJPK_SHORT = 0;
    public static final short USER_TYPE_PEMDA_SHORT = 1;
    public static final short USER_TYPE_KEMENKEU_SHORT = 2;
    public static final short USER_TYPE_LAINNYA_SHORT = 3;
    
    public static final String[] PEMDA_TYPES = new String[]{"Provinsi", "Kota", "Kabupaten"};
    public static final String PEMDA_TYPE_PROVINSI = "Provinsi";
    public static final String PEMDA_TYPE_KOTA = "Kota";
    public static final String PEMDA_TYPE_KAB = "Kabupaten";
    
    public static final short PEMDA_TYPE_PROVINSI_SHORT = 0;
    public static final short PEMDA_TYPE_KOTA_SHORT = 1;
    public static final short PEMDA_TYPE_KAB_SHORT = 2;
}