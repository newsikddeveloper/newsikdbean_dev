package app.sikd.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sora
 */
public class Pemda implements Serializable {
    private long id;
    private String kodeProvinsi;
    private String kodePemda;
    private String kodeSatker;
    private String namaPemda;
    private String namaSingkatPemda;
    private short tipePemda;

    public Pemda() {
    }

    public Pemda(String kodeSatker) {
        this.kodeSatker = kodeSatker;
    }
    public Pemda(String kodeProvinsi, String kodePemda, String kodeSatker, String namaPemda, String namaSingkatPemda, short tipePemda) {
        this.kodeProvinsi = kodeProvinsi;
        this.kodePemda = kodePemda;
        this.kodeSatker = kodeSatker;
        this.namaPemda = namaPemda;
        this.namaSingkatPemda = namaSingkatPemda;
        this.tipePemda = tipePemda;
    }

    public Pemda(long id, String kodeProvinsi, String kodePemda, String kodeSatker, String namaPemda, String namaSingkatPemda, short tipePemda) {
        this.id = id;
        this.kodeProvinsi = kodeProvinsi;
        this.kodePemda = kodePemda;
        this.kodeSatker = kodeSatker;
        this.namaPemda = namaPemda;
        this.namaSingkatPemda = namaSingkatPemda;
        this.tipePemda = tipePemda;
    }

    public short getTipePemda() {
        return tipePemda;
    }

    public void setTipePemda(short tipePemda) {
        this.tipePemda = tipePemda;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKodeProvinsi() {
        return kodeProvinsi;
    }

    public void setKodeProvinsi(String kodeProvinsi) {
        this.kodeProvinsi = kodeProvinsi;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getKodeSatker() {
        return kodeSatker;
    }

    public void setKodeSatker(String kodeSatker) {
        this.kodeSatker = kodeSatker;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }

    public String getNamaSingkatPemda() {
        return namaSingkatPemda;
    }

    public void setNamaSingkatPemda(String namaSingkatPemda) {
        this.namaSingkatPemda = namaSingkatPemda;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 89 * hash + Objects.hashCode(this.kodeProvinsi);
        hash = 89 * hash + Objects.hashCode(this.kodePemda);
        hash = 89 * hash + Objects.hashCode(this.kodeSatker);
        hash = 89 * hash + Objects.hashCode(this.namaPemda);
        hash = 89 * hash + Objects.hashCode(this.namaSingkatPemda);
        hash = 89 * hash + this.tipePemda;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pemda other = (Pemda) obj;
        if (!Objects.equals(this.kodeSatker, other.kodeSatker)) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public String toString() {
        return namaPemda;
    }
}
