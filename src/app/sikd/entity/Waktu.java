package app.sikd.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author sora
 */
public class Waktu implements Serializable{
    private String ikd;
    private short tanggal;
    private short bulan;

    public Waktu() {
    }

    public Waktu(String ikd, short tanggal, short bulan) {
        this.ikd = ikd;
        this.tanggal = tanggal;
        this.bulan = bulan;
    }

    public short getBulan() {
        return bulan;
    }

    public void setBulan(short bulan) {
        this.bulan = bulan;
    }

    public String getIkd() {
        return ikd;
    }

    public void setIkd(String ikd) {
        this.ikd = ikd;
    }

    public short getTanggal() {
        return tanggal;
    }

    public void setTanggal(short tanggal) {
        this.tanggal = tanggal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.ikd);
        hash = 97 * hash + this.tanggal;
        hash = 97 * hash + this.bulan;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Waktu other = (Waktu) obj;
        if (!Objects.equals(this.ikd, other.ikd)) {
            return false;
        }
        return true;
    }
    

    @Override
    public String toString() {
        return ikd;
    }
    
    
    
    
}
