/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.sikd.entity.graph;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class GraphGlobal implements Serializable{
    private String kodePemda;
    private String namaPemda;
    private double nilai;

    public GraphGlobal() {
    }

    public GraphGlobal(String kodePemda, String namaPemda, double nilai) {
        this.kodePemda = kodePemda;
        this.namaPemda = namaPemda;
        this.nilai = nilai;
    }

    public double getNilai() {
        return nilai;
    }

    public void setNilai(double nilai) {
        this.nilai = nilai;
    }

    public String getKodePemda() {
        return kodePemda;
    }

    public void setKodePemda(String kodePemda) {
        this.kodePemda = kodePemda;
    }

    public String getNamaPemda() {
        return namaPemda;
    }

    public void setNamaPemda(String namaPemda) {
        this.namaPemda = namaPemda;
    }
}
