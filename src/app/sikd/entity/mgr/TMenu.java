package app.sikd.entity.mgr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 *
 * @author sora
 */
public class TMenu implements Serializable{
    private long id;
    private String kode;
    private String nama;
    private MasterMenu masterMenu;
    private boolean menuItem;
    private String icon;
    private String iconType;
    private List<TMenu> subMenus;
    private TMenu parentMenu;

    public TMenu() {
    }

    public TMenu(long id) {
        this.id = id;
    }

    public TMenu(String kode, String nama, boolean menuItem) {
        this.kode = kode;
        this.nama = nama;
        this.menuItem = menuItem;
    }

    public TMenu(String kode, String nama, MasterMenu masterMenu, boolean menuItem) {
        this.kode = kode;
        this.nama = nama;
        this.masterMenu = masterMenu;
        this.menuItem = menuItem;
    }

    public TMenu(long id, String kode, String nama, boolean menuItem) {
        this.id = id;
        this.kode = kode;
        this.nama = nama;
        this.menuItem = menuItem;
    }
    public TMenu(long id, String kode, String nama, boolean menuItem, String icon) {
        this.id = id;
        this.kode = kode;
        this.nama = nama;
        this.menuItem = menuItem;
        this.icon = icon;
        setIconType(icon);
    }

    public TMenu(long id, String kode, String nama, MasterMenu masterMenu, boolean menuItem, String icon) {
        this.id = id;
        this.kode = kode;
        this.nama = nama;
        this.masterMenu = masterMenu;
        this.menuItem = menuItem;
        this.icon = icon;
        setIconType(icon);
    }

    public boolean isMenuItem() {
        return menuItem;
    }

    public void setMenuItem(boolean menuItem) {
        this.menuItem = menuItem;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public MasterMenu getMasterMenu() {
        return masterMenu;
    }

    public void setMasterMenu(MasterMenu masterMenu) {
        this.masterMenu = masterMenu;
    }

    public List<TMenu> getSubMenus() {
        return subMenus;
    }

    public void setSubMenus(List<TMenu> subMenus) {
        this.subMenus = subMenus;
    }
    
    public void addSubMenu(TMenu subMenu){
        if(subMenus == null) subMenus = new ArrayList();
        subMenus.add(subMenu);
    }

    public TMenu getParentMenu() {
        return parentMenu;
    }

    public void setParentMenu(TMenu parentMenu) {
        this.parentMenu = parentMenu;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
        setIconType(icon);
    }

    public String getIconType() {
        String rs = "";
        if( icon!=null && icon.trim().equals("")){
            StringTokenizer st = new StringTokenizer(icon, "-");
            
            while(st.hasMoreTokens()){
                rs= rs+st.nextToken();
            }
        }
        iconType = rs;
        return iconType;
    }

    public void setIconType(String iconType) {        
        this.iconType = iconType;
    }
    

    @Override
    public String toString() {
        return nama;
    }
    
    
}
