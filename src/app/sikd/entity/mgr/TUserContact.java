package app.sikd.entity.mgr;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class TUserContact implements Serializable{
    private long id;
    private String fullName;
    private String phone;
    private String email;
    private String workplace;
    private String address;

    public TUserContact() {
    }
    public TUserContact(String fullName) {
        this.fullName = fullName;
    }

    public TUserContact(String fullName, String phone, String email, String workplace, String address) {
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.workplace = workplace;
        this.address = address;
    }

    public TUserContact(long id, String fullName, String phone, String email, String workplace, String address) {
        this.id = id;
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.workplace = workplace;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }
    
    
    
}
