package app.sikd.entity.mgr;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class TUserGroup implements Serializable {
    private long id;
    private String nama;
    private String deskripsi;
    private TUserGroup parentGroup;
    private List<TUserGroup> subGroups;
    private List<TGroupRight> rights;

    public TUserGroup() {
    }

    public TUserGroup(long id) {
        this.id = id;
    }
    
    public TUserGroup(String nama, String deskripsi) {
        this.nama = nama;
        this.deskripsi = deskripsi;
    }

    public TUserGroup(long id, String nama, String deskripsi) {
        this.id = id;
        this.nama = nama;
        this.deskripsi = deskripsi;
    }
    
    public TUserGroup(long id, String nama, String deskripsi, TUserGroup parentGroup) {
        this.id = id;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.parentGroup = parentGroup;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public List<TUserGroup> getSubGroups() {
        return subGroups;
    }

    public void setSubGroups(List<TUserGroup> subGroups) {
        this.subGroups = subGroups;
    }

    public List<TGroupRight> getRights() {
        return rights;
    }

    public void setRights(List<TGroupRight> rights) {
        this.rights = rights;
    }

    public TUserGroup getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(TUserGroup parentGroup) {
        this.parentGroup = parentGroup;
    }

    @Override
    public String toString() {
        return nama;
    }
    
    
}
