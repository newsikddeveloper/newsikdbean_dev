package app.sikd.entity.mgr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author detra
 */
public class UserRef implements Serializable{
    long index;
    String userName;
    short userType;
    private List<UserGroup> grups;
    
    public static final String USER_TYPE_ADMIN = "admin";
    public static final String USER_TYPE_USER = "user";
    public static final List<UserType> USER_TYPES=new ArrayList<>(Arrays.asList(new UserType((short)0, USER_TYPE_ADMIN), new UserType((short)1, USER_TYPE_USER)));
    
    public static final short USER_TYPE_ADMIN_SHORT = 0;
    public static final short USER_TYPE_USER_SHORT = 1;

    public UserRef() {
    }

    public UserRef(long index, String userName, short userType) {
        this(userName, userType);
        this.index = index;
    }
    
    public UserRef(String userName, short userType) {
        this.userName = userName;
        this.userType = userType;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public short getUserType() {
        return userType;
    }

    public void setUserType(short userType) {
        this.userType = userType;
    }

    public List<UserGroup> getGrups() {
        return grups;
    }

    public void setGrups(List<UserGroup> grups) {
        this.grups = grups;
    }
}
