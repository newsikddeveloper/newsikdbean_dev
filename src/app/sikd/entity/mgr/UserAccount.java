package app.sikd.entity.mgr;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author detra
 */
public class UserAccount implements Serializable{
    long index;
    String userName;
    byte[] password;
    short userType;    
    boolean active;
    UserContact userContact;
    UserPemda userPemda;
    UserRef userRef;
    UserGroup grup;
    
    public static final String[] USER_TYPES = new String[]{"DJPK", "PEMDA", "KEMENKEU", "Lainnya"};
    public static final String USER_TYPE_DJPK = "DJPK";
    public static final String USER_TYPE_PEMDA = "PEMDA";
    public static final String USER_TYPE_KEMENKEU = "KEMENKEU";
    public static final String USER_TYPE_LAINNYA = "Lainnya";
    
    public static final short USER_TYPE_DJPK_SHORT = 0;
    public static final short USER_TYPE_PEMDA_SHORT = 1;
    public static final short USER_TYPE_KEMENKEU_SHORT = 2;
    public static final short USER_TYPE_LAINNYA_SHORT = 3;

    public UserAccount() {
    }

    public UserAccount(String userName, byte[] password, short userType, boolean active) {
        this.userName = userName;
        this.password = password;
        this.userType = userType;
        this.active = active;
    }

    public UserAccount(long index, String userName, byte[] password, short userType, boolean active) {
        this.index = index;
        this.userName = userName;
        this.password = password;
        this.userType = userType;
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        if( userName != null) this.userName = userName.trim();
        else this.userName = null;
    }

    public short getUserType() {
        return userType;
    }
    
    public String getUserTypeAsString(){
        if( userType>=0 && userType<USER_TYPES.length ) return USER_TYPES[userType];
        return "";
    }

    public void setUserType(short userType) {
        this.userType = userType;
    }

    public UserContact getUserContact() {
        return userContact;
    }

    public void setUserContact(UserContact userContact) {
        this.userContact = userContact;
    }

    public UserPemda getUserPemda() {
        return userPemda;
    }

    public void setUserPemda(UserPemda userPemda) {
        this.userPemda = userPemda;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserAccount other = (UserAccount) obj;
        if (!Objects.equals(this.userName, other.userName)) {
            return false;
        }
        return Arrays.equals(this.password, other.password);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.userName);
        hash = 67 * hash + Arrays.hashCode(this.password);
        hash = 67 * hash + this.userType;
        hash = 67 * hash + (this.active ? 1 : 0);
        return hash;
    }

    public UserGroup getGrup() {
        return grup;
    }

    public void setGrup(UserGroup grup) {
        this.grup = grup;
    }

    public UserRef getUserRef() {
        return userRef;
    }

    public void setUserRef(UserRef userRef) {
        this.userRef = userRef;
    }

    @Override
    public String toString() {
        return userName;
    }
    
}
