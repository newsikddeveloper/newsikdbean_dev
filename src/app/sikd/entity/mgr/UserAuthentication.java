package app.sikd.entity.mgr;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class UserAuthentication implements Serializable{
    String userName;
    String sessionId;

    public UserAuthentication() {
    }

    public UserAuthentication(String userName, String sessionId) {
        this.userName = userName;
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return userName;
    }
    
    
    
}
