package app.sikd.entity.mgr;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class AppMenu implements Serializable {
    long parentindex;
    long masterkey;
    AppModule modul;
    String menuName;
    boolean menuItem;
    private boolean isread;
    boolean iswrite;
  
  AppMenu[] subMenu;

    public AppMenu() {
    }

    public AppMenu(String menuName, boolean menuItem) {
        this.menuName = menuName;
        this.menuItem = menuItem;
    }

    public AppMenu(long masterkey, String menuName, boolean menuItem) {
        this.masterkey = masterkey;
        this.menuName = menuName;
        this.menuItem = menuItem;        
    }

    public AppMenu(long masterkey, String menuName, boolean menuItem, boolean iswrite) {
        this.masterkey = masterkey;
        this.menuName = menuName;
        this.menuItem = menuItem;
        this.iswrite = iswrite;
    }

    public AppMenu(long masterkey, String menuName, boolean menuItem, boolean isread, boolean iswrite) {
        this.masterkey = masterkey;
        this.menuName = menuName;
        this.menuItem = menuItem;
        this.isread = isread;
        this.iswrite = iswrite;
    }
    
    public long getMasterkey() {
        return masterkey;
    }

    public void setMasterkey(long masterkey) {
        this.masterkey = masterkey;
    }

    public boolean isMenuItem() {
        return menuItem;
    }

    public void setMenuItem(boolean menuItem) {
        this.menuItem = menuItem;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public AppMenu[] getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(AppMenu[] subMenu) {
        this.subMenu = subMenu;
    }

    public long getParentindex() {
        return parentindex;
    }

    public void setParentindex(long parentindex) {
        this.parentindex = parentindex;
    }

    public AppModule getModul() {
        return modul;
    }

    public void setModul(AppModule modul) {
        this.modul = modul;
    }

    @Override
    public String toString() {
        return menuName;
    }

    public boolean isIswrite() {
        return iswrite;
    }

    public void setIswrite(boolean iswrite) {
        this.iswrite = iswrite;
    }

    public boolean isIsread() {
        return isread;
    }

    public void setIsread(boolean isread) {
        this.isread = isread;
    }
}
