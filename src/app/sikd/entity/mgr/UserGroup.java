package app.sikd.entity.mgr;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author detra
 */
public class UserGroup implements Serializable {
    long masterkey;
    String groupname;
    String description;
    private short groupType;
    List<AppMenu> menus;

    public UserGroup() {
    }

    public UserGroup(String groupname, String description) {
        this.groupname = groupname;
        this.description = description;
    }

    public UserGroup(long masterkey, String groupname, String description) {
        this.masterkey = masterkey;
        this.groupname = groupname;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {        
        this.description = description;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public long getMasterkey() {
        return masterkey;
    }

    public void setMasterkey(long masterkey) {
        this.masterkey = masterkey;
    }

    public List<AppMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<AppMenu> menus) {
        this.menus = menus;
    }
    
    public short getGroupType() {
        return groupType;
    }

    public void setGroupType(short groupType) {
        this.groupType = groupType;
    }

    @Override
    public String toString() {
        return groupname;
    }
}
