package app.sikd.entity.mgr;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class MasterMenu implements Serializable {

    private long id;
    private String nama;
    private String alamat;
    private boolean actives;

    public MasterMenu() {
    }

    public MasterMenu(String nama, String alamat, boolean actives) {
        this.nama = nama;
        this.alamat = alamat;
        this.actives = actives;
    }

    public MasterMenu(long id, String alamat, boolean actives) {
        this.id = id;
        this.alamat = alamat;
        this.actives = actives;
    }

    public MasterMenu(long id, String nama, String alamat, boolean actives) {
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
        this.actives = actives;
    }

    public boolean isActives() {
        return actives;
    }

    public void setActives(boolean actives) {
        this.actives = actives;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
    
}
