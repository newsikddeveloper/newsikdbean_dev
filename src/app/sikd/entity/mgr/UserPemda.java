package app.sikd.entity.mgr;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author detra
 */
public class UserPemda implements Serializable{
    long index;
    short pemdaType;
    String pemdaCode;
    String pemdaName;
    private String satkerCode;
    
    public static final String[] PEMDA_TYPES = new String[]{"Provinsi", "Kota", "Kabupaten"};
    public static final String PEMDA_TYPE_PROVINSI = "Provinsi";
    public static final String PEMDA_TYPE_KOTA = "Kota";
    public static final String PEMDA_TYPE_KAB = "Kabupaten";
    
    public static final short PEMDA_TYPE_PROVINSI_SHORT = 0;
    public static final short PEMDA_TYPE_KOTA_SHORT = 1;
    public static final short PEMDA_TYPE_KAB_SHORT = 2;

    public UserPemda() {
    }

    public UserPemda(short pemdaType, String pemdaCode, String pemdaName, String satkerCode) {
        this.pemdaType = pemdaType;
        this.pemdaCode = pemdaCode;
        this.pemdaName = pemdaName;
        this.satkerCode = satkerCode;
    }

    public UserPemda(long index, short pemdaType, String pemdaCode, String pemdaName, String satkerCode) {
        this.index = index;
        this.pemdaType = pemdaType;
        this.pemdaCode = pemdaCode;
        this.pemdaName = pemdaName;
        this.satkerCode =satkerCode;
    }
    
    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getPemdaCode() {
        return pemdaCode;
    }

    public void setPemdaCode(String pemdaCode) {
        this.pemdaCode = pemdaCode;
    }

    public String getPemdaName() {
        return pemdaName;
    }

    public void setPemdaName(String pemdaName) {
        this.pemdaName = pemdaName;
    }

    public short getPemdaType() {
        return pemdaType;
    }
    public String getPemdaTypeAsString(){
        if( pemdaType >=0 && pemdaType< PEMDA_TYPES.length ) return PEMDA_TYPES[pemdaType];
        return "";
    }

    public void setPemdaType(short pemdaType) {
        this.pemdaType = pemdaType;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserPemda other = (UserPemda) obj;
        if (this.pemdaType != other.pemdaType) {
            return false;
        }
        if (!Objects.equals(this.pemdaCode, other.pemdaCode)) {
            return false;
        }
        if (!Objects.equals(this.pemdaName, other.pemdaName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (int) (this.index ^ (this.index >>> 32));
        hash = 79 * hash + this.pemdaType;
        hash = 79 * hash + Objects.hashCode(this.pemdaCode);
        hash = 79 * hash + Objects.hashCode(this.pemdaName);
        return hash;
    }

    @Override
    public String toString() {
        return getPemdaTypeAsString() + " " + pemdaName;
    }

    public String getSatkerCode() {
        return satkerCode;
    }

    public void setSatkerCode(String satkerCode) {
        this.satkerCode = satkerCode;
    }
}
