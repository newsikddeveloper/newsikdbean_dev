package app.sikd.entity.mgr;

import app.sikd.entity.Pemda;
import app.sikd.entity.WilayahKerja;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author sora
 */
public class TUserAccount implements Serializable{
    private long id;
    private String username;
    private byte[] password;
    private boolean actives;
    private TUserGroup group;
    private Pemda pemda;
    private TUserContact userContact;
    private List<TMenu> menus;
    private WilayahKerja wilayah;

    public TUserAccount() {
    }

    public TUserAccount(String username, byte[] password, boolean actives) {
        this.username = username;
        this.password = password;
        this.actives = actives;
    }

    public TUserAccount(long id, String username, byte[] password, boolean actives) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.actives = actives;
    }

    public boolean isActives() {
        return actives;
    }

    public void setActives(boolean actives) {
        this.actives = actives;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public TUserGroup getGroup() {
        return group;
    }

    public void setGroup(TUserGroup group) {
        this.group = group;
    }

    public Pemda getPemda() {
        return pemda;
    }

    public void setPemda(Pemda pemda) {
        this.pemda = pemda;
    }

    public TUserContact getUserContact() {
        return userContact;
    }

    public void setUserContact(TUserContact userContact) {
        this.userContact = userContact;
    }

    public List<TMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<TMenu> menus) {
        this.menus = menus;
    }

    public WilayahKerja getWilayah() {
        return wilayah;
    }

    public void setWilayah(WilayahKerja wilayah) {
        this.wilayah = wilayah;
    }
    
    
}
