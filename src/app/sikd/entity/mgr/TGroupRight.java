package app.sikd.entity.mgr;

import java.io.Serializable;

/**
 *
 * @author sora
 */
public class TGroupRight implements Serializable{
    private long id;
    private MasterMenu menu;
    private boolean reads;
    private boolean writes;

    public TGroupRight() {
    }

    public TGroupRight(MasterMenu menu, boolean reads, boolean writes) {
        this.menu = menu;
        this.reads = reads;
        this.writes = writes;
    }

    public TGroupRight(long id, MasterMenu menu, boolean reads, boolean writes) {
        this.id = id;
        this.menu = menu;
        this.reads = reads;
        this.writes = writes;
    }

    public boolean isWrites() {
        return writes;
    }

    public void setWrites(boolean writes) {
        this.writes = writes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public MasterMenu getMenu() {
        return menu;
    }

    public void setMenu(MasterMenu menu) {
        this.menu = menu;
    }

    public boolean isReads() {
        return reads;
    }

    public void setReads(boolean reads) {
        this.reads = reads;
    }
    
    
    
    
}
