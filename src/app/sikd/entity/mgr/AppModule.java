package app.sikd.entity.mgr;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class AppModule  implements Serializable {

    long masterkey;
    String moduleName;
    private String description;
    AppMenu[] appMenus;

    public AppModule() {
    }

    public AppModule(String moduleName, String description) {
        this.moduleName = moduleName;
        this.description = description;
    }

    public AppModule(long masterkey, String moduleName, String description) {
        this.masterkey = masterkey;
        this.moduleName = moduleName;
        this.description = description;
    }

    public long getMasterkey() {
        return masterkey;
    }

    public void setMasterkey(long masterkey) {
        this.masterkey = masterkey;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public AppMenu[] getAppMenus() {
        return appMenus;
    }

    public void setAppMenus(AppMenu[] appMenus) {
        this.appMenus = appMenus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    

    @Override
    public String toString() {
        return moduleName;
    }

}
