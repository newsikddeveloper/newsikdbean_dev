package app.sikd.entity.mgr;

import java.io.Serializable;

/**
 *
 * @author detra
 */
public class UserContact implements Serializable{
    long index;
    String fullName;
    String phone;
    String email;
    String workPlace;
    String address;

    public UserContact() {
    }

    public UserContact(String fullName, String phone, String email, String workPlace, String address) {
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.workPlace = workPlace;
        this.address = address;
    }

    public UserContact(long index, String fullName, String phone, String email, String workPlace, String address) {
        this.index = index;
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.workPlace = workPlace;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        if( address != null ) this.address = address.trim();
        else this.address = null;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if( email != null) this.email = email.trim();
        else this.email = null;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        if( fullName != null) this.fullName = fullName.trim();
        else this.fullName = null;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        if( phone != null ) this.phone = phone.trim();
        else this.phone = null;
    }

    public String getWorkPlace() {
        return workPlace;
    }

    public void setWorkPlace(String workPlace) {
        if( workPlace != null ) this.workPlace = workPlace.trim();
        else this.workPlace = null;
    }

    @Override
    public String toString() {
        return fullName;
    }
    
    

    
}
