package app.sikd.entity.mgr;

/**
 *
 * @author detra
 */
public interface IMGRDBConstants {
    //..............UserAccount Table and Attributes
    public static final String TABLE_USER_ACCOUNT = "useraccount";
    public static final String ATTR_RECORD_INDEX = "recordindex";
    public static final String ATTR_USER_NAME = "username";
    public static final String ATTR_PASSWORD = "password";
    public static final String ATTR_USER_TYPE = "usertype";
    public static final String ATTR_IS_ACTIVE = "isactive";
    
    //..............UserContact Table and Attributes
    public static final String TABLE_USER_CONTACT = "usercontact";
    public static final String ATTR_USER_ACCOUNT = "useraccount";
    public static final String ATTR_FULL_NAME = "fullname";
    public static final String ATTR_PHONE = "phone";
    public static final String ATTR_EMAIL = "email";
    public static final String ATTR_WORKPLACE = "workplace";
    public static final String ATTR_ADDRESS = "address";
    
    //..............UserPemda Table and Attributes
    public static final String TABLE_USER_PEMDA = "userpemda";
    public static final String ATTR_PEMDA_TYPE = "pemdatype";
    public static final String ATTR_PEMDA_CODE = "pemdacode";
    public static final String ATTR_PEMDA_NAME = "pemdaname";
    public static final String ATTR_SATKER_CODE = "satkercode";
    
    
    //..............UserGroup Table and Attributes
    public static final String TABLE_USER_GROUP = "usergroup";    
    public static final String ATTR_MASTERKEY = "masterkey";
    public static final String ATTR_DESCRIPTION = "description";
    public static final String ATTR_GROUP_NAME = "groupname";
    
    //..............UserAuthentication Table and Attributes
    public static final String TABLE_USER_AUTHENTICATION = "userauthentication";
    public static final String ATTR_SESSIONID = "sessionid";
    
    //..............UserRef Table and Attributes
    public static final String TABLE_USER_REF = "userref";
    
    //..............UserAssociation Table and Attributes
    public static final String TABLE_USER_ASSOCIATION = "userassociation";
    public static final String ATTR_USER_GROUP = "usergroup";
    public static final String ATTR_USER_REF = "userref";
    
    //..............AppModule Table and Attributes
    public static final String TABLE_APP_MODULE = "appmodule";    
    public static final String ATTR_MODULE_NAME = "modulename";
    
    //..............AppMenu Table and Attributes
    public static final String TABLE_APP_MENU = "appmenu";
    public static final String ATTR_APP_MODULE = "appmodule";
    public static final String ATTR_MENU_NAME = "menuname";
    public static final String ATTR_IS_MENU_ITEM = "ismenuitem";
    
    //..............AppMenuStructure Table and Attributes
    public static final String TABLE_APP_MENUSTRUCTURE = "appmenustructure";
    public static final String ATTR_PARENT_MENU = "parentmenu";
    public static final String ATTR_SUB_MENU = "submenu";
    
    
    //..............AppMenuRight Table and Attributes
    public static final String TABLE_APP_MENU_RIGHT = "appmenuright";
    public static final String ATTR_APP_MENU = "appmenu";
    public static final String ATTR_IS_READ = "isread";
    public static final String ATTR_IS_WRITE = "iswrite";
    public static final String ATTR_IS_EXTERNAL_ACCESS = "isexternalaccess";
    public static final String ATTR_IS_PRINTING = "isprinting";
    
}
