package app.sikd.dbapi.apbd;

/**
 *
 * @author detra
 */
public interface IAPBDConstants {
    public static final String TABLE_APBD = "apbd";
    public static final String ATTR_APBD_INDEX = "apbdindex";
    public static final String ATTR_KODE_SATKER = "kodesatker";
    public static final String ATTR_KODE_PEMDA = "kodepemda";
    public static final String ATTR_NAMA_PEMDA = "namapemda";
    public static final String ATTR_TAHUN_ANGGARAN = "tahunanggaran";
    public static final String ATTR_TANGGAL_PENGIRIMAN = "tglpengiriman";
    public static final String ATTR_KODE_DATA = "kodedata";
    public static final String ATTR_STATUS_DATA = "statusdata";
    public static final String ATTR_JENIS_COA = "jeniscoa";
    public static final String ATTR_NOMOR_PERDA = "nomorperda";
    public static final String ATTR_PENGEMBANG_APLIKASI = "pengembangaplikasi";
    public static final String ATTR_NAMA_APLIKASI = "namaaplikasi";
    public static final String ATTR_TANGGAL_PERDA = "tanggalperda";
    
    public static final String TABLE_KEGIATAN_APBD = "kegiatanapbd";
    public static final String ATTR_KEGIATAN_INDEX = "kegiatanindex";
    public static final String ATTR_KODE_URUSAN_PROGRAM = "kodeurusanprogram";
    public static final String ATTR_NAMA_URUSAN_PROGRAM = "namaurusanprogram";
    public static final String ATTR_KODE_URUSAN_PELAKSANA = "kodeurusanpelaksana";
    public static final String ATTR_NAMA_URUSAN_PELAKSANA = "namaurusanpelaksana";
    public static final String ATTR_KODE_SKPD = "kodeskpd";
    public static final String ATTR_NAMA_SKPD = "namaskpd";
    public static final String ATTR_KODE_PROGRAM = "kodeprogram";
    public static final String ATTR_NAMA_PROGRAM = "namaprogram";
    public static final String ATTR_KODE_KEGIATAN = "kodekegiatan";
    public static final String ATTR_NAMA_KEGIATAN = "namakegiatan";
    public static final String ATTR_KODE_FUNGSI = "kodefungsi";
    public static final String ATTR_NAMA_FUNGSI = "namafungsi";
    
    public static final String TABLE_KODE_REKENING_APBD = "koderekapbd";
    public static final String ATTR_KODE_AKUN_UTAMA = "kodeakunutama";
    public static final String ATTR_NAMA_AKUN_UTAMA = "namaakunutama";
    public static final String ATTR_KODE_AKUN_KELOMPOK = "kodeakunkelompok";
    public static final String ATTR_NAMA_AKUN_KELOMPOK = "namaakunkelompok";
    public static final String ATTR_KODE_AKUN_JENIS = "kodeakunjenis";
    public static final String ATTR_NAMA_AKUN_JENIS = "namaakunjenis";
    public static final String ATTR_KODE_AKUN_OBJEK = "kodeakunobjek";
    public static final String ATTR_NAMA_AKUN_OBJEK = "namaakunobjek";
    public static final String ATTR_KODE_AKUN_RINCIAN = "kodeakunrincian";
    public static final String ATTR_NAMA_AKUN_RINCIAN = "namaakunrincian";
    public static final String ATTR_KODE_AKUN_SUB = "kodeakunsub";
    public static final String ATTR_NAMA_AKUN_SUB = "namaakunsub";
    public static final String ATTR_NILAI_ANGGARAN = "nilaianggaran";
    
    public static final String TABLE_REALISASI_APBD = "realisasiapbd";
    public static final String ATTR_BULAN = "bulan";
    
    public static final String TABLE_REALISASI_KEGIATAN_APBD = "realisasikegiatanapbd";
    
    public static final String TABLE_REALISASI_KODE_REKENING_APBD = "realisasikoderekapbd";
}
